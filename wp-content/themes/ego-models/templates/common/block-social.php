<?php
$social = (is_singular('models') && isset($args['is_model'])) ? get_field('social_links') : get_field('social', 'options');
if (!empty($social)): ?>
	<div class="social <?php echo !empty($args['container']) ? $args['container'] : '' ?>">
		<?php foreach ($social as $item): ?>
			<a class="social__item <?php echo !empty($args['item']) ? $args['item'] : '' ?>" href="<?php echo $item['link'] ?>" target="_blank">
				<img class="social__icon" src="<?php echo $item['icon'] ?>" alt="">
				<?php echo svg($item['icon'], 'social__icon') ?>
			</a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
