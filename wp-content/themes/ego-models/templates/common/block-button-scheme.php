<?php
if (!empty(get_field('color-scheme-toggler', 'options'))) : ?>
	<div class="scheme-buttons scheme-buttons_debug">
		<span class="scheme-buttons__item scheme-buttons__item_white" data-scheme="white"></span>
		<span class="scheme-buttons__item scheme-buttons__item_grey" data-scheme="grey"></span>
		<span class="scheme-buttons__item scheme-buttons__item_dark" data-scheme="dark"></span>
	</div>
<?php endif; ?>