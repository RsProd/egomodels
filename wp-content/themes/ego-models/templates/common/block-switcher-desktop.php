<?php
$languages = pll_the_languages([
	'display_names_as' => 'slug',
	'hide_if_no_translation' => 1,
	'raw' => true
]);

if (!empty($languages)): ?>
	<div class="lang lang_desktop">
		<span class="lang__current"><?php echo LANG ?></span>
		<ul class="lang__inner">
			<?php foreach ($languages as $language): if ($language['slug'] !== LANG): ?>
				<li class="lang__item">
					<a class="lang__link"
					   href="<?php echo $language['url'] ?>"><?php echo $language['name'] ?></a>
				</li>
			<?php endif; endforeach; ?>
		</ul>
	</div>
<?php endif; ?>
