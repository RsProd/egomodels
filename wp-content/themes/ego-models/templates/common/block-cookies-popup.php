<?php
$link_id = get_field('cookies_page', 'options');
$link = get_permalink(pll_get_post($link_id));
?>
<div class="cookies-popup">
	<span class="cookies-popup__close"></span>
	<div
		class="cookies-popup__content"><?php _e('Мы используем файлы cookie для персонализации контента, который вы видите на сайте.', TEXT_DOMAIN); ?></div>
	<div class="cookies-popup__buttons">
		<div class="cookies-popup__button cookies-popup__button_ok">OK</div>
		<?php if (!empty($link)): ?>
			<a class="cookies-popup__button cookies-popup__button_more"
			   href="<?php echo $link ?>"><?php _e('Подробнее', TEXT_DOMAIN); ?></a>
		<?php endif; ?>
	</div>
</div>
