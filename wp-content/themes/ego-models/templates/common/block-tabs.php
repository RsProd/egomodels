<?php $tabs = get_field('tabs');

if (!empty($tabs)): ?>
<div class="photo-tabs">
	<div class="photo-tabs__buttons js-tabs">
		<?php foreach ($tabs as $tab): ?>
			<span class="photo-tabs__button js-tab-button"><?php echo $tab['name'] ?></span>
		<?php endforeach; ?>
	</div>
	<div class="photo-tabs__photos">
		<?php foreach ($tabs as $tab): ?>
			<div class="photo-tabs__photos--block js-tab-content">
				<?php if (!empty($tab['photos'])): ?>
					<div class="model-gallery js-masonry">
						<?php foreach ($tab['photos'] as $image): ?>
							<a class="model-gallery__item js-masonry-item" href="<?php echo esc_url($image['url']); ?>" data-lightbox="data-lightbox">
								<img 
									class="model-gallery__image" 
									src="<?php echo esc_url($image['sizes']['large']); ?>"
									alt="<?php echo esc_attr($image['alt']); ?>"
								/>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
