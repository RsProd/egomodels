<?php
$languages = pll_the_languages([
	'display_names_as' => 'slug',
	'hide_if_no_translation' => 1,
	'raw' => true
]);

if (!empty($languages)): ?>
	<ul class="lang lang_mobile">
		<?php foreach ($languages as $language): ?>
			<li class="lang__item">
				<a class="lang__link<?php if ($language['slug'] == LANG) echo ' is-active'; ?>"
				   href="<?php echo $language['url'] ?>"><?php echo $language['name'] ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>