<?php
$emails = get_field('emails', 'options')['form-popup'];
$contacts = get_field('general_contacts', 'options');
?>

<div class="form-container">
	<div class="popup popup_contacts">
		<div class="popup__window">
			<span class="popup__close" data-toggle=".popup_contacts"></span>
			<div class="popup__title"><?php _e('Форма обратной связи', TEXT_DOMAIN) ?></div>
			<div class="popup__inner">
				<form class="form form_contact">
					<input type="hidden" name="form-name" value="Всплывающее окно">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form__field form__field_full">
								<div
									class="form__label form__label_invert"><?php _e('Выберите тип обращения *', TEXT_DOMAIN) ?></div>
								<div class="select select_in-stream">
									<div class="select__title"><?php _e('Тема письма', TEXT_DOMAIN) ?></div>
									<input class="select__input" type="hidden" name="from">
									<ul class="select__content">
										<?php if (!empty($emails)): foreach ($emails as $email): ?>
											<li class="select__item"
											    data-value="<?php echo $email['email'] ?>"><?php echo $email['label'][LANG] ?></li>
										<?php endforeach; endif; ?>
									</ul>
								</div>
							</div>
							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="abb-name"><?php _e('ФИО', TEXT_DOMAIN) ?>*</label>
								<input class="form__input form__input_invert" id="abb-name" name="abb-name"
								       type="text" />
							</div>

							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="number"><?php _e('Телефон', TEXT_DOMAIN) ?>*</label>
								<input class="form__input form__input_invert" id="number" name="number" type="text"
								       placeholder="380" />
							</div>

						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="email"><?php _e('Электронная почта', TEXT_DOMAIN) ?>
									*</label>
								<input class="form__input form__input_invert" id="email" name="email" type="text" />
							</div>
							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="message"><?php _e('Сообщение', TEXT_DOMAIN) ?></label>
								<textarea class="form__textarea form__textarea_invert" id="message"
								          name="message"></textarea>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form__field form__field_full">
								<div class="checkbox checkbox_revert">
									<input class="checkbox__input" id="personal" name="personal" type="checkbox"
									       value="1" />
									<label class="checkbox__label checkbox__label_small" for="personal">
										<?php echo sprintf(__('Я согласен на %s обработку персональных данных %s', TEXT_DOMAIN), '<a href="' . get_the_permalink(pll_get_post(3)) . '">', '</a>') ?>
									</label>
								</div>
							</div>
							<div class="form__field form__field_full form__field_last">
								<button
									class="form__button form__button_popup"><?php _e('Отправить', TEXT_DOMAIN) ?></button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="form-buttons">
		<?php if (!empty($contacts['phone'])): ?>
			<a class="form-buttons__button form-buttons__button_call"
			   href="<?php echo linkPhone($contacts['phone']) ?>">
			<span class="form-buttons__icon-wrapper">
				<?php echo svg('phone', 'form-buttons__icon') ?>
			</span>
				<span class="form-buttons__text"><?php _e('Позвонить нам', TEXT_DOMAIN) ?></span>
			</a>
		<?php endif; ?>
		<div class="form-buttons__button form-buttons__button_write" data-toggle=".popup_contacts">
			<div class="form-buttons__icon-wrapper">
				<?php echo svg('chats', 'form-buttons__icon') ?>
			</div>
			<div class="form-buttons__text"><?php _e('Написать нам', TEXT_DOMAIN) ?></div>
		</div>
		<?php if (!empty($contacts['telegram'])): ?>
			<a class="form-buttons__button form-buttons__button_telegram" href="<?php echo $contacts['telegram'] ?>"
			   target="_blank">
			<span class="form-buttons__icon-wrapper">
				<?php echo svg('telegram-stroke', 'form-buttons__icon') ?>
			</span>
				<span class="form-buttons__text"><?php _e('Наш телеграм', TEXT_DOMAIN) ?></span>
			</a>
		<?php endif; ?>
	</div>
	<span class="form-container__overlay" data-toggle=".form-container" data-toggle-body="popup-open"></span>
</div>