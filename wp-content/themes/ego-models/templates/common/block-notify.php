<script>
function renderNotify(name) {
  return `<div class="notify">
			  <span class="notify__close"></span>
			  <div class="notify__title">${name}</div>
			  <a class="notify__button notify__button_ok"><?php _e('Перейти к списку', TEXT_DOMAIN) ?></a>
		</div>`;
}
</script>