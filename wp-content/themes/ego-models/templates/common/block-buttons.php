<div class="buttons">
	<a href="<?php echo get_permalink(pll_get_post(954)) ?>" class="buttons__item buttons__item_favorite">
		<?php echo svg('cheart', 'buttons__icon buttons__icon_cheart') ?>
	</a>
	<div class="buttons__item buttons__item_search" data-toggle=".search-container" data-toggle-body="popup-open">
		<?php echo svg('search', 'buttons__icon') ?>
	</div>
</div>
