<?php
$prices = get_field('gc-prices', 'options');
$links = get_field('studio')['links'];
$email = get_field('emails', 'options')['form-calendar'];
?>
<section class="section section_studio">
	<div class="container">
		<div
			class="section__title section__title_center section__title_small-margin"><?php _e('Аренда Зала', TEXT_DOMAIN) ?></div>
		<div class="calendar">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12 full">
					<div class="date-nav">
						<span class="date-nav__prev disabled"></span>
						<span class="date-nav__current"></span>
						<span class="date-nav__next"></span>
					</div>
				</div>
				<?php if (!empty($links)): ?>
					<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12 full">
						<div class="add-links">
							<?php foreach ($links as $link): if (!empty($link['link'])): ?>
								<a class="add-links__item"
								   href="<?php echo $link['link']['url'] ?>"><?php echo $link['link']['title'] ?></a>
							<?php endif; endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12 full">
					<div class="calendar__scroll">
						<div class="calendar__inner">
							<div class="calendar__days-label"></div>
							<div class="calendar__week-grid">
								<div class="calendar__hours-label"></div>
								<div class="calendar__week-days"></div>
							</div>
							<div class="calendar__bottom">
								<div class="calendar-info">
									<div class="calendar-info__item">
										<div class="calendar-info__icon">
											<div class="state state_disabled"></div>
										</div>
										<div class="calendar-info__label"><?php _e('Занято', TEXT_DOMAIN) ?></div>
									</div>
									<div class="calendar-info__item">
										<div class="calendar-info__icon">
											<div class="state state_select"></div>
										</div>
										<div class="calendar-info__label"><?php _e('Выбрано', TEXT_DOMAIN) ?></div>
									</div>
									<?php if (!empty($prices)): foreach (array_unique($prices, SORT_REGULAR) as $price): ?>
										<div class="calendar-info__item">
											<div class="calendar-info__icon">
												<div class="state <?php echo $price['marker'] ?>"></div>
											</div>
											<div class="calendar-info__label"><?php echo $price['value'] ?> ₴</div>
										</div>
									<?php endforeach; endif; ?>
								</div>
								<div class="total-price">
									<div class="total-price__label"><?php _e('Итого', TEXT_DOMAIN) ?>:</div>
									<div class="total-price__value">0 ₴</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12 full">
					<div class="calendar__form">
						<form class="form form_calendar">
							<div class="form__title"><?php _e('Контактные данные', TEXT_DOMAIN) ?></div>

							<input type="hidden" name="from" value="<?php echo $email ?>">
							<input type="hidden" name="form-name" value="Бронирование студии">

							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="abb-name"><?php _e('ФИО', TEXT_DOMAIN) ?></label>
								<input class="form__input form__input_invert" name="abb-name" id="abb-name"
								       type="text" />
							</div>
							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="number"><?php _e('Телефон', TEXT_DOMAIN) ?></label>
								<input class="form__input form__input_invert"
								       name="number" id="number" type="text"
								       placeholder="380" />
							</div>
							<div class="form__field form__field_full">
								<label class="form__label form__label_invert" for="email">Email</label>
								<input class="form__input form__input_invert" id="email" name="email" type="text" />
							</div>
							<div class="form__field form__field_full">
								<label class="form__label form__label_invert"
								       for="name"><?php _e('Коментарий', TEXT_DOMAIN) ?></label>
								<textarea class="form__textarea form__textarea_invert" name="message"
								          type="text"></textarea>
							</div>
							<div class="form__field form__field_full">
								<div class="checkbox checkbox_revert">
									<input class="checkbox__input"
									       id="personal"
									       name="personal"
									       type="checkbox"
									       value="1" />
									<label class="checkbox__label checkbox__label_small" for="personal">
										<?php echo sprintf(__('Я согласен на %s обработку персональных данных %s', TEXT_DOMAIN), '<a href="' . get_the_permalink(pll_get_post(3)) . '">', '</a>') ?>
									</label>
								</div>
							</div>
							<input type="hidden" name="date">
							<input type="hidden" name="price">
							<div class="form__field form__field_full">
								<button class="form__btn"><?php _e('Продолжить', TEXT_DOMAIN) ?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>