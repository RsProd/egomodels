<?php $info = get_sub_field('info') ?>
<section class="section section_about-us">
	<div class="container full-xs relative">
		<span class="stroke-placeholder stroke-placeholder_about-us">About agency</span>
		<div class="row full-xs">
			<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 full-xs">
				<?php if (!empty($info['image'])): ?>
					<div class="image-about">
						<img src="<?php echo $info['image'] ?>" alt="About agency">
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
				<div class="info">
					<?php if (!empty($info['title'])): ?>
						<h2 class="info__title"><?php echo $info['title'] ?></h2>
					<?php endif;
					if (!empty($info['desc'])): ?>
						<div class="info__content">
							<?php echo $info['desc'] ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
