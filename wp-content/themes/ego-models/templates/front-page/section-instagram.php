<?php
$token = get_sub_field('access_token');
$link = get_sub_field('instagram_link');

wp_localize_script('vendor', 'settings', array(
	'api_instagram' => $token
));

if (!empty($token)): ?>
	<section class="section section_instagram">
		<?php if (!empty($link)): ?>
			<a class="instagram-link-area" href="<?php echo $link ?>" target="_blank"></a>
		<?php endif; ?>
		<div class="grid-container">
			<div class="inst-grid">
				<?php for ($i = 1; $i <= 8; $i++): ?>
					<div class="inst-grid__layer">
						<div class="inst-grid__item"></div>
					</div>
					<?php if ($i === 4): ?>
						<div class="inst-grid__layer inst-grid__layer_link">
							<div class="inst-grid__item">
								<a class="inst-grid__link" href="<?php echo $link ?>" target="_blank">
									<?php echo svg('instagram-icon', 'inst-grid__icon') ?>
								</a>
							</div>
						</div>
					<?php endif;
				endfor; ?>
				<div class="inst-grid__layer">
					<div class="inst-grid__item inst-grid__item_text" target="_blank">Follow
						us
					</div>
				</div>
			</div>

			<div class="s-instagram-scroller"></div>
		</div>
	</section>
<?php endif; ?>

