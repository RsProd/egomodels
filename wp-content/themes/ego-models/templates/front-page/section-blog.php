<?php
$page_posts = get_option('page_for_posts');
$per_page = 4;

$articles = getPosts([
	"post_type" => "post",
	"posts_per_page" => $per_page
]);

$page_archive = [
	"title" => get_the_title($page_posts),
	"link" => get_permalink($page_posts)
];

if (!empty($articles['posts'])): ?>
	<section class="section section_blog">
		<div class="container relative">
			<span class="stroke-placeholder stroke-placeholder_blog stroke-placeholder_inversion">Blog</span>
			<h2 class="section__title section__title_inversion"><?php echo $page_archive['title'] ?></h2>
			<div class="grid">
				<?php foreach ($articles['posts'] as $article): ?>
					<div class="grid__item">
						<?php get_template_part('templates/loops/loop', 'article', $article) ?>
					</div>
				<?php endforeach; ?>
				<div class="grid__item grid__item_more">
					<?php if ($articles['total'] > $per_page): ?>
						<a class="button" href="<?php echo $page_archive['link'] ?>">
							<?php echo svg('plus', 'button__icon button__icon_invert') ?>
							<span
								class="button__name button__name_invert button__name_caps"><?php _e('Показать еще', TEXT_DOMAIN) ?></span>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>