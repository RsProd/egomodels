<section class="section section_models">
	<div class="container relative">
		<span class="stroke-placeholder stroke-placeholder_models">Become a model</span>
		<div class="section__head">
			<div class="section__title section__title_group"><?php _e('Стать моделью', TEXT_DOMAIN) ?></div>
		</div>
		<?php get_template_part('templates/become-model/block', 'form') ?>
	</div>
</section>
