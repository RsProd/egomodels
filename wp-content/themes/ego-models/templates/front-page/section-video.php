<?php $video = get_sub_field('video');
if (!empty($video['url'])): ?>
	<section class="section section_video">
		<div class="video-badge"></div>
		<div class="video">
			<?php if (!empty($video['autoplay'])): ?>
				<video class="video__poster" muted loop playsinline>
					<source data-src="<?php echo $video['url'] ?>" src="" type="video/mp4">
				</video>
			<?php else: ?>
				<div class="video__poster" style="background: url('<?php echo $video['poster'] ?>') center / fixed"></div>
			<?php endif; ?>
			<div class="video__popup">
				<video class="video__video" loop playsinline preload="metadata">
					<source src="<?php echo $video['url'] ?>" type="video/mp4">
				</video>
			</div>
		</div>
	</section>
<?php endif; ?>