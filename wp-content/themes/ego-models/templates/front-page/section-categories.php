<?php
	$categories = get_all_terms('catalog', 'field');
	$image = !empty(get_sub_field('image')) ? get_sub_field('image') : '';
?>
<section class="section section_welcome">
	<div class="container full-xs relative">
		<span class="stroke-placeholder stroke-placeholder_welcome">Ego models</span>
		<div class="category-selection" style="background: url('<?php echo $image ?>') center / cover">
			<div class="category-selection__info">
				<a class="category-selection__title" href="<?php echo get_post_type_archive_link('models') ?>">Models</a>
				<?php if (!empty($categories)): ?>
					<ul class="category-selection__list">
						<?php foreach ($categories as $category): ?>
							<li>
								<a href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php get_template_part('templates/common/block', 'social', ["container" => "social_abs"]) ?>
	<div class="scheme-buttons">
		<div class="scheme-buttons__item scheme-buttons__item_white" data-scheme="white">White</div>
		<div class="scheme-buttons__item scheme-buttons__item_grey" data-scheme="grey">Grey</div>
		<div class="scheme-buttons__item scheme-buttons__item_dark" data-scheme="dark">Black</div>
	</div>
</section>