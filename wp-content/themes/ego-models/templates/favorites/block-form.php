<?php $emails = get_field('emails', 'options')['form-favorites']; ?>

<div class="popup popup_favorites">
	<div class="popup__window">
		<span class="popup__close" data-toggle=".popup_favorites" data-toggle-body="popup-open"></span>
		<div class="popup__title"><?php _e('Отправить список', TEXT_DOMAIN) ?></div>
		<div class="popup__inner">
			<form class="form form_favorites">
				<input name="form-name" type="hidden" value="Список избранное">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form__field form__field_full">
							<div
								class="form__label form__label_invert"><?php _e('Выберите тип обращения *', TEXT_DOMAIN) ?></div>
							<div class="select select_in-stream">
								<div class="select__title"><?php _e('Не выбрано', TEXT_DOMAIN) ?></div>
								<input class="select__input" type="hidden" name="from">
								<ul class="select__content">
									<?php if (!empty($emails)): foreach ($emails as $email): ?>
										<li class="select__item"
										    data-value="<?php echo $email['email'] ?>"><?php echo $email['label'][LANG] ?></li>
									<?php endforeach; endif; ?>
								</ul>
							</div>
						</div>
						<div class="form__field form__field_full">
							<label for="abb-name"
							       class="form__label form__label_invert"><?php _e('ФИО', TEXT_DOMAIN) ?> *</label>
							<input id="abb-name" name="abb-name" type="text" class="form__input form__input_invert">
						</div>
						<div class="form__field form__field_full">
							<label for="number"
							       class="form__label form__label_invert"><?php _e('Телефон', TEXT_DOMAIN) ?>*</label>
							<input id="number" name="number" type="text" class="form__input form__input_invert">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form__field form__field_full">
							<label for="email" class="form__label form__label_invert">Email *</label>
							<input id="email" name="email" type="text" class="form__input form__input_invert">
						</div>
						<div class="form__field form__field_full">
							<label for="message"
							       class="form__label form__label_invert"><?php _e('Сообщение', TEXT_DOMAIN) ?></label>
							<textarea id="message" name="message"
							          class="form__textarea form__textarea_invert"></textarea>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form__field form__field_full">
							<div class="checkbox checkbox_revert">
								<input class="checkbox__input" id="personal_fav" name="personal" type="checkbox"
								       value="1">
								<label class="checkbox__label checkbox__label_small" for="personal_fav">
									<?php echo sprintf(__('Я согласен на %s обработку персональных данных %s', TEXT_DOMAIN), '<a href="' . get_the_permalink(pll_get_post(3)) . '">', '</a>') ?>
								</label>
							</div>
						</div>

						<div class="form__field form__field_full">
							<div class="checkbox checkbox_revert">
								<input class="checkbox__input" id="copy" name="copy" type="checkbox">
								<label class="checkbox__label checkbox__label_small"
								       for="copy"><?php _e('Отправить копию себе', TEXT_DOMAIN) ?></label>
							</div>
						</div>
						<input name="models" class="select-models" type="hidden">
						<div class="form__field form__field_full form__field_last">
							<button
								class="form__button form__button_popup"><?php _e('Отправить список', TEXT_DOMAIN) ?></button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<span class="popup__overlay" data-toggle=".popup_favorites" data-toggle-body="popup-open"></span>
</div>