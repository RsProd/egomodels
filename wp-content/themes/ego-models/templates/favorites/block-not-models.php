<div class="favorites favorites_show">
	<img class="favorites__image" src="<?php echo assets('images/favorites.png') ?>" alt="Favorites">
	<div class="favorites__inner">
		<div class="favorites__title"><?php _e('У вас нет сохраненных избранных', TEXT_DOMAIN) ?></div>
		<div class="favorites__desc"><?php _e('для добавления моделей нажмите сердце', TEXT_DOMAIN) ?></div>
		<?php echo svg('cheart', 'favorites__icon') ?>
	</div>
</div>