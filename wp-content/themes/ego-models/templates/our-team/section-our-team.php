<?php
$team = get_field('our-team', 'options');
if (!empty($team)): ?>
	<section class="section section_our-team">
		<div class="container">
			<h2 class="section__title section__title_team"><?php _e('Наша команда', TEXT_DOMAIN) ?></h2>
			<div class="row">
				<?php foreach ($team as $employee): ?>
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<div class="employee">
							<?php if (!empty('instagram')): ?>
								<a class="employee__instagram" href="<?php echo $employee['instagram'] ?>" target="_blank">
									<?php echo svg('instagram-stroke', 'employee__icon') ?>
								</a>
							<?php endif; ?>
							<div class="employee__photo">
								<?php if (!empty($employee['photo'])): ?>
									<img class="employee__image"
									     src="<?php echo $employee['photo'] ?>"
									     alt="<?php echo $employee['info']['name'] ?>"
									     title="<?php echo $employee['info']['name'] ?>" />
								<?php endif; ?>
							</div>
							<?php if (!empty($employee['info']['name'])): ?>
								<div class="employee__name"><?php echo $employee['info']['name'] ?></div>
							<?php endif; ?>
							<div class="employee__info">
								<?php if (!empty($employee['info']['position'])): ?>
									<div class="employee__item"><?php echo $employee['info']['position'] ?></div>
								<?php endif;
								if (!empty($employee['info']['e-mail'])): ?>
									<a class="employee__item"
									   href="mailto:<?php echo $employee['info']['e-mail'] ?>"><?php echo $employee['info']['e-mail'] ?></a>
								<?php endif;
								if (!empty($employee['info']['phone'])): ?>
									<a class="employee__item"
									   href="<?php echo linkPhone($employee['info']['phone']) ?>"><?php echo $employee['info']['phone'] ?></a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>