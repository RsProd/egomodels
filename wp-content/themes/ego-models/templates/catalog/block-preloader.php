<template id="loading">
	<div class="loading">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="200px" height="200px" viewBox="0 0 100 100"
		     preserveAspectRatio="xMidYMid">
			<text x="50" y="50" text-anchor="middle" dy="0.38em" fill="none" stroke-linecap="round"
			      stroke-linejoin="round" stroke="currentColor" stroke-width="0.6000000000000001" font-size="24"
			      font-family="Kelly Slab">
				Loading
				<animate attributeName="stroke-dasharray" repeatCount="indefinite" calcMode="spline"
				         dur="2.272727272727273s" values="0 100;100 100;0 100" keyTimes="0;0.5;1"
				         keySplines="0.3 0 0.7 1;0.3 0 0.7 1"></animate>
				<animate attributeName="stroke-dashoffset" repeatCount="indefinite" dur="2.272727272727273s"
				         values="0;0;-100" keyTimes="0;0.5;1"></animate>
			</text>
		</svg>
	</div>
</template>