<?php
$current_user = wp_get_current_user();
?>

<div class="reviews-form">
	<div class="reviews-form__title"><?php _e('Оставить отзыв', TEXT_DOMAIN) ?></div>
	<form class="form comments-form">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="form__field">
					<label class="form__label" for="abb-name"><?php _e('Фио', TEXT_DOMAIN) ?></label>
					<input class="form__input" id="abb-name" name="abb-name"
					       type="text" <?php echo is_user_logged_in() ? 'value="' . $current_user->display_name . '" readonly' : ''; ?>/>
				</div>
				<div class="form__field">
					<label class="form__label" for="email">Email</label>
					<input class="form__input" id="email" name="email"
					       type="text" <?php echo is_user_logged_in() ? 'value="' . $current_user->user_email . '" readonly' : ''; ?> />
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="form__field">
					<label class="form__label" for="comment"><?php _e('Отзыв', TEXT_DOMAIN) ?></label>
					<textarea class="form__textarea" id="comment" name="comment"></textarea>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="form__interactive">
					<div class="form__buttons">
						<?php if (is_user_logged_in()): ?>
							<div class="foto-comment">
								<img class="foto-comment__photo" src="<?php echo getAvatar() ?>"
								     alt="photo">
								<input type="hidden" name="photo" value="<?php echo getAvatar() ?>" />
							</div>
						<?php else: ?>
							<div class="facebook-login">
								<?php echo svg('fb-login', 'facebook-login__icon') ?>
								<input type="hidden" name="photo">
							</div>
						<?php endif; ?>

						<div class="rating" data-value="1">
							<div class="rating__label"><?php _e('ОЦЕНКА', TEXT_DOMAIN) ?>:</div>
							<input class="rating__input" type="hidden" name="rating" value="1">
							<div class="rating__items">
								<?php for ($stars = 1; $stars <= 5; $stars++): ?>
									<div class="rating__item" data-value="<?php echo $stars ?>">
										<?php echo svg('star-stroke', 'rating__icon') ?>
									</div>
								<?php endfor; ?>
							</div>
						</div>
					</div>
					<div class="form__field submit">
						<button><?php _e('Отправить', TEXT_DOMAIN) ?></button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>