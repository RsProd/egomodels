<?php
$article = isset($args) ? $args : false;
?>

<div class="article <?php echo is_front_page() || is_single() ? 'article_invert' : 'article_blog' ?> ">
	<?php if (!empty($article['thumbnail'])): ?>
		<img class="article__thumbnail" src="<?php echo $article['thumbnail'] ?>" alt="" />
	<?php endif; ?>
	<div class="article__info">
		<div class="article__row">
			<?php if (!empty($article['date'])): ?>
				<div class="article__date"><?php echo $article['date'] ?></div>
			<?php endif; ?>
		</div>
		<?php if (!empty($article['categories'])): ?>
			<div class="card-tags">
				<div class="card-tags__title"><?php _e('Теги', TEXT_DOMAIN) ?>:</div>
				<div class="card-tags__inner">
					<?php foreach ($article['categories'] as $key => $category): if ($key < 5): ?>
						<a class="card-tags__link" href="<?php echo $category['link'] ?>">
							#<?php echo $category['name'] ?></a>
					<?php endif; endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
		<a class="article__title" href="<?php echo $article['link'] ?>"><?php echo $article['title'] ?></a>
	</div>
</div>