<?php $comment = isset($args) ? $args : false ?>

<div class="review">
	<div class="review__thumbnail">
		<?php if (!empty($comment['photo'])): ?>
			<img class="review__image"
			     src="<?php echo $comment['photo'] ?>"
			     alt="<?php echo $comment['author'] ?>" title="<?php echo $comment['author'] ?>" />
		<?php else:
			echo svg('user', 'review__icon');
		endif; ?>
	</div>
	<div class="review__review-info">
		<div class="review__detail">
			<?php if (!empty($comment['author'])): ?>
				<div class="review__name"><?php echo $comment['author'] ?></div>
			<?php endif; ?>
			<div class="rating rating_invert" data-value="<?php echo $comment['rating'] ?>">
				<div class="rating__items">
					<?php for ($stars = 1; $stars <= 5; $stars++): ?>
						<div class="rating__item">
							<?php echo svg('star-stroke', 'rating__icon') ?>
						</div>
					<?php endfor; ?>
				</div>
			</div>
		</div>
		<?php if (!empty($comment['comment'])): ?>
			<div class="review__text"><?php echo $comment['comment'] ?></div>
		<?php endif; ?>
	</div>
</div>