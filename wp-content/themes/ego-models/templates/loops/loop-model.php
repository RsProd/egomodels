<?php $models = isset($args) ? $args : false; ?>

<div class="card-profile" data-id="<?php echo $models['id'] ?>">
	<?php if (!empty($models['optional']['departure'])): ?>
		<div class="label">
			<span class="label__name"><?php echo $models['optional']['departure'] ?></span>
			<?php echo svg('plane', 'label__icon') ?>
		</div>
	<?php endif; ?>
	<div class="card-profile__inner">
		<div class="card-profile__panel">
			<?php if (!empty($models['optional']['models_link'])): ?>
				<span class="card-profile__new">
					<?php echo svg('m', 'card-profile__model-icon') ?>
				</span>
			<?php else: if (!empty($models['optional']['new'])): ?>
				<span class="card-profile__new">new</span>
			<?php endif; endif;
			if (!is_page('favorites')): ?>
				<div class="button-card button-card_add <?php echo $models['favorite'] ? 'active' : '' ?>"
				     data-model-name="<?php echo $models['name'] ?>"
				     data-model-id="<?php echo $models['id'] ?>">
					<?php echo svg('cheart', 'button-card__icon') ?>
				</div>
			<?php else: ?>
				<div class="button-card button-card_remove">
					<?php echo svg('remove', 'button-card__icon') ?>
				</div>
			<?php endif; ?>
			<span class="card-profile__more-info"
			      data-toggle=".params"
			      data-toggle-context=".card-profile"
			></span>
		</div>
		<a class="card-profile__photo" href="<?php echo $models['link'] ?>" target="_blank">
			<?php if (!empty($models['photo'])): ?>
				<img class="card-profile__image active" src="<?php echo $models['photo'] ?>"
				     alt="<?php echo $models['name'] ?>" />

				<?php if (!empty($models['gallery'])): foreach ($models['gallery'] as $image): ?>
					<img class="card-profile__image" src="<?php echo $image['sizes']['model'] ?>"
					     alt="<?php echo $models['name'] ?>" loading="lazy" />
				<?php endforeach; endif; ?>
			<?php else: ?>
				<span class="image-holder">
					<?php echo svg('photo', 'image-holder__icon') ?>
					<span class="image-holder__label"><?php _e('Фотография не загружена', TEXT_DOMAIN) ?></span>
				</span>
			<?php endif; ?>
		</a>
	</div>
	<?php if (!empty($models['name'])): ?>
		<span class="card-profile__name"><?php echo $models['name'] ?></span>
	<?php endif; ?>
	<div class="params">
		<div class="params__inner">
			<?php foreach ($models['parameters'] as $key => $item): ?>
				<div class="params__item">
					<span class="params__label"><?php echo $key ?>:</span>
					<span class="params__value"><?php echo $item ?></span>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
