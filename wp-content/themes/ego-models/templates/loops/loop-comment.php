<?php $comment = isset($args) ? $args : false; ?>

<div class="comment" data-comment-id="<?php echo $comment['id'] ?>">
	<?php if (!empty($comment['photo'])): ?>
		<img class="comment__photo" src="<?php echo $comment['photo'] ?>" alt="<?php echo $comment['author'] ?>" />
	<?php else: ?>
		<div class="comment__photo">
			<?php echo svg('user', 'comment__icon') ?>
		</div>
	<?php endif; ?>
	<div class="comment__inner">
		<div class="comment__top">
			<div class="comment__name"><?php echo $comment['author'] ?></div>
			<div class="comment__date"><?php echo $comment['date'] ?></div>
			<div class="like <?php echo $comment['like']['liked'] ? 'active' : '' ?>">
				<span class="like__count"><?php echo $comment['like']['count'] ?></span>
				<?php echo svg('like', 'like__icon') ?>
			</div>
		</div>
		<div class="comment__content"><?php echo $comment['comment'] ?></div>
		<div class="comment__reply"><?php _e('Ответить', TEXT_DOMAIN) ?></div>
	</div>
</div>