<script>
function renderAjaxBlogCard(el) {
  let tagsString = ''
  el.tags.forEach(el => {
    tagsString += `<a class="card-tags__link" href="${el.tag_link}">#${el.tag_name}</a>`
  })

  return `<div class="article article_blog">
            <img class="article__thumbnail" src="${el.photo}" alt="${el.title}">
            <div class="article__info">
              <div class="article__row">
                <div class="article__date">2020.12.08</div>
              </div>
              <div class="card-tags">
                <div class="card-tags__title"><?php _e('Теги', TEXT_DOMAIN) ?>:</div>
                <div class="card-tags__inner">
                  ${tagsString}
                </div>
              </div>
              <a class="article__title" href="${el.link}">${el.title}</a>
            </div>
          </div>`;
}
</script>
