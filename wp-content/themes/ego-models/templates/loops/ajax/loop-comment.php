<script>
function renderCommentTemplate(comment) {
  console.log(comment);
  return `<div class="comment" data-comment-id="${comment.id}">
            ${comment.photo ?
                `<img class="comment__photo" src="${comment.photo}" alt="${comment.author}" />` :
                `<div class="comment__photo">
					<?php echo svg('user', 'comment__icon') ?>
				</div>`
            }
			<div class="comment__inner">
				<div class="comment__top">
					<div class="comment__name">${comment.author}</div>
					<div class="comment__date">${comment.date}</div>
					<div class="like ${comment.like.liked ? 'active' : ''}">
						<span class="like__count">${comment.like.count}</span>
						<?php echo svg('like', 'like__icon') ?>
					</div>
				</div>
				<div class="comment__content">${comment.comment}</div>
				<div class="comment__reply"><?php _e('Ответить', TEXT_DOMAIN) ?></div>
			</div>
		</div>`;
}
</script>

