<script>
function renderCommentTemplate(comment) {
  return `<div class="review">
	<div class="review__thumbnail">
	${comment.photo ?
	    `<img class="review__image"
			     src="${comment['photo']}"
			     alt="${comment['author']}" title="${comment['author']}" />` :
	    `<?php echo svg('user', 'review__icon') ?>`
	  };
	</div>
	<div class="review__review-info">
		<div class="review__detail">
			<div class="review__name">${comment['author']}</div>
			<div class="rating rating_invert" data-value="${comment['rating']}">
				<div class="rating__items">
					<div class="rating__item">
						<?php echo svg('star-stroke', 'rating__icon') ?>
					</div>
					<div class="rating__item">
						<?php echo svg('star-stroke', 'rating__icon') ?>
					</div>
					<div class="rating__item">
						<?php echo svg('star-stroke', 'rating__icon') ?>
					</div>
					<div class="rating__item">
						<?php echo svg('star-stroke', 'rating__icon') ?>
					</div>
					<div class="rating__item">
						<?php echo svg('star-stroke', 'rating__icon') ?>
					</div>
				</div>
			</div>
		</div>
		<div class="review__text">${comment['comment']}</div>
	</div>
</div>`;

}
</script>