<script>
function renderAjaxCardProfile(model) {
  const photo_not_found = "<?php _e('Фотография не загружена', TEXT_DOMAIN) ?>";

  let images = '';
  if (model.gallery) {
    model.gallery.forEach(el => {
      images += `<img class="card-profile__image" src="${el.sizes.model}" alt="${model.name}" />`;
    });
  }

	let params = '';
	for (const key in model.parameters) {
	  params += `<div class="params__item">
						<span class="params__label">${key}:</span>
						<span class="params__value">${model.parameters[key]}</span>
					</div>`;
	}

			return `<div class="card-profile" data-id="${model.id}">
				${model.optional.departure ? `
                <div class="label">
					<span class="label__name">${model.optional.departure}</span>
					<svg class="label__icon">
						<use xlink:href="${path.theme}/assets/icons/sprite.svg?v4.8#plane"></use>
					</svg>
				</div>` : ``}
				<div class="card-profile__inner">
					<div class="card-profile__panel">
						${model.optional.models_link ?
                             `<span class="card-profile__new">
								<svg class="card-profile__model-icon">
									<use xlink:href="${path.theme}/assets/icons/sprite.svg?v4.8#m"></use>
								</svg>
							</span>` : `${model.optional.new ? `<span class="card-profile__new">new</span>` : ``}`}
						<div class="button-card button-card_add" data-model-name="${model.name}" data-model-id="${model.id}">
							<svg class="button-card__icon button-card__icon_cheart">
								<use xlink:href="${path.theme}/assets/icons/sprite.svg?v4.8#cheart"></use>
							</svg>
						</div>
						<span class="card-profile__more-info" data-toggle=".params"
			                data-toggle-context=".card-profile">
						</span>
					</div>
					<a class="card-profile__photo" href="${model.link}" target="_blank">
						${model.photo ?
						  `<img class="card-profile__image active" src="${model.photo}" alt="${model.name}">${images}` :
						  `<span class="image-holder">
								<svg class="image-holder__icon">
									<use xlink:href="${path.theme}/assets/icons/sprite.svg?v4.8#photo"></use>
								</svg>
								<span class="image-holder__label">${photo_not_found}</span>
							</span>`
						}
					</a>
				</div>
				<span class="card-profile__name">${model.name}</span>
				<div class="params">
					<div class="params__inner">${params}</div>
				</div>
			</div>`;
		}
</script>
