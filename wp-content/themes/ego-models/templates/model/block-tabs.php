<?php $tabs = get_field('tabs');

if (!empty($tabs)): ?>
<div class="tabs">
	<ul class="tabs__nav">
		<?php foreach ($tabs as $tab): ?>
			<li><?php echo $tab['name'] ?></li>
		<?php endforeach; ?>
	</ul>
	<div class="tabs__inner">
		<?php foreach ($tabs as $tab): ?>
			<div class="tabs__item">
				<?php if (!empty($tab['photos'])): ?>
					<div class="model-gallery js-masonry">
						<?php foreach ($tab['photos'] as $image): ?>
							<a class="model-gallery__item js-masonry-item" href="<?php echo esc_url($image['url']); ?>"
							   data-lightbox="data-lightbox">
								<img
									class="model-gallery__image"
									src="<?php echo esc_url($image['sizes']['large']); ?>"
									alt="<?php echo esc_attr($image['alt']); ?>"
								/>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
