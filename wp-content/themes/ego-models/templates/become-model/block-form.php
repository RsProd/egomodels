<?php $email = get_field('emails', 'options'); ?>
<form class="form js-become-model-form" enctype="multipart/form-data">
	<input type="hidden" name="form-name" value="Стать моделью">
	<input type="hidden" name="from" value="<?php echo $email['form-become-a-model'] ?>">
	<div class="row become-model-row">
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="form__stage active" data-form-step="1">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form__field">
							<label class="form__label" for="abb-name"><?php _e('Фио', TEXT_DOMAIN) ?></label>
							<input class="form__input"
							       id="abb-name"
							       name="abb-name"
							       type="text"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label"
							       for="date-of-birth"><?php _e('Дата Рождения', TEXT_DOMAIN) ?></label>
							<input class="form__input"
							       id="date-of-birth"
							       name="date-of-birth"
							       type="date"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label" for="number"><?php _e('Телефон', TEXT_DOMAIN) ?></label>
							<input class="form__input"
							       id="number"
							       name="number"
							       type="text"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label" for="email">Email</label>
							<input class="form__input"
							       id="email"
							       name="email"
							       type="text"
							       data-validation="required email" />
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form__field">
							<label class="form__label" for="country"><?php _e('Страна', TEXT_DOMAIN) ?></label>
							<input class="form__input"
							       id="country"
							       name="country"
							       type="text"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label" for="city"><?php _e('Город', TEXT_DOMAIN) ?></label>
							<input class="form__input"
							       id="city"
							       name="city"
							       type="text"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label"
							       for="social"><?php _e('Ссылки на соц сети', TEXT_DOMAIN) ?></label>
							<input class="form__input" id="social" name="social" type="text" />
						</div>
						<div class="form__field form__field_center">
							<div class="checkbox checkbox_form">
								<input class="checkbox__input"
								       id="accept"
								       name="accept"
								       type="checkbox"
								       data-validation="required" />
								<label class="checkbox__label checkbox__label_small" for="accept">
									<?php echo sprintf(__('Я согласен на %s обработку персональных данных %s', TEXT_DOMAIN), '<a href="' . get_the_permalink(pll_get_post(3)) . '">', '</a>') ?>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form__stage" data-form-step="2">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form__field">
							<label class="form__label" for="height"><?php _e('Ваш рост', TEXT_DOMAIN) ?></label>
							<input class="form__input" id="height" name="height" type="number"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label" for="waist"><?php _e('Талия', TEXT_DOMAIN) ?></label>
							<input class="form__input" id="waist" name="waist" type="number"
							       data-validation="required" />
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form__field">
							<label class="form__label" for="social"><?php _e('Бюст', TEXT_DOMAIN) ?></label>
							<input class="form__input" id="bust" name="bust" type="number"
							       data-validation="required" />
						</div>
						<div class="form__field">
							<label class="form__label" for="social"><?php _e('Бедра', TEXT_DOMAIN) ?></label>
							<input class="form__input" id="hips" name="hips" type="number"
							       data-validation="required" />
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form__field">
							<label class="form__label" for="motivation">
								<?php _e('Почему вы хотите стать моделью Ego Models?', TEXT_DOMAIN) ?>
							</label>
							<textarea class="form__input" id="motivation" name="motivation"
							          placeholder="Текст"></textarea>
						</div>
						<div class="form__field">
							<div class="checkbox checkbox_form">
								<input class="checkbox__input js-affiliates-checkbox" id="affiliate"
								       name="affiliate" type="checkbox" />
								<label class="checkbox__label checkbox__label_small" for="affiliate">
									<?php _e('Помогал ли Вам кто-то заполнять анкету?', TEXT_DOMAIN) ?>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row js-affiliates-row affiliates-row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form__field">
							<label class="form__label" for="affiliate-name"><?php _e('Фио', TEXT_DOMAIN) ?>
							</label>
							<input class="form__input" id="affiliate-name" name="affiliate-name"
							       type="text" />
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form__field">
							<label class="form__label" for="affiliate-phone"><?php _e('Телефон', TEXT_DOMAIN) ?></label>
							<input class="form__input" id="affiliate-phone" name="affiliate-phone"
							       type="text" />
						</div>
					</div>
				</div>
			</div>
			<div class="form__stage form__stage form__stage_file" data-form-step="3">
				<div class="files-field">
					<div class="files-field__inner">
						<div class="drop-zone"></div>
						<div class="file-uploader">
							<input class="file-uploader__input" type="file" name="files[]" id="file"
							       accept=".jpg, .jpeg, .png" multiple>
							<label class="file-uploader__label" for="file">
								<span class="file-uploader__dragndrop">
									<?php echo svg('upload', 'file-uploader__icon') ?>
									<span class="file-uploader__text"><?php _e('Загрузить фото', TEXT_DOMAIN) ?></span>
								</span>
							</label>
						</div>
						<div class="files-preview"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 first-sm first-xs flex-end">
			<ul class="form-navigation">
				<li class="form-navigation__item active" data-form-step-mark="1">
					<div class="form-navigation__inner"><span
							class="form-navigation__label"><?php _e('Первый этап', TEXT_DOMAIN) ?></span>
						<button class="form-navigation__num" type="button" data-form-step-mark-btn="1">1</button>
					</div>
				</li>
				<li class="form-navigation__item" data-form-step-mark="2">
					<div class="form-navigation__inner"><span
							class="form-navigation__label"><?php _e('Второй этап', TEXT_DOMAIN) ?></span>
						<button class="form-navigation__num" type="button"
						        data-form-step-mark-btn="2">2
						</button>
					</div>
				</li>
				<li class="form-navigation__item" data-form-step-mark="3">
					<div class="form-navigation__inner"><span
							class="form-navigation__label"><?php _e('Третий этап', TEXT_DOMAIN) ?></span>
						<button class="form-navigation__num" type="button"
						        data-form-step-mark-btn="3">3
						</button>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<button class="button" data-form-step-trigger="2">
		<?php echo svg('plus', 'button__icon') ?>
		<div class="button__name"><?php _e('Продолжить', TEXT_DOMAIN) ?></div>
	</button>
</form>