<div class="post-info">
	<div class="post-info__item"><?php echo get_the_date('j F, Y') ?></div>
	<div class="post-info__item">
		<?php get_template_part('templates/single/block', 'author') ?>
	</div>
	<div class="post-info__item">
		<div class="post-info__title"><?php _e('Поделиться', TEXT_DOMAIN) ?>:</div>
		<?php get_template_part('templates/single/block', 'share') ?>
	</div>
	<div class="post-info__item">
		<div class="post-info__title"><?php _e('Теги', TEXT_DOMAIN) ?>:</div>
		<?php get_template_part('templates/single/block', 'categories') ?>
	</div>
</div>