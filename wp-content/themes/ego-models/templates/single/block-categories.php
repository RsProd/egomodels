<?php $categories = get_article_categories(get_the_ID());
if (!empty($categories)): ?>
	<div class="tag">
		<?php foreach ($categories as $category): ?>
			<div class="tag__item">
				<a class="tag__link" href="<?php echo $category['link'] ?>">#<?php echo $category['name'] ?></a>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>