<?php $share = get_field('share', 'options');
if (!empty($share)): ?>
	<div class="share">
		<?php foreach ($share as $item): ?>
			<a class="share__item" href="<?php echo $item['link'] ?>">
				<?php echo svg($item['icon'], 'share__icon') ?>
			</a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>