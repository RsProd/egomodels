<?php
$postID = get_the_ID();
$comments = getComments($postID);
$current_user = wp_get_current_user();
?>

<section class="section section_contrast">
	<div class="container">
		<h2 class="section__title section__title_single-comments"><?php _e('Комментарии', TEXT_DOMAIN) ?>
			<span class="count-comments"><?php echo $comments['all_count'] ?></span>
		</h2>
		<div class="comments">
			<div class="comments__inner comments-list">
				<?php listComments($comments['comments'],
					'comments__item load',
					'comments__sub-item load',
					'templates/loops/loop-comment') ?>
			</div>
			<?php if ($comments['count'] > 3): ?>
				<div class="loadmore loadmore_comments">
					<?php echo svg('load', 'loadmore__icon') ?>
					<span><?php _e('Загрузить еще', TEXT_DOMAIN) ?></span>
				</div>
			<?php endif; ?>
		</div>
		<form class="comments-form">
			<div class="row">
				<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
					<div class="comments-form__field">
						<?php if (is_user_logged_in()):
							$photo = getAvatar(); ?>
							<div class="foto-comment">
								<?php if (!empty($photo)): ?>
									<img class="foto-comment__photo" src="<?php echo $photo ?>" alt="photo">
									<input type="hidden" name="photo" value="<?php echo $photo ?>">
								<?php else: ?>
									<div class="foto-comment__not-found">
										<?php echo svg('user', 'foto-comment__icon') ?>
									</div>
								<?php endif; ?>
							</div>
						<?php else: ?>
							<div class="facebook-login">
								<?php echo svg('fb-login', 'facebook-login__icon') ?>
								<input type="hidden" name="photo">
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="comments-form__field">
						<input name="abb-name"
						       class="comments-form__input"
						       placeholder="<?php _e('Имя', TEXT_DOMAIN) ?>"
						       type="text" <?php echo is_user_logged_in() ? 'value="' . $current_user->display_name . '" readonly' : ''; ?> />
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
					<div class="comments-form__field">
						<textarea name="comment" class="comments-form__textarea"
						          placeholder="<?php _e('Комментарий', TEXT_DOMAIN) ?>"></textarea>
					</div>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
					<div class="comments-form__field">
						<button class="comments-form__button">
							<?php echo svg('arrow-send', 'comments-form__button-icon') ?>
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<?php get_template_part('templates/loops/ajax/loop', 'comment') ?>
</section>