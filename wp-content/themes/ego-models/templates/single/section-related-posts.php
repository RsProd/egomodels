<?php

$post_id = get_the_ID();
$terms = get_the_terms_id($post_id, 'category');
$posts = getPosts([
	"cat" => $terms,
 	"post__not_in" => [$post_id],
 	"posts_per_page" => 3
]);

if (!empty($posts['posts'])): ?>
	<section class="section section_related-posts">
		<div class="container">
			<div class="section__title section__title_inversion"><?php _e('Похожие новости', TEXT_DOMAIN) ?></div>
			<div class="row">
				<?php foreach ($posts['posts'] as $post): ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<?php get_template_part('templates/loops/loop', 'article', $post) ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>