<section class="section section_subscribe">
	<form class="subscribe-form">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<h2 class="subscribe-form__title"><?php _e('Подписаться на новости', TEXT_DOMAIN) ?></h2>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-center">
					<input class="subscribe-form__input"
					       name="email"
					       placeholder="<?php _e('Электронная почта', TEXT_DOMAIN) ?>"
					       type="text" />
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-center">
					<button class="subscribe-form__button"><?php _e('Подписаться', TEXT_DOMAIN) ?></button>
				</div>
			</div>
		</div>
	</form>
</section>