<?php
$authorID = get_the_author_meta('ID');
$authorMeta = get_user_meta($authorID);
$authorFullName = sprintf('%s %s', $authorMeta['first_name'][0], $authorMeta['last_name'][0]);
$photo = get_field('photo', 'user_' . $authorID);
?>

<div class="author-card">
	<?php if (!empty($photo)): ?>
		<img class="author-card__photo"
		     src="<?php echo $photo['sizes']['thumbnail'] ?>"
		     alt="<?php echo $authorFullName ?>" />
	<?php endif; ?>
	<div class="author-card__info">
		<div class="author-card__label"><?php _e('Автор', TEXT_DOMAIN) ?>:</div>
		<div class="author-card__name"><?php echo $authorFullName ?> </div>
	</div>
</div>