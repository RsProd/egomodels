<?php
$email = get_field('emails', 'options')['form-contact-page'];
?>
<form class="form form_contact-page">
	<div class="form__inputs">
		<input type="hidden" name="from" value="<?php echo $email ?>">
		<input type="hidden" name="form-name" value="Страница контакты">
		<div class="form__field name">
			<input class="form__input"
			       id="abb-name"
			       name="abb-name"
			       type="text"
			       placeholder="<?php _e('Имя', TEXT_DOMAIN) ?>" />
		</div>
		<div class="form__field phone">
			<input class="form__input"
			       id="number"
			       name="number" type="text"
			       placeholder="<?php _e('Телефон', TEXT_DOMAIN) ?>" />
		</div>
		<div class="form__field email">
			<input class="form__input"
			       id="email"
			       name="email"
			       type="text"
			       placeholder="Email" />
		</div>
		<div class="form__field message">
			<textarea class="form__textarea"
			          id="message"
			          name="message"
			          placeholder="<?php _e('Сообщение', TEXT_DOMAIN) ?>"></textarea>
		</div>
		<div class="form__field submit">
			<button class="form__button"><?php _e('Отправить сообщение', TEXT_DOMAIN) ?></button>
		</div>
	</div>
</form>