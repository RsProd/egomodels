<?php get_header();
$categories = get_all_terms('category');
$per_page = 9;

$articles = getPosts([
	"post_type" => "post",
	"posts_per_page" => $per_page
]);

?>
<main class="main" role="main">
	<section class="section">
		<div class="container relative">
			<span class="stroke-placeholder stroke-placeholder_blog">Blog</span>
			<div class="page-head">
				<?php breadcrumbs();
				if (!empty($categories)): ?>
					<div class="categories">
						<div class="categories__title"><?php _e('Разделы', TEXT_DOMAIN) ?>:</div>
						<div class="categories__list">
							<?php foreach ($categories as $category): ?>
								<a class="categories__link"
								   href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="row js-blogs-container">
				<?php if (!empty($articles['posts'])): foreach ($articles['posts'] as $article): ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<?php get_template_part('templates/loops/loop', 'article', $article) ?>
					</div>
				<?php endforeach; else: ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="post-not-found"><?php _e('Записей не найдено', TEXT_DOMAIN) ?></div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($articles['total'] > $per_page): ?>
			<div class="loadmore loadmore_blog js-loadmore-blogs">
				<?php echo svg('load', 'loadmore__icon') ?>
				<span><?php _e('Загрузить еще', TEXT_DOMAIN) ?></span>
			</div>
		<?php endif; ?>
	</section>
	<?php
	get_template_part('templates/loops/ajax/loop', 'blog');
	?>
</main>
<?php get_footer(); ?>
