<?php
function get_all_terms($taxonomy, $order = 'abc'): array {

	$result = [];

	$args = [
		"taxonomy" => $taxonomy,
		"hide_empty" => true,
		"parent" => 0
	];

	if ($order == "abc") {

		$args['orderby'] = "name";

	} elseif ($order == 'field') {

		$args['meta_key'] = "term_order";
		$args['orderby'] = "meta_value_num";

	}

	$terms = get_terms($args);

	foreach ($terms as $term) {

		$childrenArr = get_terms([
			"taxonomy" => $taxonomy,
			"hide_empty" => true,
			"parent" => $term->term_id
		]);

		$children = [];

		foreach ($childrenArr as $child) {

			$children[] = [
				"name" => $child->name,
				"link" => get_term_link((int)$child->term_id, $taxonomy),
				"id" => $child->term_id,
			];

		}

		$result[] = [
			"name" => $term->name,
			"id" => $term->term_id,
			"link" => get_term_link((int)$term->term_id, $taxonomy),
			"children" => $children
		];
	}

	return $result;
}