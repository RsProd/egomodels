<?php
function thumbnailGallery() {
	$photos = get_field('thumbnail-gallery', $_POST['post_id']);
	$result = [];

	foreach ($photos as $photo) {
		$result[] = $photo['sizes']['model'];
	}

	
	die(json_encode(!empty($result) ? $result : false));
}

add_action('wp_ajax_get_photos', 'thumbnailGallery');
add_action('wp_ajax_nopriv_get_photos', 'thumbnailGallery');

