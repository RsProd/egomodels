<?php

function get_article_categories($postID): array {
	$taxonomy = "category";
	$result = [];
	$terms = get_the_terms($postID, $taxonomy);

	if (!empty($terms)) {
		foreach ($terms as $term) {
			$result[] = [
				"name" => $term->name,
				"id" => $term->term_id,
				"link" => get_term_link((int)$term->term_id, $taxonomy),
			];
		}
	}
	return $result;
}

function get_the_terms_id($id, $taxonomy): array {
	$terms = get_the_terms($id, $taxonomy);
	$result = [];
	if (!empty($terms)) {
		foreach ($terms as $term) {
			$result[] = $term->term_id;
		}
	}
	return $result;
}

function getPosts($args, $format_date = 'Y.m.d'): array {
	$isAjax = wp_doing_ajax();
	$data = $isAjax ? $_POST : $args;

	$defaults = [
		"post_type" => "post",
		"order" => "DESC",
		"posts_per_page" => get_option('post_per_page'),
	];

	$parsed_data = wp_parse_args($data, $defaults);

	$q = new WP_Query();
	$posts = $q->query($parsed_data);

	$result = [
		'posts' => [],
		'total' => $q->found_posts,
		'debug' => $parsed_data
	];
	if (!empty($posts)):
		foreach ($posts as $post) {

			$result['posts'][] = [
				"id" => $post->ID,
				"title" => $post->post_title,
				"description" => $post->post_content ? wp_kses(do_limit($post->post_content, 10), []) : '',
				"date" => get_the_date($format_date, $post->ID),
				"categories" => get_article_categories($post->ID),
				"thumbnail" => has_post_thumbnail($post->ID) ? get_the_post_thumbnail_url($post->ID, 'large') : false,
				"link" => get_permalink($post->ID)
			];
		}
	endif;
	return $isAjax ? die(json_encode($result)) : $result;

}

add_action('wp_ajax_get_posts', 'getPosts');
add_action('wp_ajax_nopriv_get_posts', 'getPosts');
