<?php

add_filter('manage_posts_columns', 'thumbnail_column', 1);
add_action('manage_posts_custom_column', 'posts_custom_columns', 1, 2);
function thumbnail_column($columns) {
	$new = array();
	foreach ($columns as $key => $title) {
		if ($key == 'title')
			$new['thumbnail'] = __('Миниатюра', TEXT_DOMAIN);
		$new[$key] = $title;
	}
	return $new;
}

function posts_custom_columns($column_name, $id) {
	if ($column_name === 'thumbnail') {
		the_post_thumbnail('large', $id);
	}
}
