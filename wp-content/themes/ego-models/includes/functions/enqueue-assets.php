<?php

//Подключение стилей
function enqueue_styles() {
	wp_dequeue_style('wp-block-library');
	wp_enqueue_style('font-style');

	wp_enqueue_style('app', PRODUCTION ?
		assets("styles/app.min.css") :
		assets("styles/app.css"), false, PRODUCTION ? VER : time());
}

add_action('wp_enqueue_scripts', 'enqueue_styles');

//Подключение скриптов
function enqueue_script() {
	wp_deregister_script('jquery');
	wp_deregister_script('wp-embed');

	wp_enqueue_script('vendor', PRODUCTION ?
		assets('scripts/vendor.min.js') :
		assets('scripts/vendor.js'), false, PRODUCTION ? VER : time(), true);

	wp_enqueue_script('main', PRODUCTION ?
		assets("scripts/main.min.js") :
		assets("scripts/main.js"), array('vendor'), PRODUCTION ? VER : time(), true);
}

add_action('wp_enqueue_scripts', 'enqueue_script', 11);


//Подключение скриптов для Амин-панели
add_action('admin_enqueue_scripts', function () {
	wp_enqueue_style('main', assets('/styles/admin.css'), false, VER);
}, 99);
