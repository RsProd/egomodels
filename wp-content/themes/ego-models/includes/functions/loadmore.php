<?php

function loadMoreBlogs($args): array {
	$isAjax = wp_doing_ajax();
	$data = $isAjax ? [] : $args;

	$defaults = [
		"post_type" => "post",
		"order" => "DESC",
		"posts_per_page" => 9,
	];

	if (isset($_POST['offset'])) $data['offset'] = $_POST['offset'];
	if (isset($_POST['cat'])) $data['cat'] = $_POST['cat'];

	$parsed_data = wp_parse_args($data, $defaults);

	$q = new WP_Query();
	$posts = $q->query($parsed_data);

	$result = [
		'posts' 					=> [],
		'total' 					=> $q->found_posts,
		'posts_per_page' 	=> 9
	];	

	foreach ($posts as $post) {

		$tags_arr = array();
		$tags = get_tags();
		foreach ( $tags as $tag ) {
			$tag_link = get_tag_link( $tag->term_id );
			$tag_name = $tag->name;
			$tags_arr[] = [
				"tag_link" => $tag_link,
				"tag_name" => $tag_name
			];
		}

		$result['posts'][] = [
			"id" => $post->ID,
			"title" => $post->post_title,
			"photo" => has_post_thumbnail($post->ID) ? get_the_post_thumbnail_url($post->ID, 'large') : false,
			"link" => get_permalink($post->ID),
			"tags" => $tags_arr
		];
	}

	return $isAjax ? die(json_encode($result)) : $result;

}

add_action('wp_ajax_load_more_blogs', 'loadMoreBlogs');
add_action('wp_ajax_nopriv_load_more_blogs', 'loadMoreBlogs');
