<?php

//Debug
function debug($obj) {
	ob_start(); ?>
	<pre class="debug">
			<?php print_r($obj) ?>
		</pre>
	<?php return ob_get_clean();
}

function customScript($key) {
	ob_start();
	if (!empty($script = get_field($key, 'options'))): ?>
		<script>
		<?php echo $script ?>
		</script>
	<?php endif;
	return ob_get_clean();
}


function assets($file): string {
	return TH_PATH . '/assets/' . $file;
}

//Ограничение отрывка по словам
function do_limit($string, $word_limit): string {
	$words = explode(' ', $string, ($word_limit + 1));
	if (count($words) > $word_limit) {
		array_pop($words);
	}

	return implode(' ', $words) . ' ...';
}

//Получение иконок из svg спрайта
function svg($icon, $class): string {

	$version = PRODUCTION ? VER : time();

	return '<svg class="' . $class . ' ' . $class . '_' . $icon . '">
		<use xlink:href="' . assets('icons/sprite.svg?v'.$version.'#') . $icon . '"></use>
	</svg>';
}

function linkPhone($link): string {
	$link = preg_replace('/[^0-9]/', '', $link);

	return 'tel:+' . $link;
}

function linkNumber($prefix, $number): string {
	$link = preg_replace('/[^0-9]/', '', $number);

	return $prefix . $link;
}

function menuClass(): string {
	switch (true) {
		case is_singular('models'):
			return 'menu menu_primary menu_gradient-variable';
			break;
		case is_404():
			return 'menu menu_primary menu_gradient';
			break;
		default:
			return 'menu menu_primary';
	}
}
