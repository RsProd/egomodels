<?php function breadcrumbs($mod = false) {
	$page_posts = get_option('page_for_posts');
	$labels = [
		"home" => __('Главная', TEXT_DOMAIN)
	];

	$data[] = [
		"name" => $labels["home"],
		"link" => pll_home_url()
	];

	wp_list_comments();

	switch (true) {
		case is_home():
			$data[] = [
				"name" => get_the_title($page_posts),
				"link" => get_permalink($page_posts)
			];
			break;

		case is_search():

			$data[] = [
				"name" => sprintf(__('Поиск по запросу "%s"', TEXT_DOMAIN), $_GET["s"]),
				"class" => "breadcrumbs__item_small"
			];

			break;

		case is_category():
			$current_cat = get_queried_object();

			$data[] = [
				"name" => get_the_title($page_posts),
				"link" => get_permalink($page_posts)
			];

			$data[] = [
				"name" => $current_cat->name,
				"link" => get_term_link($current_cat->term_id)
			];
			break;
		case is_tax() || is_post_type_archive():
			$current_cat = get_queried_object();

			if (is_tax()) {

				$post_type = get_post_type_object('models');

				$data[] = [
					"name" => $post_type->labels->name,
					"link" => get_post_type_archive_link(get_post_type())
				];

				if ($current_cat->parent) {
					$parrent_term = get_term($current_cat->parent);

					$data[] = [
						"name" => $parrent_term->name,
						"link" => get_term_link($parrent_term->term_id)
					];
				}
				$data[] = [
					"name" => $current_cat->name,
					"link" => get_term_link($current_cat->term_id),
				];
			} else {
				$data[] = [
					"name" => post_type_archive_title('', false),
					"link" => get_post_type_archive_link(get_post_type())
				];
			}
			break;
		case is_page():
			$data[] = [
				"name" => get_the_title(),
				"link" => get_permalink()
			];
			break;

		case is_singular('post'):
			$term = get_the_terms(get_the_ID(), 'category');

			if ($page_posts) {
				$data[] = [
					"name" => get_the_title($page_posts),
					"link" => get_permalink($page_posts)
				];
			}

			if (!empty($term)) {
				$data[] = [
					"name" => $term[0]->name,
					"link" => get_term_link($term[0]->term_id)
				];
			}

			$data[] = [
				"name" => get_the_title(),
				"link" => get_permalink()
			];
			break;

		case is_singular('models'):
			$post_type = get_post_type_object('models');


			$data[] = [
				"name" => $post_type->labels->name,
				"link" => get_post_type_archive_link(get_post_type())
			];

			$term = get_the_terms(get_the_ID(), 'catalog');

			if (!empty($term)) {
				$data[] = [
					"name" => $term[0]->name,
					"link" => get_term_link($term[0]->term_id)
				];
			}
			$data[] = [
				"name" => get_the_title(),
				"link" => get_permalink()
			];
			break;
	}

	if (!empty($data)): ?>
		<div class="breadcrumbs <?php echo $mod ? $mod : ''; ?>">
			<?php foreach ($data as $link): ?>
				<div class="breadcrumbs__item <?php echo isset($link['class']) ? $link['class'] : '' ?>">
					<?php if ($link !== end($data)): ?>
						<a class="breadcrumbs__link" href="<?php echo $link['link'] ?>"><?php echo $link['name'] ?></a>
					<?php else: ?>
						<span><?php echo $link['name'] ?></span>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif;
}



