<?php

add_action('phpmailer_init', 'smtp_phpmailer_init');

function smtp_phpmailer_init($phpmailer) {
	$settings = get_field('smtp', 'options');

	$phpmailer->IsSMTP();

	$phpmailer->CharSet = 'UTF-8';

	$phpmailer->Host = $settings['host'];
	$phpmailer->Username = $settings['username'];
	$phpmailer->Password = $settings['password'];
	$phpmailer->SMTPAuth = true;
	$phpmailer->SMTPSecure = 'ssl';

	$phpmailer->Port = (int)$settings['port'];
	$phpmailer->From = $settings['from'];
	$phpmailer->FromName = $settings['fromname'];

	$phpmailer->isHTML(true);
}

function uploadsFile($files): array {
	$result = [];
	$uploads_dir = get_temp_dir();
	if (isset($files["error"])):
		foreach ($files["error"] as $key => $error) {
			if ($error == UPLOAD_ERR_OK) {
				$tmp_name = $files["tmp_name"][$key];
				$name = basename($files["name"][$key]);
				if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
					$result[] = "$uploads_dir$name";
				}
			}
		}
	endif;
	return $result;
}


function getMessages($data): string {
	$result = '';

	$labels = [
		"abb-name" => "ФИО",
		"date-of-birth" => "Дата рождения",
		"full-number" => "Номер телефона",
		"email" => "Почтовый адрес",
		"country" => "Страна",
		"city" => "Город",
		"social" => "Социальные сети",
		"height" => "Рост",
		"waist" => "Вес",
		"bust" => "Грудь",
		"hips" => "Талия",
		"price" => "Стоимость бронирования",
		"motivation" => "Почему хочет стать моделью",
		"affiliate-name" => "ФИО человека который, помогал заполянять анкету",
		"affiliate-phone" => "Телефон человека который, помогал заполянять анкету",
		"message" => "Сообщение",
		"models" => "Список выбранных моделей"
	];

	$ignore_fields = ['action', 'accept', 'affiliate', 'form-name', 'from', 'personal', 'copy', 'models', 'number'];

	foreach ($data as $key => $field) {
		if (!empty($field) && !in_array($key, $ignore_fields)) {
			$result .= "<p><strong>" . $labels[$key] . "</strong>: " . $field . "</p>\n";
		}

		if ($key == "models") {
			$modelsStr = '';
			$models = getModelsForID($field);
			$i = 1;
			foreach ($models as $model) {
				$sep = $i !== count($models) ? ', ' : '';
				$modelsStr .= "<a href='" . $model['link'] . "'>" . $model['name'] . "</a>" . $sep;
				$i++;
			}
			$result .= "<p><strong>" . $labels[$key] . "</strong>: " . $modelsStr . "</p>\n";
		}
	}

	return $result;
}

function send_from($post): array {
	$emails = [];

	!isset($post['from']) ?
		$emails[] = get_field('emails', 'options')['default'] :
		$emails[] = $post['from'];

	if (isset($post['copy'])) $emails[] = $post['email'];

	return $emails;
}

function send_forms() {
	$to = send_from($_POST);
	$subject = sprintf('%s - письмо от %s (%s)', $_POST['abb-name'], get_bloginfo('name'), $_POST['form-name']);
	$message = getMessages($_POST);
	$attachment = uploadsFile($_FILES['photos']);

	$result = wp_mail($to, $subject, $message, false, $attachment);

	$result ?
		die(json_encode(__('Сообщение успешно отправлено', TEXT_DOMAIN))) :
		die(json_encode(__('Ошибка! Что-то пошло не так...', TEXT_DOMAIN)));
}

add_action('wp_ajax_send_form', 'send_forms');
add_action('wp_ajax_nopriv_send_form', 'send_forms');