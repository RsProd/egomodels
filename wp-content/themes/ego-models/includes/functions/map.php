<?php
add_shortcode('g-maps', 'shortcode_map');

function shortcode_map($atts) {
	$key = get_field('gmaps-apikey', 'options');
	$options = get_field('map', 'options');
	$route = get_field('route', 'options');

	$atts = shortcode_atts(array(
		'width' => '100%',
		'height' => '100%'
	), $atts);


	ob_start();
	if (!empty($key) && !empty($options)): ?>
		<div class="map">
			<?php if (!empty($route)): ?>
				<a class="map__route" href="<?php echo $route ?>"
				   target="_blank">
					<span><?php _e('Проложить маршрут', TEXT_DOMAIN) ?></span>
					<?php echo svg('route', 'map__icon') ?>
				</a>
			<?php endif; ?>
			<div class="gmap-init"
			     style="width: <?php echo $atts['width'] ?>; height: <?php echo $atts['height'] ?>;">
			</div>
		</div>
	<?php endif;
	return ob_get_clean();
}

?>