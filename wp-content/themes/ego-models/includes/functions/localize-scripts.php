<?php

function enqueue_localize_script() {
	wp_localize_script('vendor', 'path', array(
		'ajax' => admin_url('admin-ajax.php'),
		'theme' => get_stylesheet_directory_uri()
	));

	if (is_single() || is_page()) {
		wp_localize_script('vendor', 'comments', array(
			'post_id' => get_the_ID()
		));
	}
	wp_localize_script('vendor', 'CalendarOptions', array(
		"labels" => [
			"months" => [
				__("Января", TEXT_DOMAIN),
				__("Февраля", TEXT_DOMAIN),
				__("Марта", TEXT_DOMAIN),
				__("Апреля", TEXT_DOMAIN),
				__("Мая", TEXT_DOMAIN),
				__("Июня", TEXT_DOMAIN),
				__("Июля", TEXT_DOMAIN),
				__("Августа", TEXT_DOMAIN),
				__("Сентября", TEXT_DOMAIN),
				__("Октября", TEXT_DOMAIN),
				__("Ноября", TEXT_DOMAIN),
				__("Декабря", TEXT_DOMAIN)
			],
			"days" => [
				__("Вс", TEXT_DOMAIN),
				__("Пн", TEXT_DOMAIN),
				__("Вт", TEXT_DOMAIN),
				__("Ср", TEXT_DOMAIN),
				__("Чт", TEXT_DOMAIN),
				__("Пт", TEXT_DOMAIN),
				__("Сб", TEXT_DOMAIN),
			]
		],
		"general" => (array)get_field('gc-general-settings', 'options'),
		"service" => (array)get_field('gc-settings', 'options'),
		"prices" => (array)get_field('gc-prices', 'options')
	));

	wp_localize_script('vendor', 'GMaps', array(
		'api_key' => get_field('gmaps-apikey', 'options'),
		'options' => get_field('map', 'options')
	));

	wp_localize_script('vendor', 'modelForm', array(
		'formErrors' => [
			"fieldRequired" => __("Это поле является обязательным", TEXT_DOMAIN),
			"minLength" => __("Минимальное значение 3 символа", TEXT_DOMAIN),
			"invalidNumber" => __("Неверный формат телефона", TEXT_DOMAIN),
			"wrongEmail" => __("Неверный формат електронной почты", TEXT_DOMAIN)
		],
		"modelFormSubmitBtnText" => __("Отправить", TEXT_DOMAIN),
		"modelFormNextStepBtnText" => __("Продолжить", TEXT_DOMAIN),
		"modelFormSuccessText" => __("Ваше сообщение успешно отправлено", TEXT_DOMAIN),
		"modelFormErrorText" => __("При отправке сообщения возникли технические проблемы, напишите, пожалуйста, позже", TEXT_DOMAIN)
	));

	wp_localize_script('vendor', 'loadMore', array(
		'models' => is_tax('catalog') ? get_queried_object()->term_id : "",
		'blogs' => is_category() ? get_queried_object()->term_id : "",
	));

	wp_localize_script('vendor', 'user', array(
		'avatar' => getAvatar()
	));

	wp_localize_script('vendor', 'notifyPopup', array(
		'message' => __('добавлена в список избранные', TEXT_DOMAIN),
		'link' => [
			'title' => __('Перейти к списку', TEXT_DOMAIN),
			'href' => get_permalink(pll_get_post(954))
		]
	));

}

add_action('wp_enqueue_scripts', 'enqueue_localize_script', 12);
