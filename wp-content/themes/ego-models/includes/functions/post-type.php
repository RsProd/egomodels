<?php
add_action('init', 'models_init');

function models_init(){
	register_post_type('models', array(
		'labels'             => array(
			'name'               => __('Модели', TEXT_DOMAIN),
			'singular_name'      => __('Модель', TEXT_DOMAIN),
			'add_new'            => __('Добавить анкету', TEXT_DOMAIN),
			'add_new_item'       => __('Добавить новую анкету', TEXT_DOMAIN),
			'edit_item'          => __('Редактировать анкету', TEXT_DOMAIN),
			'new_item'           => __('Новая анкета', TEXT_DOMAIN),
			'view_item'          => __('Посмотреть анкету', TEXT_DOMAIN),
			'search_items'       => __('Найти анкету', TEXT_DOMAIN),
			'not_found'          => __('Анкет не найдено', TEXT_DOMAIN),
			'not_found_in_trash' => __('Анкет не найдено', TEXT_DOMAIN),
			'menu_name'          => __('Модели', TEXT_DOMAIN)

		  ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'query_var'          => true,
		'menu_icon'          => 'dashicons-universal-access',
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title','editor','author','thumbnail','excerpt','comments', 'page-attributes')
	) );
}


add_action( 'init', 'taxonomy_models_init' );

function taxonomy_models_init() {
	register_taxonomy( 'catalog', [ 'models' ], [
		'labels'              => array(
			'name'              => __('Категории моделей', TEXT_DOMAIN),
			'singular_name'     => __('Категория', TEXT_DOMAIN),
			'search_items'      => __('Поиск категории', TEXT_DOMAIN),
			'all_items'         => __('Все категории', TEXT_DOMAIN),
			'view_item '        => __('Просмотр', TEXT_DOMAIN),
			'parent_item'       => __('Родительская категория', TEXT_DOMAIN),
			'parent_item_colon' => __('Родительская категория:', TEXT_DOMAIN),
			'edit_item'         => __('Редактировать категорию', TEXT_DOMAIN),
			'update_item'       => __('Обновить категорию', TEXT_DOMAIN),
			'add_new_item'      => __('Добавить новую категорию', TEXT_DOMAIN),
			'new_item_name'     => __('Новая категория', TEXT_DOMAIN),
			'menu_name'         => __('Категории', TEXT_DOMAIN),
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true,
		'meta_box_cb'           => null,
		'show_admin_column'     => true,
		'show_in_rest'          => null,
		'rest_base'             => null,
	] );
}

add_action( 'init', 'location_models_init' );

function location_models_init() {
	register_taxonomy( 'locations', [ 'models' ], [
		'labels'              => array(
			'name'              => __('Локации', TEXT_DOMAIN),
			'singular_name'     => __('Локация', TEXT_DOMAIN),
			'search_items'      => __('Поиск локаций', TEXT_DOMAIN),
			'all_items'         => __('Все локации', TEXT_DOMAIN),
			'view_item '        => __('Просмотр локации', TEXT_DOMAIN),
			'parent_item'       => __('Родительская локация', TEXT_DOMAIN),
			'parent_item_colon' => __('Родительская локация:', TEXT_DOMAIN),
			'edit_item'         => __('Редактировать локацию', TEXT_DOMAIN),
			'update_item'       => __('Обновить локацию', TEXT_DOMAIN),
			'add_new_item'      => __('Добавить новую локацию', TEXT_DOMAIN),
			'new_item_name'     => __('Новая локация', TEXT_DOMAIN),
			'menu_name'         => __('Локации', TEXT_DOMAIN),
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true,
		'meta_box_cb'           => null,
		'show_admin_column'     => true,
		'show_in_rest'          => null,
		'rest_base'             => null,
	] );
}
