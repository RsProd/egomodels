<?php

//Добавление сео атрибутов к внешним ссылкам
add_filter('the_content', 'true_wp_posts_nofollow');
add_filter('acf/load_value/name=editor', 'true_wp_posts_nofollow');
add_filter('acf/load_value/name=desc', 'true_wp_posts_nofollow');

//Добавление seo тега rel
function true_wp_posts_nofollow($content): string {
	$content = preg_replace_callback(
		'|<a (.+?)>|i',
		function ($matches) {
			return wp_rel_callback($matches, 'noreferrer noopener nofollow');
		},
		$content
	);
	return stripslashes($content);
}
