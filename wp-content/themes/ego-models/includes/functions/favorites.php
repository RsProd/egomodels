<?php function getFavorites(): array {
	return isset($_COOKIE['favorites']) ? json_decode($_COOKIE['favorites'], true) : [];
}
