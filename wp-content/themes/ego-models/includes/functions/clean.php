<?php
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

//Удаление id из пунктов меню
add_filter('nav_menu_item_id', 'remove_nav_menu_item_id', 10, 3);
function remove_nav_menu_item_id($id, $item, $args): string {
	return "";
}

//Удаление лишних css классов из пунктов меню
add_filter('nav_menu_css_class', 'clean_nav_menu_item_class', 10, 3);
function clean_nav_menu_item_class($classes, $item, $args): array {
	return [];

}

add_filter('style_loader_tag', 'remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'remove_type_attr', 10, 2);

function remove_type_attr($tag, $handle) {
	return preg_replace("/ type=['\"]text\/(javascript|css)['\"]/", '', $tag);
}

function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'remove_admin_login_header');