<?php
function getRangeValue($field): array {
	global $wpdb;

	$full_field = sprintf('parameters_%s', $field);

	$result = $wpdb->get_results($wpdb->prepare(
		"SELECT min(cast(pm.meta_value as int)) as min_value, max(cast(pm.meta_value as int)) as max_value
				FROM wp_postmeta as pm
				LEFT JOIN wp_posts as p
				ON p.ID = pm.post_id 
				LEFT JOIN wp_term_relationships as rel
				ON rel.object_id = p.id
				LEFT JOIN wp_term_taxonomy as tt
				ON tt.term_taxonomy_id = rel.term_taxonomy_id
				LEFT JOIN wp_terms as terms
				ON terms.term_id = tt.term_id
				WHERE pm.meta_key = %s AND
				p.post_status = 'publish' AND
				terms.slug = %s",
		$full_field, LANG
	));

	return [
		"min" => $result[0]->min_value,
		"max" => $result[0]->max_value
	];

}

function getModelsForID($id): array {
	$args = [
		'post_type' => 'models',
		'include' => $id
	];

	$models = get_posts($args);
	$result = [];
	if (!empty($models)) {
		foreach ($models as $model) {
			setup_postdata($model);
			$result[] = [
				"id" => $model->ID,
				"photo" => has_post_thumbnail($model->ID) ? get_the_post_thumbnail_url($model->ID, 'large') : false,
				"name" => $model->post_title,
				"parameters" => get_field('parameters', $model->ID),
				"link" => get_permalink($model->ID)
			];

		}
		wp_reset_postdata();
	}
	return $result;
}

function getModels($args): array {
	$isAjax = wp_doing_ajax();
	$data = $isAjax ? [] : $args;

	$range = ["bust", "waist", "height", "hips", "shoe"];
	$terms = ["catalog", "locations"];

	$defaults = [
		"post_type" => "models",
		"post_status" => "publish",
		"posts_per_page" => 9,
	];

	if (isset($_POST['offset'])) {
		$data['offset'] = $_POST['offset'];
	}

	if (isset($_POST['filter'])) {

		$meta = ["relation" => "AND"];

		foreach ($_POST['filter'] as $key => $item) {

			if (in_array($key, $range)) {
				$meta[$key] = ["relation" => "AND"];

				$meta[$key][] = [
					"key" => 'parameters_' . $key,
					"value" => $item,
					"compare" => "BETWEEN",
					"type" => "NUMERIC"
				];

			} else if (in_array($key, $terms)) {

				$data["tax_query"][] = [
					"taxonomy" => $key,
					"terms" => $item,
					"relation" => "OR",
					"field" => "term_id",
					"type" => "NUMERIC",
				];

			} else {

				$meta[$key] = ["relation" => "OR"];
				$meta[$key] = [
					"key" => $key,
					"value" => $item,
					"compare" => "IN",
				];

			}
		}

		if (!empty($meta)) {
			$data["meta_query"] = $meta;
		}

	}


	$parsed_data = wp_parse_args($data, $defaults);

	$q = new WP_Query();
	$posts = $q->query($parsed_data);

	$result = [
		'posts' => [],
		'total' => $q->found_posts,
		'debug' => $parsed_data
	];

	foreach ($posts as $post) {

		$result['posts'][] = [
			"id" => $post->ID,
			"name" => $post->post_title,
			"photo" => has_post_thumbnail($post->ID) ? get_the_post_thumbnail_url($post->ID, 'model') : false,
			"gallery" => get_field('thumbnail-gallery', $post->ID),
			"link" => get_permalink($post->ID),
			"favorite" => in_array($post->ID, getFavorites()),
			"parameters" => get_field('parameters', $post->ID),
			"optional" => [
				"new" => !empty(get_field('new', $post->ID)) ? get_field('new', $post->ID) : false,
				"departure" => !empty(get_field('departure', $post->ID)) ? get_field('departure', $post->ID) : false,
				"models_link" => !empty(get_field('models_link', $post->ID)) ? get_field('models_link', $post->ID) : false,
			]
		];
	}

	return $isAjax ? die(json_encode($result)) : $result;

}

add_action('wp_ajax_get_models', 'getModels');
add_action('wp_ajax_nopriv_get_models', 'getModels');
