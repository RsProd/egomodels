<?php function colorScheme(): string {
	return isset($_COOKIE['scheme']) ? $_COOKIE['scheme'] : get_field('default-color-scheme', 'options');
}
