<?php

add_action('after_setup_theme', function () {
	load_theme_textdomain(TEXT_DOMAIN, get_template_directory() . '/languages');
});

if (function_exists('add_theme_support')) {
	add_theme_support('title-tag');
	add_theme_support('menus');
	add_theme_support('post-thumbnails', array('page', 'post', 'models'));
}

register_nav_menus(array(
	'primary' => 'Основное меню',
	'second' => 'Дополнительное меню'
));

if (function_exists('add_image_size')) {
	add_image_size('model', 360, 465, true);
}

add_filter('mime_types', 'webp_upload_mimes');
function webp_upload_mimes($existing_mimes) {
	$existing_mimes['webp'] = 'image/webp';
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}

if (function_exists('acf_add_options_page')) {

	$page_our_team = acf_add_options_page([
		'page_title' => 'Наша команда',
		'menu_title' => 'Наша команда',
		'menu_slug' => 'our-team',
		'capability' => 'edit_posts',
		'position' => 30,
		'icon_url' => 'dashicons-groups',
		'redirect' => false
	]);

	$page_google_settings = acf_add_options_page(array(
		'page_title' => 'Google',
		'menu_title' => 'Google',
		'menu_slug' => 'g-calendar',
		'capability' => 'edit_posts',
		'position' => 80,
		'icon_url' => 'dashicons-google',
		'redirect' => true
	));

	$page_google_settings_sub_calendar = acf_add_options_sub_page(array(
		'page_title' => 'Google Calendar',
		'menu_title' => 'Google Calendar',
		'parent_slug' => $page_google_settings['menu_slug'],
	));

	$page_google_settings_sub_maps = acf_add_options_sub_page(array(
		'page_title' => 'Google Maps',
		'menu_title' => 'Google Maps',
		'parent_slug' => $page_google_settings['menu_slug'],
	));

	$page_parameters = acf_add_options_page(array(
		'page_title' => 'Параметры',
		'menu_title' => 'Параметры',
		'menu_slug' => 'theme-general-settings',
		'icon_url' => 'dashicons-screenoptions',
		'capability' => 'edit_posts',
		'position' => 90,
		'redirect' => true
	));

	$page_parameters_sub_logo = acf_add_options_sub_page(array(
		'page_title' => 'Общие',
		'menu_title' => 'Общие',
		'parent_slug' => $page_parameters['menu_slug'],
	));

	$page_parameters_sub_social = acf_add_options_sub_page(array(
		'page_title' => 'Социальные сети',
		'menu_title' => 'Социальные сети',
		'parent_slug' => $page_parameters['menu_slug'],
	));

	$page_parameters_sub_share = acf_add_options_sub_page(array(
		'page_title' => 'Поделиться',
		'menu_title' => 'Поделиться',
		'parent_slug' => $page_parameters['menu_slug'],
	));

	$page_parameters_sub_forms = acf_add_options_sub_page(array(
		'page_title' => 'Настройки форм',
		'menu_title' => 'Настройки форм',
		'parent_slug' => $page_parameters['menu_slug'],
	));

	$page_parameters_contacts = acf_add_options_sub_page(array(
		'page_title' => 'Контакты',
		'menu_title' => 'Контакты',
		'parent_slug' => $page_parameters['menu_slug'],
	));

	$page_parameters_scripts = acf_add_options_sub_page(array(
		'page_title' => 'Пользовательские скрипты',
		'menu_title' => 'Пользовательские скрипты',
		'parent_slug' => $page_parameters['menu_slug'],
	));
}


function google_key_acf() {
	acf_update_setting('google_api_key', get_field('gmaps-apikey', 'options'));
}

add_action('acf/init', 'google_key_acf');
