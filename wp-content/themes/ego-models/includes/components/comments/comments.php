<?php

function count_parent_comments($id) {
	global $wpdb;
	$query = "SELECT COUNT(comment_post_id) AS count FROM $wpdb->comments WHERE `comment_approved` = 1 AND `comment_post_ID` = $id AND `comment_parent` = 0";
	$parents = $wpdb->get_row($query);
	return $parents->count;
}

function setLikeComments() {
	$commentID = $_POST['comment_id'];
	$ip = $_SERVER['REMOTE_ADDR'];

	$like_user_ip = (array)get_comment_meta($commentID, 'like_user_ip', true);
	$like = (int)get_comment_meta($commentID, 'like_count', true);

	if (!in_array($ip, $like_user_ip)) {
		$like++;
		array_push($like_user_ip, $ip);
	} else {
		$like--;
		unset($like_user_ip[array_search($ip, $like_user_ip)]);
	}

	update_comment_meta($commentID, 'like_user_ip', $like_user_ip);
	update_comment_meta($commentID, 'like_count', $like);

	die(json_encode($like));
}

add_action('wp_ajax_set_like', 'setLikeComments');
add_action('wp_ajax_nopriv_set_like', 'setLikeComments');

function getLikeCommentsData($commentID): array {

	$ip = $_SERVER['REMOTE_ADDR'];
	$like = (int)get_comment_meta($commentID, 'like_count', true);

	$like_ip = (array)get_comment_meta($commentID, 'like_user_ip', true);

	return [
		"user_ip" => $like_ip,
		"liked" => in_array($ip, $like_ip),
		"count" => $like
	];
}

function getAvatar() {
	if (is_user_logged_in()) {
		$userID = get_current_user_id();
		return get_field('photo', 'user_' . $userID)['sizes']['thumbnail'];
	}
}

function getComments($post_id): array {

	$postID = wp_doing_ajax() ? $_POST['post_id'] : $post_id;

	$args = array(
		'orderby' => 'comment_date',
		'order' => 'DESC',
		'post_id' => $postID,
		'status' => 'approve',
		'hierarchical' => 'threaded',
		'offset' => wp_doing_ajax() ? $_POST['offset'] : 0,
		'number' => 3
	);

	$data = get_comments($args);

	function setItem($item): array {
		return [
			"id" => $item->comment_ID,
			"date" => get_comment_date('j M | H:i', $item->comment_ID),
			"author" => $item->comment_author,
			"photo" => get_comment_meta($item->comment_ID, 'photo', true),
			"email" => $item->comment_author_email,
			"rating" => get_comment_meta($item->comment_ID, 'rating', true),
			"comment" => !empty($item->comment_content) ? $item->comment_content : '',
			"like" => getLikeCommentsData($item->comment_ID)
		];
	}

	function setData($obj): array {
		$result = [];
		$i = 0;

		foreach ($obj as $item) {
			$result[$i] = setItem($item);

			if (!empty($item->get_children())) {
				$result[$i]['children'] = setData($item->get_children());
			}
			$i++;
		}

		return $result;
	}

	$result = [
		"comments" => setData($data),
		"count" => count_parent_comments($postID),
		"all_count" => wp_count_comments($postID)->approved,
	];

	if (wp_doing_ajax()) {
		die(json_encode($result));
	} else {
		return $result;
	}
}

add_action('wp_ajax_get_comments', 'getComments');
add_action('wp_ajax_nopriv_get_comments', 'getComments');

function sendComment() {
	$comment_data = array(
		'comment_post_ID' => $_POST['post_id'],
		'author' => $_POST['abb-name'],
		'email' => isset($_POST['email']) ? $_POST['email'] : "no-reply@ego-models.com",
		'comment' => $_POST['comment'],
	);

	if (isset($_POST['reply'])) $comment_data['comment_parent'] = $_POST['reply'];

	if (!is_user_logged_in()) {
		if ($_POST['abb-name']) {
			$result = wp_handle_comment_submission($comment_data);
		} else {
			wp_send_json_error(__('Заполните обязательные поля', TEXT_DOMAIN));
		}
	} else {
		$result = wp_handle_comment_submission($comment_data);
	}

	if (is_wp_error($result)) {
		wp_send_json_error($result->get_error_message());
	} else {

		if (!empty($_POST['photo'])) update_comment_meta($result->comment_ID, 'photo', $_POST['photo']);
		if (!empty($_POST['rating'])) update_comment_meta($result->comment_ID, 'rating', $_POST['rating']);

		if (!is_user_logged_in()) {
			wp_send_json_success(__('Ваш комментарий отправлен на модерацию', TEXT_DOMAIN));
		} else {
			wp_send_json_success(__('Ваш комментарий был опубликован', TEXT_DOMAIN));
		}
	}
	die();
}

add_action('wp_ajax_send_comment', 'sendComment');
add_action('wp_ajax_nopriv_send_comment', 'sendComment');


function listComments($array, $item_class = '', $sub_item_class = '', $template_parts = '') {
	foreach ($array as $key => $item) {
		echo "<div class='" . $item_class . "'>";
		get_template_part($template_parts, false, $item);
		if (isset($item['children']) && is_array($item['children'])) {
			listComments($item['children'], $sub_item_class, $sub_item_class, $template_parts);
		}
		echo "</div>";
	}
}