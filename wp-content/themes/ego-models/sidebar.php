<?php

$current_category = is_tax('catalog') ? get_queried_object()->term_id : "";

$categories = get_all_terms('catalog', 'field');
$locations = get_all_terms('locations', 'abc');

$fields = get_field_objects();

$translation_params = [
	"height" => __('Рост', TEXT_DOMAIN),
	"hips" => __('Бедра', TEXT_DOMAIN),
	"waist" => __('Талия', TEXT_DOMAIN),
	"bust" => __('Грудь', TEXT_DOMAIN),
	"shoe" => __('Размер обуви', TEXT_DOMAIN),
];

$translation_gender = [
	1 => __('Мужской', TEXT_DOMAIN),
	2 => __('Женский', TEXT_DOMAIN)
];

if (!empty($categories) || !empty($fields)): ?>
	<div class="sidebar">
		<form class="filter">
			<span class="filter-button filter-button_close"
			      data-toggle=".filter"
			      data-toggle-body="filter-open"><?php _e('Фильтр', TEXT_DOMAIN) ?></span>
			<div class="filter__content">
				<?php if (!empty($categories)): ?>
					<div class="filter__block">
						<div
							class="filter__title <?php echo !empty($current_category) ? 'active' : '' ?>"><?php _e('Категории', TEXT_DOMAIN) ?></div>
						<div class="filter__slide">
							<div class="filter__inner">
								<?php foreach ($categories as $category): ?>
									<div class="filter__item">
										<label
											class="checkbox <?php echo $current_category == $category['id'] ? 'checkbox_disabled' : '' ?>">
											<input class="checkbox__input"
											       name="catalog"
											       type="checkbox"
											       value="<?php echo $category['id'] ?>"
												<?php echo $current_category == $category['id'] ? 'checked' : '' ?>/>
											<span
												class="checkbox__label checkbox__label_filter"><?php echo $category['name'] ?>
												<?php if ($current_category == $category['id']): ?>
													<sub>(Current)</sub>
												<?php endif; ?>
												</span>
										</label>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif;
				if (!empty($locations)): ?>
					<div class="filter__block">
						<div class="filter__title"><?php _e('Локация модели', TEXT_DOMAIN) ?></div>
						<div class="filter__slide">
							<div class="filter__inner">
								<input class="filter__input search-input" name="search-location"
								       placeholder="<?php _e('Поиск по локации', TEXT_DOMAIN) ?>" type="text" />
								<ul class="filter__scroll search-list">
									<?php foreach ($locations as $location): ?>
										<li class="filter__item">
											<label class="checkbox">
												<input class="checkbox__input" name="locations" type="checkbox"
												       value="<?php echo $location['id'] ?>" />
												<span
													class="checkbox__label checkbox__label_filter"><?php echo $location['name'] ?></span>
											</label>
											<?php if (!empty($location['children'])): ?>
												<ul class="filter__sub-list">
													<?php foreach ($location['children'] as $location_child): ?>
														<li class="filter__item">
															<label class="checkbox ">
																<input class="checkbox__input" name="locations"
																       type="checkbox"
																       value="<?php echo $location_child['id'] ?>" />
																<span
																	class="checkbox__label checkbox__label_filter"><?php echo $location_child['name'] ?></span>
															</label>
														</li>
													<?php endforeach; ?>
												</ul>
											<?php endif; ?>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				<?php endif;
				if (!empty($fields['gender'])): ?>
					<div class="filter__block">
						<div class="filter__title"><?php _e('Пол', TEXT_DOMAIN) ?></div>
						<div class="filter__slide">
							<div class="filter__inner">
								<?php foreach ($fields['gender']['choices'] as $key => $gender): ?>
									<div class="filter__item">
										<label class="checkbox">
											<input class="checkbox__input" name="gender" type="checkbox"
											       value="<?php echo $key ?>" />
											<span
												class="checkbox__label checkbox__label_filter"><?php echo $translation_gender[$key] ?></span>
										</label>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif;
				if (!empty($fields['parameters'])): ?>
					<div class="filter__block">
						<div class="filter__title"><?php _e('Параметры', TEXT_DOMAIN) ?></div>
						<div class="filter__slide">
							<div class="filter__inner">
								<?php foreach ($fields['parameters']['sub_fields'] as $field):
									$values = getRangeValue($field['name']);
									if ($values['min'] != $values['max']): ?>
										<div class="sub-block">
											<div
												class="sub-block__title"><?php echo $translation_params[$field['name']] ?></div>
											<div class="rangeSlider"
											     data-min="<?php echo $values['min'] ?>"
											     data-max="<?php echo $values['max'] ?>"
											     data-name="<?php echo $field['name'] ?>">

												<div class="rangeSlider__labels">
													<span class="rangeSlider__label"></span>
													<span class="rangeSlider__label"></span>
												</div>
												<div class="rangeSlider__inner"></div>
											</div>
										</div>
									<?php endif;
								endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="apply-filter" data-toggle=".filter" data-toggle-body="filter-open">
				<span class="apply-filter__icon"></span>
				<span class="apply-filter__label"><?php _e('Применить', TEXT_DOMAIN) ?></span>
			</div>
			<div class="reset-filter">
				<div class="reset-filter__btn"></div>
				<div class="reset-filter__label"><?php _e('Сбросить все фильтры', TEXT_DOMAIN) ?></div>
			</div>
		</form>
	</div>
<?php endif; ?>
