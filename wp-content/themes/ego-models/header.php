<!doctype html>
<html lang="<?php echo LOCALE ?>" class="<?php echo colorScheme() ?>">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<?php wp_head(); ?>
	<?php echo customScript('before_head') ?>
</head>

<body <?php body_class('preload'); ?>>
<?php echo customScript('after_body') ?>
<div class="preloader preloader_load">
	<div class="preloader__part"><span></span></div>
	<div class="preloader__part"><span></span></div>
</div>
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<div class="panel">
					<?php if (!empty($logo = get_field('logo', 'options'))): ?>
						<a class="logo" href="<?php echo get_home_url() ?>">
							<img class="logo__icon"
							     src="<?php echo $logo ?>"
							     alt="<?php echo get_bloginfo('name') ?>">
						</a>
					<?php endif; ?>
					<div class="btn-menu"
					     data-toggle=".navigation"
					     data-toggle-body="menu-open">
						<span></span>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-8">
				<nav class="navigation">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'primary',
						'container' => 'ul',
						'menu_class' => menuClass()
					));
					?>
					<?php get_template_part('templates/common/block', 'social', [
						"container" => "social_header"
					]) ?>
					<?php get_template_part('templates/common/block', 'switcher-mobile'); ?>
				</nav>
			</div>
			<div class="col-lg-2 col-md-2">
				<div class="group-panel">
					<?php
						get_template_part('templates/common/block', 'switcher-desktop');
						get_template_part('templates/common/block', 'buttons');
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="chats-button" data-toggle=".form-container" data-toggle-body="popup-open">
		<?php echo svg('chats', 'chats-button__icon') ?>
		<span class="chats-button__label"><?php _e('Связаться с нами', TEXT_DOMAIN) ?></span>
	</div>
	<?php get_template_part('templates/common/block', 'button-scheme') ?>
</header>