<footer class="footer">
	<?php
	echo get_search_form();
	get_template_part('templates/common/block', 'contact-form');
	get_template_part('templates/common/block', 'cookies-popup');

	if (is_page('favorites')) {
		get_template_part('templates/favorites/block', 'form');
	}
	?>
	<div class="container">
		<div class="up-btn">
			<?php echo svg('up', 'up-btn__icon') ?>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
				<a class="logo logo_footer" href="/">
					<img class="logo__icon" src="<?php the_field('logo', 'options') ?>" alt="<?php bloginfo('name') ?>">
				</a>
			</div>
			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-center last-md last-sm last-xs">
				<?php
				wp_nav_menu(array(
					'theme_location' => 'primary',
					'container' => 'ul',
					'menu_class' => 'menu menu_second'
				));
				?>
				<div class="block-group">
					<div class="copyright">
						&copy; <?php echo sprintf(__('%s-%s %s Все права защищены.', TEXT_DOMAIN), 2007, date('Y'), 'EgoModels TM.') ?></div>
					<?php
					wp_nav_menu(array(
						'theme_location' => 'second',
						'container' => 'ul',
						'menu_class' => 'menu menu_additionally'
					));
					?>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
				<?php get_template_part('templates/common/block', 'social', [
					"container" => "social_static",
					"item" => "social__item_small"
				]) ?>
			</div>
		</div>
	</div>
</footer>
<?php
wp_footer();
echo customScript('before_body');
?>
</body>

</html>
