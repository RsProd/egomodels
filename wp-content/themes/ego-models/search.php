<?php get_header();
$per_page = 9;

$search_results = getPosts([
	"posts_per_page" => $per_page,
	"post_type" => array('post', 'models', 'page'),
	"s" => $_GET['s']
]);

?>
<main class="main" role="main">
	<section class="section">
		<div class="container relative">
			<span class="stroke-placeholder stroke-placeholder_blog">Search</span>
			<div class="page-head">
				<?php breadcrumbs(); ?>
			</div>
			<?php if (!empty($search_results['posts'])): ?>
			<div class="row">
				<?php foreach ($search_results['posts'] as $search_result): ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<?php get_template_part('templates/loops/loop', 'article', $search_result) ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php if ($search_results['total'] > $per_page): ?>
			<div class="loadmore loadmore_blog">
				<?php echo svg('load', 'loadmore__icon') ?>
				<span><?php _e('Загрузить еще', TEXT_DOMAIN) ?></span>
			</div>
		<?php endif; ?>
	</section>
</main>

<?php get_footer(); ?>
