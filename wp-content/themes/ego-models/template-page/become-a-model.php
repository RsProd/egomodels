<?php
/* Template Name: Become a Model */
get_header() ?>
	<main class="main" role="main">
		<section class="section">
			<div class="container relative">
				<div class="stroke-placeholder stroke-placeholder_large">Models</div>
				<div class="page-head">
					<?php breadcrumbs() ?>
				</div>
				<?php get_template_part('templates/become-model/block', 'form'); ?>
			</div>
		</section>
	</main>
<?php get_footer() ?>