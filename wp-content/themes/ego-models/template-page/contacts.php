<?php
/* Template Name: Contacts */
get_header();

$content = get_field('content');
$contacts = get_field('contacts');

$str_translate = [
	"email" => "Email",
	"phone" => __('Телефон', TEXT_DOMAIN),
	"address" => __('Адрес', TEXT_DOMAIN)
];

?>
<main class="main" role="main">
	<section class="section section_contacts">
		<div class="container relative">
			<span class="stroke-placeholder stroke-placeholder_contacts">Contacts</span>
			<div class="page-head">
				<?php breadcrumbs() ?>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<?php get_template_part('templates/common/block', 'social', [
						"container" => "social_static social_start",
						"item" => "social__item_small"
					]) ?>
					<div class="subtitle">
						<?php _e('Возникли вопросы? Пишите нам и мы ответим в кратчайшие сроки.', TEXT_DOMAIN) ?>
					</div>
					<div class="contact-form">
						<?php get_template_part('templates/forms/form', 'contacts') ?>
						<div class="form-decor"></div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<?php echo do_shortcode('[g-maps]') ?>
				</div>
			</div>
			<?php if (!empty($contacts)): ?>
				<div class="row">
					<div class="info-panel">
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
							<div class="addresses">
								<?php foreach ($contacts as $key => $item): ?>
									<div class="addresses__item">
										<div class="addresses__title"><?php echo $str_translate[$key] ?>:</div>
										<div class="addresses__text"><?php echo $item ?></div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			<?php
			endif;
			if (!empty($content['text']) && !empty($content['video']) && !empty($content['image'])): ?>
				<div class="row about-container">
					<div class="about">
						<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 first-xs">
							<div class="about__content">
								<?php if (!empty($content['text'])): ?>
									<div class="about__text-info">
										<?php echo $content['text'] ?>
									</div>
								<?php endif;
								if (!empty($content['video']['file'])): ?>
									<div class="about__videowrap">
										<video src="<?php echo $content['video']['file'] ?>"
										       poster="<?php echo $content['video']['poster'] ?>" controls></video>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<?php if (!empty($content['image'])): ?>
							<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
								<div class="about__content about__content_rubber">
									<img class="about__image-info"
									     src="<?php echo $content['image'] ?>"
									     alt="<?php bloginfo('name'); ?>" />
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
	<?php get_template_part('templates/our-team/section', 'our-team') ?>
</main>
<?php get_footer() ?>
