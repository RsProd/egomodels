<?php
/* Template Name: Studio */
get_header();
$studio = get_field('studio');
?>
<main class="main" role="main">
	<section class="section section_pd">
		<div class="container relative nearby-mobile">
			<div class="row nearby-mobile">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="page-head">
						<span class="stroke-placeholder stroke-placeholder_studio">Studio</span>
						<?php breadcrumbs() ?>
					</div>
					<?php if (!empty($studio['description'])): ?>
						<div class="studio-info">
							<?php echo $studio['description'] ?>
						</div>
					<?php endif;
					if (!empty($studio['video'])): ?>
						<video class="video-frame hidden-sm hidden-xs"
						       poster="<?php echo $studio['video']['poster'] ?>"
						       controls>
							<source src="<?php echo $studio['video']['file']['url'] ?>"
							        type="<?php echo $studio['video']['file']['mime_type'] ?>">
						</video>
					<?php endif; ?>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 nearby-mobile">
					<?php if (!empty($studio['slider'])): ?>
						<div class="slider">
							<div class="slider__inner swiper-container">
								<div class="swiper-wrapper">
									<?php foreach ($studio['slider'] as $slide): ?>
										<div class="slider__item swiper-slide">
											<img class="slider__image"
											     src="<?php echo $slide['sizes']['large'] ?>"
											     alt="<?php $slide['alt'] ?>" />
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="slider__navigation">
								<div class="slider__btn-next"></div>
								<div class="slider__btn-prev"></div>
							</div>
						</div>
					<?php endif;
					if (!empty($studio['video'])): ?>
						<video class="video-frame hidden-md hidden-lg"
						       poster="<?php echo $studio['video']['poster'] ?>"
						       controls>
							<source src="<?php echo $studio['video']['file']['url'] ?>"
							        type="<?php echo $studio['video']['file']['mime_type'] ?>">
						</video>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<span class="black-line"></span>
	</section>
	<?php if (!empty($studio['gallery'])): ?>
		<section class="section section_photo-gallery">
			<div class="container">
				<h2 class="section__title section__title_inversion section__title_center"><?php _e('Фото студии', TEXT_DOMAIN) ?></h2>
				<div class="gallery">
					<?php foreach ($studio['gallery'] as $image): ?>
						<a class="gallery__item"
						   href="<?php echo $image['url'] ?>"
						   data-lightbox="data-lightbox">
							<img class="gallery__image"
							     src="<?php echo $image['sizes']['large'] ?>"
							     alt="<?php echo $image['alt'] ?>" />
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		</section>
	<?php
	endif;
	get_template_part('templates/studio/section', 'calendar');
	?>
</main>

<?php get_footer() ?>
