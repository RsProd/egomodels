<?php
/* Template Name: Compcard */

get_header('empty');
$model = getModelsForID($_GET['model'])[0];
$photos = get_field('compcard-gallery', $model['id']);
$contacts = get_field('compcard_contacts', 'options');

?>
	<main class="main">
		<section class="section-center">
			<div id="compcard">
				<div class="card">
					<div class="card__download" data-html2canvas-ignore>
						<?php echo svg('download', 'card__icon') ?>
					</div>
					<div class="card__col">
						<div class="card__row">
							<div class="card__photo">
									<span class="card__photo-image"
									      style="background: url('<?php echo $model['photo'] ?>') center / cover"></span>
								<div class="card__logo">
									<img class="logo__icon" src="<?php echo assets('icons/source/logo.svg') ?>"
									     alt="<?php echo get_bloginfo('name') ?>">
								</div>
							</div>
							<?php if (!empty($model['parameters'])): ?>
								<div class="card__params">
									<?php foreach ($model['parameters'] as $key => $params): ?>
										<div class="card__params-item">
											<span><?php echo $key ?>:</span>
											<span><?php echo $params ?></span>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>

						<div class="card__row">
							<div class="compcard-contacts">
								<div class="compcard-contacts__row">
									<div class="compcard-contacts__col">
										<a href="<?php echo get_site_url() ?>" class="compcard-contacts__link"
										   target="_blank">
											<?php echo get_site_url() ?>
										</a>
										<?php if (!empty($contacts['phone'])): ?>
											<a href="<?php echo linkPhone($contacts['phone']) ?>"
											   class="compcard-contacts__link">
												<?php echo $contacts['phone'] ?>
											</a>
										<?php endif; ?>
									</div>
									<div class="compcard-contacts__col">
										<?php if (!empty($contacts['e-mail'])): ?>
											<a href="mailto:<?php echo $contacts['e-mail'] ?>"
											   class="compcard-contacts__link"><?php echo $contacts['e-mail'] ?></a>
										<?php endif;
										if (!empty($contacts['instagram'])): ?>
											<a href="https://instagram.com/<?php echo $contacts['instagram'] ?>"
											   class="compcard-contacts__link" target="_blank">
												Instagram @<?php echo $contacts['instagram'] ?>
											</a>
										<?php endif; ?>
									</div>
								</div>
								<div class="compcard-contacts__item">
									<?php if (!empty($contacts['address'])): ?>
										<p><?php echo $contacts['address'] ?></p>
									<?php endif;
									if (!empty($contacts['link'])): ?>
										<a href="<?php echo $contacts['link'] ?>"><?php echo $contacts['link'] ?></a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

					<div class="card__inner">
						<?php if (!empty($model['name'])): ?>
							<div class="card__name"><?php echo $model['name'] ?></div>
						<?php endif;
						if (!empty($compcard)): ?>
							<div class="card__text">
								<?php echo $compcard ?>
							</div>
						<?php endif;
						if (!empty($photos)): ?>
							<div class="card__gallery">
								<?php foreach ($photos as $key => $photo): if ($key < 4): ?>
									<span class="card__small-photo"
									      style="background: url('<?php echo $photo['sizes']['large'] ?>') top center / cover"></span>
								<?php endif; endforeach; ?>
							</div>
						<?php endif; ?>

					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer('empty') ?>