<?php
/* Template Name: Favorites */
get_header();

$favorites = getFavorites();

$models = getModels([
	"post__in" => $favorites,
	"posts_per_page" => -1
]);

?>
<main class="main" role="main">
	<section class="section section_main">
		<div class="container container-favorites relative">
			<span class="stroke-placeholder stroke-placeholder_favorites">Favorites</span>
			<div class="page-head">
				<?php breadcrumbs();
				if (!empty($favorites && !empty($models['posts']))): ?>
					<div class="clear-button"><?php _e('Очистить список', TEXT_DOMAIN) ?></div>
				<?php endif; ?>
			</div>
			<?php if (!empty($favorites && !empty($models['posts']))): ?>
				<div class="row grid-row ajax-container">
					<?php foreach ($models['posts'] as $model): ?>
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 grid-item load">
							<?php get_template_part('templates/loops/loop', 'model', $model) ?>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="button button_favorites" data-toggle=".popup_favorites" data-toggle-body="popup-open">
					<?php echo svg('plus', 'button__icon') ?>
					<div class="button__name"><?php _e('Отправить список', TEXT_DOMAIN) ?></div>
				</div>
			<?php else:
				get_template_part('templates/favorites/block', 'not-models');
			endif; ?>

		</div>
		<template id="model-not-found">
			<?php get_template_part('templates/favorites/block', 'not-models') ?>
		</template>
	</section>
</main>
<?php get_footer() ?>
