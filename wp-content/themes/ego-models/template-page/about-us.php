<?php
/* Template Name: About US */
get_header();

$text_areas = get_field('text_areas');
$content = get_field('content');
?>
<main class="main" role="main">
	<section class="section section_pd">
		<div class="container relative">
			<span class="stroke-placeholder stroke-placeholder_blog">About us</span>
			<div class="page-head">
				<?php breadcrumbs() ?>
			</div>
			<div class="row">
				<?php if (!empty($text_areas['image'])): ?>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="content content_rubber">
							<img class="content__image-info"
							     src="<?php echo $text_areas['image'] ?>" alt="" />
							<div class="decor-first">
								<span></span>
							</div>
						</div>
					</div>
				<?php endif;
				if (!empty($text_areas['test_area'])): ?>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 first-xs">
						<div class="content">
							<div class="content__text-info content__text-info_prop">
								<?php echo $text_areas['test_area'] ?>
							</div>
						</div>
					</div>
				<?php endif;
				if (!empty($text_areas['test_area_second'])): ?>
					<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
						<div class="content content_black">
							<div class="content__text-info content__text-info_black">
								<?php echo $text_areas['test_area_second'] ?>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="decor-second"></div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<?php if (!empty($content['image']) && !empty($content['content'])): ?>
		<section class="section">
			<div class="container full-xs">
				<div class="row full-xs">
					<?php if (!empty($content['image'])): ?>
						<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 full-xs">
							<div class="image-about">
								<img src="<?php echo $content['image'] ?>">
							</div>
						</div>
					<?php endif; ?>
					<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
						<div class="info">
							<?php if (!empty($content['title'])): ?>
								<div class="info__title"><?php echo $content['title'] ?></div>
							<?php endif;
							if (!empty($content['content'])): ?>
								<div class="info__content">
									<?php echo $content['content'] ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif;
	get_template_part('templates/front-page/section', 'video');
	get_template_part('templates/our-team/section', 'our-team');
	?>
</main>
<?php get_footer() ?>
