<?php
/* Template Name: Reviews */
get_header();
$postID = get_the_ID();
$comments = getComments($postID);
?>

<main class="main" role="main">
	<section class="section">
		<div class="container relative">
			<span class="stroke-placeholder stroke-placeholder_reviews">Reviews</span>
			<div class="page-head">
				<?php breadcrumbs(); ?>
			</div>
			<div class="reviews">
				<div class="reviews__inner">
					<div class="reviews__list comments-list">
						<?php foreach ($comments['comments'] as $comment): ?>
							<div class="reviews-block__item">
								<?php get_template_part('templates/loops/loop', 'review', $comment) ?>
							</div>
						<?php endforeach; ?>
					</div>
					<?php if ($comments['count'] > 3): ?>
						<div class="loadmore loadmore_comments">
							<?php echo svg('load', 'loadmore__icon') ?>
							<span><?php _e('Загрузить еще', TEXT_DOMAIN) ?></span>
						</div>
					<?php endif; ?>
				</div>

				<?php get_template_part('templates/reviews/block', 'form') ?>
			</div>
		</div>
	</section>
	<?php get_template_part('templates/loops/ajax/loop', 'review') ?>
</main>
<?php get_footer() ?>
