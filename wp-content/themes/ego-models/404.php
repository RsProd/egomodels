<?php get_header(); ?>
    <main class="main" role="main">
      <div class="container">
        <div class="page-404">
          <div class="row">
            <div class="page-404__image"></div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="page-404__info">
                <div class="page-404__title">
	                <?php echo sprintf(__('Извините... %s страница не найдена', TEXT_DOMAIN), '<br>') ?>
                </div>
                <div class="page-404__desc">
	                <?php echo sprintf(__('Но ничего страшного, вы можете перейти на %s Главную страницу %s', TEXT_DOMAIN), '<a href="/">', '</a>') ?>
	              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>

<?php get_footer();
