<div class="search-container">
	<form class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
		<input
			type="text"
			class="search-form__input"
			name="s"
			placeholder="<?php _e('Поиск по сайту...', TEXT_DOMAIN); ?>"
			value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>"
		/>
		<div class="search-form__result-wrapper">
			<div class="search-form__result"></div>
		</div>
	</form>
</div>