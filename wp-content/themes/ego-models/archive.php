<?php get_header();

echo 'This is ARCHIVE.PHP';

single_cat_title();

if (have_posts()) :
	while (have_posts()) : the_post();

		echo get_the_title();
		echo get_permalink();

		if (has_post_thumbnail()):
			echo get_the_post_thumbnail_url();
		else:
			echo 'image not found';
		endif;

	endwhile;
endif;

wp_reset_query();

get_footer();
