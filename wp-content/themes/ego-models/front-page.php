<?php get_header();
if (have_rows('sections')): ?>
	<main class="main">
		<?php while (have_rows('sections')) : the_row();
			get_template_part('templates/front-page/section', get_row_layout());
		endwhile; ?>
	</main>
<?php endif;
get_footer(); ?>