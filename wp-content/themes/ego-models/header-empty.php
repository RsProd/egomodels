<!doctype html>
<html lang="<?php echo LOCALE ?>" class="<?php echo colorScheme() ?>">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name='robots' content='noindex,nofollow' />
	<?php wp_head(); ?>
	<?php echo customScript('before_head') ?>
</head>

<body <?php body_class('preload'); ?>>
<div class="preloader preloader_load">
	<div class="preloader__part"><span></span></div>
	<div class="preloader__part"><span></span></div>
</div>