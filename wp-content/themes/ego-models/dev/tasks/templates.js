'use strict';

import { paths } from '../gulpfile.babel';
import pugData from '../src/data/data.json';

import gulp from 'gulp';
import pug from 'gulp-pug';
import plumber from 'gulp-plumber';
import browsersync from 'browser-sync';
import yargs from 'yargs';
import data from 'gulp-data';

const argv = yargs.argv,
	production = !!argv.production;

gulp.task('templates', () => {
	return gulp.src(paths.templates.src)
		.pipe(plumber())
		.pipe(data(function(file) {
			return pugData;
		}))
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest(paths.templates.dist))
		.pipe(browsersync.stream());
});
