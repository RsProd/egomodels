'use strict';

import { paths } from '../gulpfile.babel';
import gulp from 'gulp';
import browsersync from 'browser-sync';

gulp.task('serve', () => {
  
  let settings;
  
  paths.serve.proxy_support ? settings = { ...paths.serve.proxy } : settings = { ...paths.serve.default };
  
  browsersync.init({
    ...settings,
    port: paths.serve.port,
    open: false,
    notify: false
  });
  
  gulp.watch(paths.templates.watch, gulp.parallel('templates'));
  gulp.watch(paths.data.watch, gulp.parallel('templates'));
  gulp.watch(paths.styles.watch, gulp.parallel('styles'));
  gulp.watch(paths.scripts.watch, gulp.parallel('scripts'));
  gulp.watch(paths.sprites.watch, gulp.parallel('sprites'));
  gulp.watch(paths.images.watch, gulp.parallel('images'));
  gulp.watch(paths.webp.watch, gulp.parallel('webp'));
  gulp.watch(paths.fonts.watch, gulp.parallel('fonts'));
});
