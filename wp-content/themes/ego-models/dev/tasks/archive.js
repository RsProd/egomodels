'use strict';

import gulp from 'gulp';
import zip from 'gulp-zip';

gulp.task('archive', () => {
	const zipName = 'build.zip';
	
	return gulp.src('dist/**/*')
		.pipe(zip(zipName))
		.pipe(gulp.dest('.'));
});
