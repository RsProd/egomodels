'use strict';

import { paths } from '../gulpfile.babel';
import gulp from 'gulp';
import favicons from 'gulp-favicons';
import debug from 'gulp-debug';

gulp.task('favicons', () => {
	return gulp.src(paths.favicons.src)
		.pipe(favicons({
			appName: 'App',
			appShortName: 'App short name',
			appDescription: 'App description',
			path: '/assets/images/favicons/',
			icons: {
				appleIcon: true,
				favicons: true,
				online: false,
				appleStartup: false,
				android: true,
				firefox: true,
				yandex: true,
				windows: true,
				coast: true
			}
		}))
		.pipe(gulp.dest(paths.favicons.dist))
		.pipe(debug({
			'title': 'Favicons'
		}));
});
