'use strict';

import { paths } from '../gulpfile.babel';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import gulp from 'gulp';
import gulpif from 'gulp-if';
import rename from 'gulp-rename';
import browsersync from 'browser-sync';
import debug from 'gulp-debug';
import yargs from 'yargs';

const webpackConfig = require('../webpack.config.js'),
  argv = yargs.argv,
  production = !!argv.production,
  cms = !!argv.cms;

webpackConfig.mode = production ? 'production' : 'development';
webpackConfig.devtool = production ? false : 'source-map';

gulp.task('scripts', () => {
  return gulp.src(paths.scripts.src)
    .pipe(webpackStream(webpackConfig), webpack)
    .on('error', function handleError() {
      this.emit('end');
    })
    .pipe(gulpif(production, rename({
      suffix: '.min'
    })))
    .pipe(gulp.dest(gulpif(cms, paths.scripts.cms, paths.scripts.dist)))
    .pipe(debug({
      'title': 'JS files'
    }))
    .on('end', browsersync.reload);
});
