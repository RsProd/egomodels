import * as Facebook from 'fb-sdk-wrapper';

const config = {
  appId: '1374665999549175'
};

const facebookOAuth = () => {
  const button = document.querySelector('.facebook-login');
  
  const name = document.querySelector('input[name="abb-name"]');
  const email = document.querySelector('input[name="email"]');
  const photo = document.querySelector('input[name="photo"]');
  
  const auth = (callback) => {
    Facebook.load().then(() => {
      Facebook.init({
        appId: config.appId,
        cookie: true,
        xfbml: false,
        version: 'v9.0'
      });
      
      Facebook.login({
        scope: 'email',
        return_scopes: false
      }).then((response) => {
        if (response.status === 'connected') {
          Facebook.api('/me', 'get', {
            fields: 'id, email, first_name, last_name, picture.type(large)'
          }).then((response) => callback(response));
        }
      });
    });
  };
  
  if (button) {
    const foto = document.createElement('img');
    foto.className = 'facebook-login__photo';
    
    button.addEventListener('click', () => {
      auth((response) => {
        name.value = `${response.first_name} ${response.last_name}`;
        email.value = response.email;
        photo.value = response['picture']['data']['url'];
        foto.src = response['picture']['data']['url'];
        button.children[0].remove();
        button.append(foto);
      });
    });
  }
  
};

export default facebookOAuth;