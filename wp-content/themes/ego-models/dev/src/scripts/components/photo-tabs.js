import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';

const tabs = () => {
  const tabs = document.querySelector('.tabs');
  
  if (tabs) {
    const nav = tabs.querySelectorAll('.tabs__nav li');
    const content = tabs.querySelectorAll('.tabs__item');
    
    let masonry = [];
    let activeTab = 0;
    
    content.forEach((tab, i) => {
      const el = tab.querySelector('.js-masonry');
      
      imagesLoaded('.tabs', () => {
        masonry[i] = new Masonry(el, {
          columnWidth: '.model-gallery__item',
          gutter: 0,
          itemSelector: '.js-masonry-item',
          transitionDuration: 0,
          horizontalOrder: true
        });
        
      });
      
    });
    
    nav[activeTab].classList.add('active');
    content[activeTab].classList.add('active');
    
    nav.forEach((item, number) => {
      
      item.addEventListener('click', () => {
        if (number !== activeTab && number >= 0) {
          nav[activeTab].classList.remove('active');
          nav[number].classList.add('active');
          
          content[activeTab].classList.remove('active');
          content[number].classList.add('active');
          
          masonry[number].layout();
          activeTab = number;
        }
      });
      
    });
  }
};

export default tabs;