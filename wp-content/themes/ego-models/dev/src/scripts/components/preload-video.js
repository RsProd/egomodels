const preloadVideo = () => {
  window.addEventListener('load', function() {
    
    setTimeout(() => {
      const video = document.querySelectorAll('.video__poster, .model-video__player');
      video.forEach((item) => {
        const source = item.querySelector('source');
        source.src = source.getAttribute('data-src');
        item.load();
        item.play();
      });
      
    });
  });
};

export default preloadVideo;