const sniffer = () => {
  const ua = navigator.userAgent.toLowerCase();
  
  const isWindowsPhone = /windows phone|iemobile|wpdesktop/.test(ua);
  
  const isDroidPhone = !isWindowsPhone && /android.*mobile/.test(ua);
  const isDroidTablet = !isWindowsPhone && !isDroidPhone && (/android/i).test(ua);
  
  const isIos = !isWindowsPhone && (/ip(hone|od|ad)/i).test(ua) && !window.MSStream;
  const isIpad = !isWindowsPhone && (/ipad/i).test(ua) && isIos;
  
  const isTablet = isDroidTablet || isIpad;
  const isPhone = isDroidPhone || (isIos && !isIpad) || isWindowsPhone;
  const isDevice = isPhone || isTablet;
  
  let infos = {
    isDevice: isDevice,
    isPhone: isPhone,
    isTablet: isTablet,
    isDesktop: !isPhone && !isTablet
  };
  let output = {};
  Object.keys(infos).forEach(info =>
    Object.defineProperty(output, info, { get: () => infos[info] }));
  
  return Object.freeze(output);
};

const debounce = (func, wait, immediate) => {
  let timeout;
  return function executedFunction() {
    const context = this;
    const args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    
    const callNow = immediate && !timeout;
    
    clearTimeout(timeout);
    
    timeout = setTimeout(later, wait);
    
    if (callNow) func.apply(context, args);
  };
};

const createEl = (arr, appendTo) => {
  const el = '';
  
  for (const key in arr) {
    const el = document.createElement(arr[key].tag);
    
    arr[key].class ? el.className = arr[key].class : '';
    arr[key].text ? el.innerHTML = arr[key].text : '';
    
    if (arr[key].attr !== undefined) {
      for (const attr in arr[key].attr) {
        el.setAttribute(attr, arr[key].attr[attr]);
      }
    }
    
    if (arr[key].style !== undefined) {
      Object.assign(el.style, arr[key].style);
    }
    
    if (arr[key].children !== undefined) {
      createEl(arr[key].children, el);
    }
    
    appendTo.append(el);
  }
};

const declOfNum = (n, text_forms) => {
  n = Math.abs(n) % 100;
  const n1 = n % 10;
  if (n > 10 && n < 20) { return text_forms[2]; }
  if (n1 > 1 && n1 < 5) { return text_forms[1]; }
  if (n1 === 1) { return text_forms[0]; }
  return n + ' ' + text_forms[2];
};

export { debounce, sniffer, createEl, declOfNum };
