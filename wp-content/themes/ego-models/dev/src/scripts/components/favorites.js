import Cookies from 'js-cookie';
import imagesLoaded from 'imagesloaded';
import { debounce } from './helpers';

export default class FavoritesList {
  constructor() {
    this.body = document.querySelector('body');
    this.notifyText = notifyPopup;
    this.data = Cookies.get('favorites') !== undefined ? JSON.parse(Cookies.get('favorites')) : [];
    this.container = document.querySelector('.container-favorites');
    this.containerProfiles = document.querySelector('.ajax-container');
    this.buttonPopup = document.querySelector('.button_favorites');
    this.formFavorites = document.querySelector('.form_favorites');
    this.clearButton = document.querySelector('.clear-button');
    
    this.formData = [];
    
    if (this.container) {
      this.placeholder = document.querySelector('#model-not-found');
      this.placeholderContent = this.placeholder.content.cloneNode(true);
    }
  }
  
  setCookies = () => {
    Cookies.set('favorites', this.data, {
      expires: 7
    });
  };
  
  notify = (name) => {
    const titleText = name + ' ' + this.notifyText['message'];
    const container = document.createElement('div');
    const title = document.createElement('div');
    const button = document.createElement('a');
    
    container.className = 'notify';
    title.className = 'notify__title';
    button.className = 'notify__button notify__button_ok';
    
    title.innerHTML = titleText;
    button.href = this.notifyText.link.href;
    button.innerText = this.notifyText.link.title;
    
    container.append(title, button);
    
    const notify = document.querySelector('.notify');
    if (notify) {
      const title = notify.querySelector('.notify__title');
      title.innerHTML = titleText;
    } else {
      this.body.append(container);
      setTimeout(() => container.classList.add('active'), 100);
    }
  };
  
  setData = () => {
    if (this.buttonPopup) {
      this.buttonPopup.addEventListener('click', () => {
        const selectCards = this.containerProfiles.querySelectorAll('.card-profile');
        
        selectCards.forEach((item) => {
          const name = item.getAttribute('data-id');
          if (!this.formData.includes(name)) {
            this.formData.push(name);
          }
        });
        this.formFavorites.querySelector('.select-models').value = this.formData.join(', ');
      });
    }
  };
  
  add = () => {
    
    const removeNotify = debounce(() => {
      const notify = document.querySelector('.notify');
      
      notify.classList.remove('active');
      setTimeout(() => notify.remove(), 300);
    }, 4000);
    
    document.addEventListener('click', (e) => {
      const target = e.target;
      const element = target.closest('.button-card_add');
      
      if (element) {
        const modelName = element.getAttribute('data-model-name');
        const id = +element.getAttribute('data-model-id');
        console.log(modelName, id);
        this.data.includes(id) ?
          this.data = this.data.filter(item => item !== id) :
          this.data.push(id);
        
        element.classList.toggle('active');
        
        this.setCookies();
        
        if (element.classList.contains('active')) {
          this.notify(modelName);
          removeNotify();
        }
      }
    });
  };
  
  remove = () => {
    if (this.container) this.placeholderContent.querySelector('.favorites').classList.remove('favorites_show');
    
    document.addEventListener('click', (e) => {
      const target = e.target;
      const element = target.closest('.button-card_remove');
      
      if (element) {
        const elCard = element.closest('.card-profile');
        const id = +elCard.getAttribute('data-id');
        const name = elCard.querySelector('.card-profile__name').innerHTML;
        const gridItem = element.closest('.grid-item');
        
        if (this.formData.includes(name)) {
          this.formData = this.formData.filter(el => el !== name);
        }
        
        this.data = this.data.filter(item => item !== id);
        this.setCookies();
        
        gridItem.classList.remove('load');
        setTimeout(() => {
          gridItem.remove();
          const elements = document.querySelectorAll('.card-profile');
          if (elements.length === 0) {
            this.container.appendChild(this.placeholderContent);
            this.buttonPopup.remove();
            this.clearButton.remove();
            this.containerProfiles.remove();
            imagesLoaded('.favorites', e => e.elements[0].classList.add('favorites_show'));
          }
        }, 500);
      }
    });
  };
  
  clear = () => {
    if (this.clearButton) {
      this.clearButton.addEventListener('click', () => {
        this.containerProfiles.remove();
        this.clearButton.remove();
        this.buttonPopup.remove();
        this.container.appendChild(this.placeholderContent);
        
        imagesLoaded('.favorites', e => e.elements[0].classList.add('favorites_show'));
        Cookies.remove('favorites');
      });
    }
  };
  
  init() {
    this.add();
    this.remove();
    this.clear();
    this.setData();
  }
}