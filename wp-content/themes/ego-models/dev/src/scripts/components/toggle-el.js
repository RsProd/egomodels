const toggleEl = () => {
  document.addEventListener('click', (e) => {
    const element = e.target.closest('[data-toggle]');
    
    if (element) {
      const attr = {
        toggle: element.getAttribute('data-toggle'),
        body: element.getAttribute('data-toggle-body'),
        context: element.getAttribute('data-toggle-context')
      };
      
      const el = {
        toggle: document.querySelectorAll(attr.toggle),
        body: document.querySelector('body'),
        context: attr.context && attr.context.length ? element.closest(attr.context) : false
      };
      
      element.classList.toggle('active');
      
      attr.context && el.context ?
        el.context.querySelectorAll(attr.toggle).forEach(el => el.classList.toggle('active')) :
        el.toggle.forEach(el => el.classList.toggle('active'));
      
      if (attr.body !== null && attr.body.length) {
        el.body.classList.toggle(attr.body);
      }
    }
  });
};

export default toggleEl;