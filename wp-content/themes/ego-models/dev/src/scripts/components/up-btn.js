import scrollPolyfill from 'scroll-behavior-polyfill';

const buttonUP = () => {
  const btn = document.querySelector('.up-btn');
  if (btn) {
    btn.addEventListener('click', () => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    });
    
    window.addEventListener('scroll', () => {
      (window.scrollY >= 100) ?
        btn.classList.add('active') :
        btn.classList.remove('active');
    });
  }
};

export default buttonUP;