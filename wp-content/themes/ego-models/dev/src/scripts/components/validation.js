import validate from 'validate.js';

export default class Validation {
  constructor(form, data) {
    this.form = form;
    this.data = validate.collectFormValues(form);
    this.translation = modelForm.formErrors;
    
    this.errors = {};
    
    this.init();
  }
  
  valid = () => {
    const fields = this.form.querySelectorAll('input, textarea');
    const thisRules = {};
    
    validate.validators.presence.message = '^' + this.translation.fieldRequired;
    
    const rules = {
      'abb-name': {
        presence: true,
        length: {
          minimum: 3,
          message: '^' + this.translation.minLength
        }
      },
      
      'number': {
        presence: true,
        format: {
          pattern: '[0-9 -]+',
          flags: 'i',
          message: '^' + this.translation.invalidNumber
        }
      },
      
      'email': {
        presence: true,
        email: {
          message: '^' + this.translation.wrongEmail
        }
      },
      
      'message': {
        presence: true
      },
      
      'personal': {
        presence: true
      },
      
      'comment': {
        presence: true
      }
      
    };
    
    fields.forEach((e) => {
      if (rules[e.name] !== undefined) {
        thisRules[e.name] = rules[e.name];
      }
    });
    
    this.form.querySelectorAll('.form__error').forEach(el => el.remove());
    this.form.querySelectorAll('input[type="checkbox"]').forEach(el => el.parentElement.classList.remove('is-error'));
    this.errors = validate(this.data, thisRules);
    this.printErrors();
  };
  
  printErrors = () => {
    const errorEl = document.createElement('span');
    errorEl.className = 'form__error';
    
    for (const key in this.errors) {
      const field = this.form.querySelector('[name=\'' + key + '\']');
      const el = errorEl.cloneNode();
      const type = field.getAttribute('type');
      
      if (type !== 'checkbox') {
        el.innerHTML = this.errors[key];
        field.closest('.form__field').append(el);
      } else {
        field.parentElement.classList.add('is-error');
      }
    }
  };
  
  complete = () => {
    return this.errors === undefined;
  };
  
  init() {
    this.valid();
  }
  
}