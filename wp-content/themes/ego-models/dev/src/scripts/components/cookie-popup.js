import Cookies from 'js-cookie';

export default function(days) {
  
  let popupContainer = document.querySelector('.cookies-popup');
  let closeBtn = document.querySelector('.cookies-popup__close');
  let acceptBtn = document.querySelector('.cookies-popup__button');
  
  if (popupContainer) {
    if (!Cookies.get('accept-policy')) {
      setTimeout(() => {
        popupContainer.classList.add('active');
      }, 1000);
    }
    
    closeBtn.addEventListener('click', function() {
      popupContainer.classList.remove('active');
    });
    
    acceptBtn.addEventListener('click', function() {
      popupContainer.classList.remove('active');
      Cookies.set('accept-policy', true, {
        expires: days
      });
    });
  }
}
