const videoModels = () => {
  const video = document.querySelector('.model-video');
  
  if (video !== null) {
    const player = video.querySelector('.model-video__player');
    const mute = document.querySelector('.model-video__mute');
    const play = document.querySelector('.model-video__control');
    
    mute.addEventListener('click', () => {
      player.muted = mute.classList.contains('active');
      mute.classList.toggle('active');
    });
    
    setTimeout(() => {
      if (!player.paused) {
        play.classList.add('active');
      }
    });
    
    play.addEventListener('click', () => {
      play.classList.toggle('active');
      player.paused ?
        player.play() :
        player.pause();
    });
    
    player.addEventListener('loadeddata', (event) => {
      video.classList.add('loaded');
    });
    
  }
};

export default videoModels;