import Swiper, { Navigation, Thumbs } from 'swiper';
import imagesLoaded from 'imagesloaded';

Swiper.use([Navigation, Thumbs]);

export default class LightBox {
  constructor() {
    this.body = document.querySelector('body');
    this.links = document.querySelectorAll('[data-lightbox]');
  }
  
  create = (arr) => {
    const container = document.createElement('div');
    container.className = 'lightbox loading';
    
    const close = document.createElement('div');
    close.className = 'lightbox__close';
    
    const inner = document.createElement('div');
    inner.className = 'lightbox__inner container';
    
    const imageBig = document.createElement('div');
    imageBig.className = 'lightbox__gallery swiper-container';
    
    const thumbs = document.createElement('div');
    thumbs.className = 'lightbox__thumbs swiper-container';
    
    const wrap = document.createElement('div');
    wrap.className = 'swiper-wrapper';
    
    const slide = document.createElement('div');
    slide.className = 'lightbox__slide swiper-slide';
    
    const image = document.createElement('img');
    image.className = 'lightbox__image';
    
    const prevArrow = document.createElement('div');
    prevArrow.className = 'swiper-button-prev';
    
    const nextArrow = document.createElement('div');
    nextArrow.className = 'swiper-button-next';
    
    for (let i = 0; arr.length > i; i++) {
      let src = arr[i].getAttribute('href');
      let slideEl = slide.cloneNode();
      let imageEl = image.cloneNode();
      
      imageEl.setAttribute('src', src);
      slideEl.append(imageEl);
      wrap.append(slideEl);
    }
    
    const wrapThumb = wrap.cloneNode(true);
    wrapThumb.classList.add('lightbox__thumb-wrap');
    
    imageBig.append(wrap);
    thumbs.append(wrapThumb);
    inner.append(imageBig, thumbs, prevArrow, nextArrow);
    container.append(close, inner);
    
    this.body.append(container);
  };
  
  open = () => {
    this.links.forEach((item, index, arr) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        this.create(arr);
        this.slider(index);
        
        item.parentElement.classList.add('disabled');
        
        const lightBox = document.querySelector('.lightbox');
        lightBox.classList.add('active');
        this.body.classList.add('menu-open');
        item.parentElement.classList.remove('disabled');
        
        imagesLoaded('.lightbox', () => lightBox.classList.remove('loading'));
        
      });
    });
  };
  
  close = () => {
    document.addEventListener('click', (e) => {
      let lightbox = document.querySelector('.lightbox');
      if (e.target.classList[0] === 'lightbox__close') {
        lightbox.classList.remove('active');
        this.body.classList.remove('menu-open');
        setTimeout(() => {
          lightbox.remove();
        }, 500);
      }
    });
    
  };
  
  slider = (slideTo) => {
    let gallery = '.lightbox__gallery';
    let thumbs = '.lightbox__thumbs';
    
    const galleryThumbs = new Swiper(thumbs, {
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      touchRatio: 0.2,
      breakpoints: {
        320: {
          slidesPerView: 4,
          spaceBetween: 4
        },
        640: {
          spaceBetween: 4,
          slidesPerView: 8
        }
      }
    });
    
    const galleryTop = new Swiper(gallery, {
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      thumbs: {
        swiper: galleryThumbs
      }
    });
    
    galleryTop.slideTo(slideTo, 0);
  };
  
  init() {
    this.open();
    this.close();
  }
}
