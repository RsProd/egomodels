import axios from 'axios';
import Qs from 'qs';

import Validation from './validation';

export default class Forms {
  constructor() {
    this.forms = document.querySelectorAll('.form_contact, .form_contact-page, .form_calendar, .form_favorites');
    this.data = {};
  };
  
  setData = (form) => {
    const inputs = form.querySelectorAll('input, textarea');
    this.data = {};
    
    inputs.forEach((input) => {
      const ckeckbox = input.type === 'checkbox';
      
      input.value.length && !ckeckbox || ckeckbox && input.checked ?
        this.data[input.name] = input.value :
        delete this.data[input.name];
      
    });
  };
  
  request = (data, callback) => {
    data['action'] = 'send_form';
    
    axios.post(path.ajax, Qs.stringify(data))
      .then(response => callback(response.data))
      .catch(error => console.log(error));
  };
  
  send = () => {
    const message = document.createElement('div');
    message.className = 'form__message';
    
    this.forms.forEach((form) => {
      const messageEl = message.cloneNode();

      form.addEventListener('submit', (e) => {
        e.preventDefault();
        this.setData(e.target);
        
        const valid = new Validation(e.target);
        
        if (valid.complete()) {
          const button = form.querySelector('button');
          
          button.disabled = true;
          this.request(this.data, (response) => {
            form.reset();
            
            button.disabled = false;
            messageEl.innerHTML = response;
            form.append(messageEl);
            
            setTimeout(() => {
              document.querySelectorAll('.form__message').forEach(el => el.remove());
            }, 2000);
          });
        }
      });
    });
  };
  
  init() {
    this.send();
  }
}