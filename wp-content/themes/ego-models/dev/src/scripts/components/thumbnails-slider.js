import gsap from 'gsap';
import { sniffer } from './helpers';

export default class CardSlider {
  constructor(card) {
    this.time = 0.8;
    this.ease = 'none';
    this.current = 0;
    this.card = card;
    this.slides = [...card.querySelectorAll('.card-profile__image')];
    this.immediate = false;
    this.length = this.slides.length - 1;
    this.breakpoint = window.matchMedia('(min-width: 992px)').matches;
    this.timeline = new gsap.timeline({
      paused: true,
      onComplete: () => this.clear()
    });
  }
  
  clear = () => {
    this.timeline.clear();
    this.reset(this.current);
    this.immediate && this.play();
  };
  
  reset = (current) => {
    this.slides
      .filter((slide, i) => i !== current)
      .forEach(slide => gsap.set(slide, { opacity: '0' }));
  };
  
  to = (current, next) => {
    this.timeline.play();
    this.timeline
      .to(current, { opacity: '0' }, 0)
      .fromTo(next, { opacity: '0' }, { opacity: '1' }, 0);
  };
  
  play = () => {
    if (!this.timeline.isActive()) {
      const next = this.current === this.length ? 0 : this.current + 1;
      this.to(this.slides[this.current], this.slides[next]);
      this.current < this.length ? this.current++ : this.current = 0;
    }
  };
  
  addListeners = () => {
    this.card.addEventListener('mouseover', (e) => {
      this.immediate = true;
      this.play();
    });
    this.card.addEventListener('mouseout', (e) => {
      this.immediate = false;
      
      if (this.current !== 0) {
        this.timeline.then(() => {
          this.to(this.slides[this.current], this.slides[0]);
          this.current = 0;
        });
      }
      
    });
  };
  
  init() {
    gsap.defaults({
      ease: this.ease,
      duration: this.time,
      immediateRender: true,
      repeatRefresh: true,
      delay: 1
    });
    
    if (!sniffer().isDevice && this.breakpoint) {
      if (this.length > 1) {
        this.addListeners();
      }
    }
  }
}
