const select = () => {
  const el = document.querySelectorAll('.select');
  
  el.forEach((select) => {
    const title = select.querySelector('.select__title');
    const input = select.querySelector('.select__input');
    const inner = select.querySelector('.select__content');
    const items = inner.querySelectorAll('.select__item');
    
    title.addEventListener('click', () => {
      select.classList.toggle('active');
    });
    
    items.forEach((item) => {
      const value = item.getAttribute('data-value');
      const label = item.innerHTML;
      
      item.addEventListener('click', () => {
        select.classList.toggle('active');
        title.innerHTML = label;
        input.value = value;
      });
      
    });
  });
  
};

export default select;
