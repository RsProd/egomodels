import axios from 'axios';
import Qs from 'qs';

export default function() {
  // Modal
  let searchBtn = document.querySelector('.buttons__item_search');
  let searchContainer = document.querySelector('.search-container');
  
  if (searchContainer) {
    searchContainer.addEventListener('click', function(e) {
      if (e.target.closest('.search-form')) return;
      searchContainer.classList.remove('active');
      searchBtn.classList.remove('active');
      document.querySelector('body').classList.remove('popup-open');
    });
  }
  
  // LiveSearch
  let body = document.querySelector('body');
  let formSearch = document.querySelector('.search-form');
  if (formSearch) {
    let inputSearch = formSearch.querySelector('.search-form__input');
    let resultContainer = formSearch.querySelector('.search-form__result');
    
    inputSearch.addEventListener('input', startSearch);
    
    function startSearch() {
      const data = {
        s: inputSearch.value,
        post_type: ['post', 'models', 'page']
      };
      
      inputSearch.value.length > 2
        ? request(data, (response) => {
          response.total > 0
            ? visibleResult(response.posts)
            : hiddenResult('По вашему запросу ничего не найдено');
        }) :
        hiddenResult('Запрос должен быть длиной минимум 3 символа');
    }
    
    function visibleResult(data) {
      resultContainer.classList.add('is-active');
      
      while (resultContainer.firstChild) {
        resultContainer.removeChild(resultContainer.firstChild);
      }
      
      for (const item in data) {
        let article = data[item];
        let itemEl = document.createElement('div');
        itemEl.className = 'search-form__item';
        itemEl.innerHTML = `
          <a class="search-result" href="${article.link}">
            <div class="search-result__title">${article.title}</div>
            <div class="search-result__descr">${article.description}</div>
          </a>
        `;
        resultContainer.append(itemEl);
      }
      
      formSearch.classList.add('active');
      body.classList.add('active');
    }
    
    function hiddenResult(message) {
      resultContainer.innerHTML = message;
      resultContainer.classList.remove('is-active');
    }
    
    function request(data, callback) {
      data.action = 'get_posts';
      
      axios.post(path.ajax, Qs.stringify(data))
        .then(response => callback(response.data))
        .catch(error => console.log(error));
    }
  }
}