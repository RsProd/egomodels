import axios from 'axios';

class FormValidation {
  constructor() {
    this.DOM = {
      name: document.querySelector('[name="abb-name"]'),
      dateOfBirth: document.querySelector('[name="date-of-birth"]'),
      phone: document.querySelector('[name="number"]')
    };
    this.errors = {
      required: modelForm.formErrors['fieldRequired'],
      email: modelForm.formErrors['wrongEmail']
    };
  }
  
  validateStep(stepNumber) {
    const firstStepWrapper = document.querySelector(
      `[data-form-step='${stepNumber}']`
    );
    const elements = [
      ...firstStepWrapper.querySelectorAll('[data-validation]')
    ];
    const validationResult = elements
      .map((el) => {
        let r = this.validate(el);
        r = r.filter((res) => res !== true);
        return r.length;
      })
      .filter((res) => res > 0);
    return validationResult.length ? false : true;
  }
  
  validate(el) {
    const validationTypes = el.dataset.validation.split(' ');
    return validationTypes.map((type) => {
      let res;
      if (type === 'required') {
        res =
          el.type === 'checkbox'
            ? this.validateRequiredCheckBox(el)
            : this.validateRequiredTextInput(el);
      }
      if (type === 'email') {
        res = this.validateEmail(el);
      }
      return res;
    });
  }
  
  validateRequiredTextInput(el) {
    let isValid = true;
    const parentField = el.closest('.form__field');
    if (el.value === '') {
      this.createError(parentField, 'required');
      isValid = false;
    } else {
      this.clearError(parentField);
    }
    return isValid;
  }
  
  validateRequiredCheckBox(el) {
    let isValid = true;
    if (!el.checked) {
      el.closest('.checkbox').classList.add('is-error');
      isValid = false;
    } else {
      el.closest('.checkbox ').classList.remove('is-error');
    }
    return isValid;
  }
  
  validateEmail(el) {
    let isValid = true;
    const val = el.value;
    if (!val) return;
    const emailRegEx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,10}$/g;
    const validEmail = val.match(emailRegEx);
    const parentField = el.closest('.form__field');
    if (!validEmail) {
      this.createError(parentField, 'email');
      isValid = false;
    } else {
      this.clearError(parentField, 'email');
    }
    return isValid;
  }
  
  createError(parentElement, errorType) {
    const errorElement = parentElement.querySelector('.error');
    if (errorElement) {
      if (errorElement.dataset.errorType === errorType) {
        return;
      } else {
        error.innerHTML = this.errors[errorType];
      }
    } else {
      const error = document.createElement('span');
      error.className = 'error';
      error.innerHTML = this.errors[errorType];
      error.setAttribute('data-error-type', errorType);
      parentElement.appendChild(error);
    }
  }
  
  clearError(parentElement) {
    const error = parentElement.querySelector('.error');
    if (error) {
      error.remove();
    }
  }
}

class FileUploader {
  constructor() {
    this.images = [];
    this.modelForm = document.querySelector('.js-become-model-form');
    this.dropZone = document.querySelector('.drop-zone');
    this.fileInput = document.querySelector('.file-uploader__input');
    this.preview = document.querySelector('.files-preview');
    this.createUploader();
  }
  
  toggleModelForm(action) {
    this.modelForm.classList[action]('is-dragover');
  }
  
  preventEvents(e) {
    e.preventDefault();
    e.stopPropagation();
  }
  
  isAdvancedUpload() {
    var div = document.createElement('div');
    return (
      ('draggable' in div || ('ondragstart' in div && 'ondrop' in div)) &&
      'FormData' in window &&
      'FileReader' in window
    );
  }
  
  createUploader() {
    if (this.isAdvancedUpload()) {
      this.modelForm.classList.add('has-advanced-upload');
      
      this.dropZone.addEventListener('drag', this.preventEvents);
      this.dropZone.addEventListener('dragstart', this.preventEvents);
      this.dropZone.addEventListener('dragend', this.preventEvents);
      this.dropZone.addEventListener('dragover', this.preventEvents);
      this.dropZone.addEventListener('dragenter', this.preventEvents);
      this.dropZone.addEventListener('dragleave', this.preventEvents);
      this.dropZone.addEventListener('drop', this.preventEvents);
      
      this.dropZone.addEventListener('dragover', () =>
        this.toggleModelForm('add')
      );
      this.dropZone.addEventListener('dragenter', () =>
        this.toggleModelForm('add')
      );
      
      this.dropZone.addEventListener('dragleave', () =>
        this.toggleModelForm('remove')
      );
      this.dropZone.addEventListener('dragend', () =>
        this.toggleModelForm('remove')
      );
      this.dropZone.addEventListener('drop', () =>
        this.toggleModelForm('remove')
      );
      
      this.dropZone.addEventListener('drop', (e) => {
        this.modelForm.classList.add('files-added');
        this.handleFiles(e.dataTransfer.files);
      });
      
      this.fileInput.addEventListener('change', (e) => {
        this.modelForm.classList.add('files-added');
        this.handleFiles(e.srcElement.files);
      });
    }
  }
  
  handleFiles(files) {
    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      
      if (!file.type.startsWith('image/')) {
        continue;
      }
      
      const uuid = `f${(+new Date()).toString(16)}`;
      
      const itemWrapper = document.createElement('div');
      itemWrapper.classList.add('files-preview__item');
      
      const imageWrapper = document.createElement('div');
      imageWrapper.classList.add('files-preview__image-wrapper');
      
      const imageDeleteBtn = document.createElement('button');
      imageDeleteBtn.type = 'button';
      imageDeleteBtn.classList.add('files-preview__delete-btn');
      
      const imageDeleteSpan = document.createElement('span');
      imageDeleteSpan.classList.add('files-preview__delete-inner');
      
      const img = document.createElement('img');
      img.classList.add('files-preview__img');
      
      imageDeleteBtn.appendChild(imageDeleteSpan);
      imageWrapper.appendChild(img);
      itemWrapper.appendChild(imageDeleteBtn);
      itemWrapper.appendChild(imageWrapper);
      
      img.file = file;
      this.preview.appendChild(itemWrapper);
      
      var reader = new FileReader();
      reader.onload = (function(aImg) {
        return function(e) {
          aImg.src = e.target.result;
        };
      })(img);
      reader.readAsDataURL(file);
      
      this.images.push({
        filename: `file-${uuid}`,
        file
      });
      
      imageDeleteBtn.addEventListener('click', (e) => {
        this.images = this.images.filter(
          (el) => el.filename !== `file-${uuid}`
        );
        itemWrapper.remove();
      });
    }
  }
  
  createImagesData() {
    return this.images;
  }
}

// Steps
const formStepsHandler = () => {
  const form = new FormValidation();
  const fileUploader = new FileUploader();
  
  const btn = document.querySelector('[data-form-step-trigger]');
  
  const step1 = document.querySelector('[data-form-step=\'1\']');
  const step2 = document.querySelector('[data-form-step=\'2\']');
  const step3 = document.querySelector('[data-form-step=\'3\']');
  
  const mark1 = document.querySelector('[data-form-step-mark=\'1\']');
  const mark2 = document.querySelector('[data-form-step-mark=\'2\']');
  const mark3 = document.querySelector('[data-form-step-mark=\'3\']');
  
  const markBtn1 = document.querySelector('[data-form-step-mark-btn=\'1\']');
  const markBtn2 = document.querySelector('[data-form-step-mark-btn=\'2\']');
  const markBtn3 = document.querySelector('[data-form-step-mark-btn=\'3\']');
  
  const activeStep1 = () => {
    step1.classList.add('active');
    mark1.classList.add('active');
    step2.classList.remove('active');
    step3.classList.remove('active');
    mark2.classList.remove('active');
    mark3.classList.remove('active');
  };
  
  const activeStep2 = () => {
    step2.classList.add('active');
    mark2.classList.add('active');
    step1.classList.remove('active');
    step3.classList.remove('active');
    mark3.classList.remove('active');
  };
  
  const activeStep3 = () => {
    step3.classList.add('active');
    mark3.classList.add('active');
    step2.classList.remove('active');
    step1.classList.remove('active');
  };
  
  const preloader = (function() {
    const form = document.querySelector('.js-become-model-form');
    const load = document.createElement('div');
    const icon = document.createElement('div');
    
    load.className = 'form__preloader';
    icon.className = 'form__preloader-icon';
    
    load.append(icon);
    
    function add() {
      form.append(load);
      setTimeout(() => load.classList.add('show'), 100);
    }
    
    function remove() {
      load.classList.remove('show');
      setTimeout(() => load.remove(), 300);
    }
    
    return {
      add: add,
      remove: remove
    };
  })();
  
  const submitForm = () => {
    const form = document.querySelector('.js-become-model-form');
    const data = new FormData(form);
    const images = fileUploader.createImagesData();
    
    images.forEach(function(element) {
      data.append('photos[]', element.file);
    });
    
    preloader.add();
    
    request(data, (response) => {
      preloader.remove();
      
      response
        ? responseModelFormMessage(modelForm.modelFormSuccessText, '')
        : responseModelFormMessage(modelForm.modelFormErrorText, 'is-error');
    });
  };
  
  function responseModelFormMessage(msg, cls) {
    const successModal = document.createElement('div');
    successModal.className = `model-form-response-message ${cls}`;
    successModal.innerHTML = `
      <div class="model-form-response-message__text">
        ${msg}
      <div>
    `;
    const form = document.querySelector('.js-become-model-form');
    const btnText = btn.querySelector('.button__name');
    
    form.appendChild(successModal);
    activeStep1();
    form.reset();
    btnText.textContent = modelForm.modelFormNextStepBtnText;
    btn.setAttribute('data-form-step-trigger', '2');
    setTimeout(() => {
      successModal.classList.add('hide');
      setTimeout(() => successModal.remove(), 300);
    }, 3000);
  }
  
  function stepsHandler(e) {
    e.preventDefault();
    
    const target = e.currentTarget;
    const targetType = target.classList.contains('form-navigation__num')
      ? 'nav'
      : 'btn';
    
    const nextStep = target.getAttribute(
      targetType === 'btn'
        ? 'data-form-step-trigger'
        : 'data-form-step-mark-btn'
    );
    const btnText = btn.querySelector('.button__name');
    
    switch (nextStep) {
      case '1':
        activeStep1();
        btn.setAttribute('data-form-step-trigger', '2');
        btnText.textContent = modelForm.modelFormNextStepBtnText;
        
        break;
      case '2':
        const validateFirstStep = form.validateStep(1);
        if (!validateFirstStep) return false;
        activeStep2();
        btn.setAttribute('data-form-step-trigger', '3');
        btnText.textContent = modelForm.modelFormNextStepBtnText;
        break;
      case '3':
        const validateStep1 = form.validateStep(1);
        const validateStep2 = form.validateStep(2);
        if (!validateStep1 || !validateStep2) return false;
        activeStep3();
        btn.type = 'submit';
        btn.setAttribute('data-form-step-trigger', 'submit');
        btnText.textContent = modelForm.modelFormSubmitBtnText;
        break;
      case 'submit':
        submitForm();
        break;
    }
  }
  
  document.querySelector('.js-become-model-form input[name=\'number\']').addEventListener('input', function() {
    const parent = this.closest('.form__field');
    const code = parent.querySelector('.iti__selected-dial-code').innerHTML;
    parent.querySelector('input[name="full-number"]').value = code + this.value;
  });
  
  btn.addEventListener('click', stepsHandler);
  markBtn1.addEventListener('click', stepsHandler);
  markBtn2.addEventListener('click', stepsHandler);
  markBtn3.addEventListener('click', stepsHandler);
};

const formHelperMethods = () => {
  const affiliatesRow = document.querySelector('.js-affiliates-row');
  const affiliatesCheckbox = document.querySelector('.js-affiliates-checkbox');
  affiliatesCheckbox.addEventListener('change', (e) => {
    affiliatesRow.classList[affiliatesCheckbox.checked ? 'add' : 'remove'](
      'is-show'
    );
  });
};

const becomeModelForm = () => {
  if (document.querySelector('.js-become-model-form') !== null) {
    formStepsHandler();
    formHelperMethods();
  }
};

const request = (data, callback) => {
  data.append('action', 'send_form');
  
  axios
    .post(path.ajax, data)
    .then((response) => callback(response.data))
    .catch((error) => console.log(error));
};

export default becomeModelForm;
