const preloader = () => {
  window.addEventListener('load', function() {
    let body = document.querySelector('body');
    let preloader = document.querySelector('.preloader');
    let timeout = 1000;
    
    body.classList.remove('preload');
    setTimeout(function() {
      preloader.classList.remove('preloader_load');
    }, timeout);
  });
};

export default preloader;