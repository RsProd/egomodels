import Cookies from 'js-cookie';

const colorToggle = () => {
	const rootContainer = document.querySelector('html');
	const buttonContainer = document.querySelectorAll('.scheme-buttons');
	
	if (buttonContainer !== null) {
		for (let i = 0; buttonContainer.length > i; i++) {
			let buttons = buttonContainer[i].children;
			for (let i = 0; buttons.length > i; i++) {
				buttons[i].addEventListener('click', function() {
					
					const  scheme = this.getAttribute('data-scheme');
					for (let j = 0; buttons.length > j; j++) {
						let a = buttons[j].getAttribute('data-scheme');
						rootContainer.classList.remove(a);
					}
					Cookies.set('scheme', scheme, {
						expires: 365
					});
					rootContainer.classList.add(scheme);
				});
			}
		}
	}
};

export default colorToggle;
