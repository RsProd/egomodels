import axios from 'axios';
import Qs from 'qs';
import Validation from './validation';

export default class CommentsPost {
  constructor() {
    this.commentsForm = document.querySelector('.comments-form');
    
    if (this.commentsForm) {
      this.fields = this.commentsForm.querySelectorAll('input, textarea');
      this.commentsButton = this.commentsForm.querySelector('button');
      
      this.commentsList = document.querySelector('.comments-list');
      this.loadButton = document.querySelector('.loadmore_comments');
    }
    
    this.formData = {};
    this.offset = 0;
  }
  
  setFormData = () => {
    this.fields.forEach((field) => {
      this.formData[field.name] = field.value;
    });
    
  };
  
  like = () => {
    document.addEventListener('click', (e) => {
      const clickEl = e.target;
      const element = clickEl.closest('.like');
      const commentID = clickEl.closest('.comment') ?
        clickEl.closest('.comment').getAttribute('data-comment-id') :
        false;
      
      if (element) {
        const counter = element.querySelector('.like__count');
        
        const data = {
          action: 'set_like',
          comment_id: commentID
        };
        
        this.request(data, (response) => {
          counter.innerHTML = response;
          element.classList.toggle('active');
        });
      }
    });
  };
  
  reply = () => {
    document.addEventListener('click', (e) => {
      const target = e.target;
      const button = target.closest('.comment__reply');
      const formPosition = this.commentsForm.getBoundingClientRect().top + window.pageYOffset - 300;
      const commentField = this.commentsForm.querySelector('.comments-form__field textarea');
      const replyTo = document.querySelector('.reply-to');
      
      const createReplyTo = document.createElement('div');
      const createReplyClose = document.createElement('span');
      
      createReplyTo.className = 'reply-to';
      createReplyClose.className = 'reply-to__close';
      createReplyClose.innerText = 'Отменить';
      
      if (button) {
        const commentEl = button.closest('.comment');
        const id = commentEl.getAttribute('data-comment-id');
        const name = commentEl.querySelector('.comment__name').innerHTML;
        const replyName = '@' + name + ',';
        
        this.formData['reply'] = id;
        commentField.value = replyName;
        createReplyTo.innerText = 'Ответ пользователю ' + name;
        createReplyTo.append(createReplyClose);
        
        if (replyTo !== null) replyTo.remove();
        commentField.after(createReplyTo);
        
        window.scrollTo({
          top: formPosition,
          behavior: 'smooth'
        });
        
      }
      
      if (target.closest('.reply-to__close')) {
        commentField.value = '';
        replyTo.remove();
        delete this.formData['reply'];
      }
      
    });
  };
  
  setRating = () => {
    document.querySelectorAll('.rating').forEach((field) => {
      const value = +field.getAttribute('data-value');
      const items = field.querySelectorAll('.rating__item');
      const input = field.querySelector('.rating__input');
      
      items.forEach((item, index) => {
        
        if ((index + 1) <= value) item.classList.add('active');
        
        if (input) {
          item.addEventListener('click', () => {
            input.value = index + 1;
            items.forEach((el, i) => i <= index ?
              el.classList.add('active') : el.classList.remove('active'));
          });
        }
      });
      
    });
  };
  
  sendComment = () => {
    this.commentsForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.setFormData();
      const messageCreate = document.createElement('span');
      messageCreate.className = 'comments-form__message';
      
      const data = {
        action: 'send_comment',
        ...this.formData
      };
      
      const valid = new Validation(e.target);
      
      if (valid.complete()) {
        
        this.commentsButton.disabled = true;
        
        this.request(data, (response) => {
          messageCreate.innerHTML = response.data;
          this.commentsForm.append(messageCreate);
          this.commentsButton.disabled = false;
          
          this.commentsForm.reset();
          this.formData = {};
          
          if (document.querySelector('.reply-to')) {
            document.querySelector('.reply-to').remove();
          }
          
          setTimeout(() => {
            document.querySelector('.comments-form__message').remove();
          }, 3000);
        });
        
      }
      
    });
  };
  
  outputComments = (comments, appendTo, itemClass = 'comments__item') => {
    
    comments.forEach((item, i) => {
      const itemEl = document.createElement('div');
      
      itemEl.className = itemClass;
      itemEl.innerHTML = renderCommentTemplate(item);
      
      if (item.children !== undefined) {
        this.outputComments(item.children, itemEl, 'comments__sub-item');
      }
      
      appendTo.append(itemEl);
      
      setTimeout(() => itemEl.classList.add('load'), 100 * (i + 1));
      
    });
  };
  
  loadComments = () => {
    if (this.loadButton !== null) {
      this.loadButton.addEventListener('click', () => {
        const offset = this.commentsList.children.length;
        
        const data = {
          action: 'get_comments',
          offset: offset
        };
        
        this.loadButton.classList.add('disabled');
        
        this.request(data, (response) => {
          this.outputComments(response.comments, this.commentsList);
          const offset = this.commentsList.children.length;
          
          this.setRating();
          (+response.count === offset) ?
            this.loadButton.remove() :
            this.loadButton.classList.remove('disabled');
        });
      });
    }
  };
  
  request = (data, callback) => {
    data.post_id = comments.post_id;
    
    axios.post(path.ajax, Qs.stringify(data))
      .then(response => callback(response.data))
      .catch(error => console.log(error));
    
  };
  
  init() {
    if (this.commentsForm) {
      this.reply();
      this.sendComment();
      this.like();
      this.setRating();
      this.loadComments();
    }
  }
}
