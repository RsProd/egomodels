import { debounce } from './helpers';

import CardSlider from './thumbnails-slider';
import noUiSlider from 'nouislider';
import axios from 'axios';
import Qs from 'qs';

export default class AjaxFilter {
  constructor() {
    this.filter = document.querySelector('.filter');
    this.catalogContainer = document.querySelector('.catalog');
    
    if (this.filter) {
      this.slideButton = this.filter.querySelectorAll('.filter__title');
      this.locationInput = this.filter.querySelector('.search-input');
      this.locationList = this.filter.querySelector('.search-list');
      this.checkboxFields = this.filter.querySelectorAll('.checkbox');
      this.range = this.filter.querySelectorAll('.rangeSlider');
      this.resetButton = document.querySelector('.reset-filter');
      this.preloader = document.querySelector('#loading');
      this.ajaxContainer = document.querySelector('.ajax-container');
      this.count = document.querySelector('.find-total span');
      
      this.fetch = false;
      this.rangeSliderChange = false;
      
      this.offset = 0;
      this.category = +loadMore.models;
      this.filterData = {};
    }
  }
  
  setData = (type, key, value) => {
    if (type === 'add') {
      !this.filterData[key] && (this.filterData[key] = []);
      if (!this.filterData[key].includes(value)) {
        if (Array.isArray(value)) {
          this.filterData[key].splice(0, this.filterData[key].length);
          this.filterData[key].push(Math.floor(value[0]), Math.floor(value[1]));
        } else {
          this.filterData[key].push(value);
        }
      }
    } else {
      this.filterData[key] = this.filterData[key].filter(item => item !== value);
      if (this.filterData[key].length === 0) delete this.filterData[key];
    }
    this.offset = 0;
  };
  
  slideBlock = () => {
    
    for (let i = 0; i < this.slideButton.length; i++) {
      
      const toggleInner = (el) => {
        const inner = el.nextElementSibling;
        
        el.classList.contains('active') ?
          inner.style.maxHeight = inner.scrollHeight + 'px' :
          inner.style.maxHeight = null;
      };
      
      toggleInner(this.slideButton[i]);
      
      this.slideButton[i].addEventListener('click', () => {
        this.slideButton[i].classList.toggle('active');
        toggleInner(this.slideButton[i]);
      });
    }
  };
  
  setCategory = () => {
    if (this.category) {
      this.setData('add', 'catalog', this.category);
    }
  };
  
  locationSearch = () => {
    const self = this;
    
    function handler() {
      const filter = this.value.toUpperCase();
      const li = self.locationList.getElementsByTagName('li');
      for (let i = 0; i < li.length; i++) {
        li[i].innerHTML.toUpperCase().indexOf(filter) > -1 ?
          li[i].style.display = '' :
          li[i].style.display = 'none';
      }
    }
    
    this.locationInput.addEventListener('input', handler);
  };
  
  checkbox = () => {
    
    const debounceRequest = debounce(() => {
      this.request(true);
      this.fetch = false;
    }, 1500);
    
    for (let i = 0; i < this.checkboxFields.length; i++) {
      const input = this.checkboxFields[i].querySelector('.checkbox__input');
      
      if (!input.closest('.checkbox_disabled')) {
        input.checked = false;
      }
      
      input.addEventListener('change', () => {
        const key = input.getAttribute('name');
        const value = +input.value;
        
        this.loading();
        
        input.checked ?
          this.setData('add', key, value) :
          this.setData('remove', key, value);
        
        this.fetch = true;
        debounceRequest();
      });
    }
  };
  
  rangeSlider = () => {
    const debounceRequest = debounce(() => {
      this.request(true);
    }, 500);
    
    for (let i = 0; this.range.length > i; i++) {
      let rangeLabels = this.range[i].children[0],
        rangeInner = this.range[i].children[1],
        dataMinAttr = +this.range[i].getAttribute('data-min'),
        dataMaxAttr = +this.range[i].getAttribute('data-max');
      
      noUiSlider.create(rangeInner, {
        start: [dataMinAttr, dataMaxAttr],
        step: 1,
        connect: true,
        range: {
          'min': dataMinAttr,
          'max': dataMaxAttr
        }
      });
      
      rangeInner.noUiSlider.on('update', (str, handle, value) => {
        rangeLabels.children[handle].innerHTML = Math.floor(value[handle]);
      });
      
      rangeInner.noUiSlider.on('change.one', (str, handle, value) => {
        const key = this.range[i].getAttribute('data-name');
        
        this.loading();
        this.setData('add', key, value);
        this.rangeSliderChange = true;
        
        debounceRequest();
      });
    }
  };
  
  reset = () => {
    this.resetButton.addEventListener('click', () => {
      this.filter.reset();
      this.filterData = {};
      this.offset = 0;
      this.range.forEach(el => el.children[1].noUiSlider.reset());
      this.resetButton.classList.remove('active');
      this.rangeSliderChange = false;
      this.setCategory();
      this.loading();
      this.request(true);
    });
  };
  
  load = () => {
    const scrollArea = document.querySelector('html');
    
    const debounceRequest = debounce(() => {
      this.preloaderScroll();
      this.request();
    }, 100);
    
    window.addEventListener('scroll', () => {
      const offset = document.querySelector('.footer').clientHeight * 2;
      const loading = scrollArea.scrollTop + scrollArea.clientHeight + offset >= scrollArea.scrollHeight && !this.fetch;
      
      if (loading) {
        this.offset = this.ajaxContainer.children.length;
        debounceRequest();
      }
    });
  };
  
  loading = () => {
    const container = this.preloader.content.cloneNode(true);
    this.ajaxContainer.innerHTML = '';
    this.ajaxContainer.append(container);
  };
  
  preloaderScroll = () => {
    const preload = document.createElement('div');
    const span = document.createElement('span');
    preload.className = 'catalog__preloader';
    for (let i = 0; i < 3; i++) {
      preload.append(span.cloneNode());
    }
    
    this.ajaxContainer.parentElement.append(preload);
    setTimeout(() => preload.style.opacity = '1', 100);
    
  };
  
  print = (data, redrawing) => {
    
    if (redrawing) {
      this.count.innerHTML = data.total;
      this.ajaxContainer.querySelector('.loading').remove();
    } else {
      this.ajaxContainer.parentElement.querySelector('.catalog__preloader').remove();
    }
    
    data.posts.forEach((el, i) => {
      const gridEl = document.createElement('div');
      gridEl.className = 'col-lg-4 col-md-4 col-sm-4 col-xs-6 grid-item';
      gridEl.innerHTML = renderAjaxCardProfile(el);
      this.ajaxContainer.append(gridEl);
      
      const card = gridEl.querySelector('.card-profile__photo');
      const slider = new CardSlider(card);
      slider.init();
      
      setTimeout(() => gridEl.classList.add('load'), 100 * (i + 1));
    });
    
    const visibleCount = this.ajaxContainer.children.length;
    
    visibleCount === data.total ?
      this.fetch = true :
      this.fetch = false;
  };
  
  request = (redrawing) => {
    const data = {
      action: 'get_models',
      offset: this.offset,
      filter: {
        ...this.filterData
      }
    };
    
    axios.post(path.ajax, Qs.stringify(data))
      .then(response => this.print(response.data, redrawing))
      .catch(error => console.log(error));
    
    this.fetch = true;
    
    const checkboxActive = Array.from(this.filter.querySelectorAll('input[type="checkbox"]:checked'));
    const activeLength = checkboxActive.filter((e) => +e.value !== +this.category).length;
    
    (activeLength !== 0 || this.rangeSliderChange) ?
      this.resetButton.classList.add('active') :
      this.resetButton.classList.remove('active');
  };
  
  init() {
    if (this.filter) {
      this.setCategory();
      this.slideBlock();
      this.locationSearch();
      this.checkbox();
      this.rangeSlider();
      this.reset();
      this.load();
    }
  }
}
