export default function() {
	let langBtn = document.querySelector('.lang__current');
	if (langBtn != null) {
		langBtn.onclick = function(e) {
			e.stopPropagation();
			this.parentNode.classList.toggle('active');
		};
		document.onclick = function() {
			document.querySelector('.lang_desktop').classList.remove('active');
		};
	}
}
