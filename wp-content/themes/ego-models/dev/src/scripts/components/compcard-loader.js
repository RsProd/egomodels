import html2pdf from 'html2pdf.js';

export default class GeneratePdf {
  options = {
    margin: 0,
    image: {
      type: 'png',
      quality: 1
    },
    html2canvas: {
      dpi: 300,
      useCORS: true,
      letterRendering: false,
      removeContainer: true
    },
    jsPDF: {
      unit: 'mm',
      orientation: 'landscape'
    }
  };
  
  constructor(cardId = 'compcard', options = {}) {
    this.compcard = document.getElementById(cardId);
    this.options = Object.assign(this.options, options);
  }
  
  getModelName() {
    const name = this.compcard.querySelector('.card__name').innerHTML;
    return name.replace(' ', '-').toLowerCase();
  }
  
  createFileName() {
    return `egomodels-${this.getModelName()}.pdf`;
  }
  
  createPDF() {
    html2pdf()
      .set(this.options)
      .from(this.compcard)
      .save(this.createFileName());
  }
  
  addHandler() {
    if (this.compcard) {
      const btn = this.compcard.querySelector('.card__download');
      btn.addEventListener('click', () => this.createPDF());
    }
  }
  
  init() {
    this.addHandler();
  }
}
