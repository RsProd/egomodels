'use strict';

import axios from 'axios';

export default class Calendar {
  
  constructor() {
    this.options = CalendarOptions;
    
    this.date = {
      dayWeek: new Date().getDay(),
      day: new Date().getDate(),
      month: new Date().getMonth(),
      year: new Date().getFullYear()
    };
    
    this.data = [];
    this.events = [];
    
    this.week = 0;
    
    this.priceMarker = ['marker1', 'marker2', 'marker3', 'marker4', 'marker5', 'marker6', 'marker7'];
    
    this.elements = {
      container: document.querySelector('.calendar'),
      labelsDays: document.querySelector('.calendar__days-label'),
      labelsHours: document.querySelector('.calendar__hours-label'),
      current: document.querySelector('.date-nav__current'),
      days: document.querySelector('.calendar__week-days')
    };
    
    this.formData = {
      total_price: 0,
      date: {}
    };
    
  }
  
  getEvents = () => {
    return new Promise((resolve, reject) => {
      
      const dateStart = new Date(this.date.year, this.date.month, this.date.day, +this.options.general.schedule.start, 0);
      const dateEnd = new Date(this.date.year, this.date.month, this.date.day + +this.options.general['day-count'], +this.options.general.schedule.end, 0);
      
      axios.get('https://www.googleapis.com/calendar/v3/calendars/' + this.options.service.calendar_id + '/events', {
        params: {
          timeMin: dateStart,
          timeMax: dateEnd,
          orderBy: 'startTime',
          singleEvents: true,
          key: this.options.service.api_key
        }
      }).then((response) => {
        
        response.data.items.forEach((item) => {
          const start = new Date(item.start.dateTime);
          const end = new Date(item.end.dateTime);
          const fullDateStart = start.toLocaleString().slice(0, 10);
          const time = [];
          
          for (let i = start.getHours(); i < end.getHours(); i++) {
            time.push(i);
          }
          
          (!this.events[fullDateStart]) ?
            this.events[fullDateStart] = time :
            this.events[fullDateStart].push(...time);
          
        });
        resolve();
        
      }).catch(error => reject(error));
    });
  };
  
  set = () => {
    const week = Math.floor(+this.options.general['day-count'] / 7);
    
    let count = 0;
    
    for (let i = 0; i < week; i++) {
      
      const dateStart = new Date(this.date.year, this.date.month, this.date.day + (7 * i), 0, 0);
      const dateEnd = new Date(this.date.year, this.date.month, this.date.day + (7 * i) + 6, 0, 0);
      const current = `${dateStart.getDate()} ${this.options.labels.months[dateStart.getMonth()]} - ${dateEnd.getDate()} ${this.options.labels.months[dateEnd.getMonth()]}`;
      
      this.data[i] = {
        top: {
          current,
          labels: []
        },
        days: []
      };
      
      for (let j = 0; j < 7; j++) {
        const date = new Date(this.date.year, this.date.month, this.date.day + count++, 0, 0);
        const fullDate = date.toLocaleString().slice(0, 10);
        const day = date.getDay();
        const dayLabel = `${this.options.labels.days[day]}, ${date.getDate()}`;
        const datetime = {};
        
        for (let time = +this.options.general.schedule.start; time <= +this.options.general.schedule.end; time++) {
          const key = fullDate + 'T' + time;
          const ev = this.events[fullDate] && this.events[fullDate].includes(time);
          const disabled = (ev) ? ev : false;
          
          datetime[key] = {
            selected: false,
            disabled,
            price: {
              value: this.options.prices[day].value,
              marker: this.options.prices[day].marker
            }
          };
          
        }
        this.data[i].top.labels.push(dayLabel);
        this.data[i].days.push(datetime);
        
      }
    }
  };
  
  print = () => {
    const data = this.data[0];
    
    const hoursLabel = document.createElement('span');
    
    const dayLabel = document.createElement('div');
    dayLabel.className = 'calendar__day-label';
    
    const dayEl = document.createElement('div');
    dayEl.className = 'calendar__day';
    
    const hourEl = document.createElement('div');
    hourEl.className = 'calendar__hour';
    
    this.elements.current.innerHTML = data.top.current;
    
    for (let time = +this.options.general.schedule.start; time <= +this.options.general.schedule.end; time++) {
      const hoursLabelEl = hoursLabel.cloneNode();
      
      hoursLabelEl.innerHTML = `${String(time).padStart(2, 0)}:00 - ${String(time + 1).padStart(2, 0)}:00`;
      this.elements.labelsHours.append(hoursLabelEl);
    }
    
    data.top.labels.forEach((item, i) => {
      const dayLabelEl = dayLabel.cloneNode();
      const inner = document.createElement('span');
      if (i === 0) dayLabelEl.classList.add('calendar__day-label_active');
      
      inner.innerHTML = item;
      dayLabelEl.append(inner);
      this.elements.labelsDays.append(dayLabelEl);
    });
    
    data.days.forEach((item) => {
      const onlyDayEl = dayEl.cloneNode();
      
      for (const day in item) {
        const hours = item[day];
        const onlyHourEl = hourEl.cloneNode();
        
        hours.disabled ? onlyHourEl.classList.add('disabled') : '';
        onlyHourEl.setAttribute('data-datetime', day);
        onlyHourEl.setAttribute('data-price', hours.price.value);
        onlyHourEl.classList.add(hours.price.marker);
        onlyDayEl.appendChild(onlyHourEl);
      }
      
      this.elements.days.append(onlyDayEl);
    });
  };
  
  update = (index) => {
    const data = this.data[index];
    
    this.elements.current.innerHTML = data.top.current;
    
    (index === 0) ?
      this.elements.labelsDays.children[0].classList.add('calendar__day-label_active') :
      this.elements.labelsDays.children[0].classList.remove('calendar__day-label_active');
    
    data.top.labels.forEach((label, i) => {
      this.elements.labelsDays.children[i].children[0].innerHTML = label;
    });
    
    data.days.forEach((item, i) => {
      let a = 0;
      for (const dataTime in item) {
        const dayEl = this.elements.days.children[i].children[a];
        dayEl.setAttribute('data-datetime', dataTime);
        dayEl.setAttribute('data-price', item[dataTime].price.value);
        
        (item[dataTime].selected) ?
          dayEl.classList.add('active') :
          dayEl.classList.remove('active');
        
        (item[dataTime].disabled) ?
          dayEl.classList.add('disabled') :
          dayEl.classList.remove('disabled');
        a++;
      }
    });
    
  };
  
  calculation = (element) => {
    const el = document.querySelector('.total-price__value');
    const price = +element.getAttribute('data-price');
    
    element.classList.contains('active') ?
      this.formData['total_price'] = this.formData['total_price'] + price :
      this.formData['total_price'] = this.formData['total_price'] - price;
    
    document.querySelector('input[name=\'price\']').value = this.formData['total_price'];
    
    el.innerHTML = this.formData['total_price'] + ' ₴';
  };
  
  navigation = () => {
    const prev = document.querySelector('.date-nav__prev');
    const next = document.querySelector('.date-nav__next');
    const weekCount = this.data.length - 1;
    
    prev.addEventListener('click', () => {
      next.classList.remove('disabled');
      if (this.week !== 0) {
        this.update(--this.week);
        if (this.week === 0) {
          prev.classList.add('disabled');
        }
      }
    });
    
    next.addEventListener('click', () => {
      prev.classList.remove('disabled');
      if (this.week !== weekCount) {
        this.update(++this.week);
        if (this.week === weekCount) {
          next.classList.add('disabled');
        }
      }
    });
    
  };
  
  setFrom = (t) => {
    const dateTime = t.split('T');
    
  };
  
  select = () => {
    document.addEventListener('click', (e) => {
      const el = e.target;
      
      if (el.closest('.calendar__hour')) {
        const weekData = this.data[this.week].days;
        const dateTime = el.getAttribute('data-datetime');
        
        el.classList.toggle('active');
        
        for (const key in weekData) {
          if (weekData[key][dateTime] !== undefined) {
            weekData[key][dateTime]['selected'] = el.classList.contains('active');
          }
        }
        
        // this.setFrom();
        this.calculation(el);
      }
    });
  };
  
  init() {
    if (this.elements.container) {
      this.getEvents().then(() => {
        this.set();
        this.print();
        this.navigation();
        this.select();
      });
    }
  }
}
