import axios from "axios";
import Qs from "qs";

export default function () {
  const loadmoreBtn = document.querySelector(".js-loadmore-blogs");
  const blogsContainer = document.querySelector(".js-blogs-container");
  if (!loadmoreBtn) return;

  const catID = loadMore.blogs;

  loadmoreBtn.addEventListener("click", (e) => {
    const currentBlogsNumber = blogsContainer.children.length;

    loadmoreBtn.classList.add("disabled");

    const data = {
      action: "load_more_blogs",
      offset: currentBlogsNumber,
    };

    if (catID) {
      data['cat'] = catID;
    }

    axios
      .post(path.ajax, Qs.stringify(data))
      .then((response) => {
        const restPosts = parseInt(response.data.total) - currentBlogsNumber;
        loadmoreBtn.classList.remove("disabled");
        if (restPosts < response.data.posts_per_page) {
          loadmoreBtn.remove();
        }

        response.data.posts.forEach((el) => {
          const card = document.createElement("div");
          card.className = "col-lg-4 col-md-4 col-sm-6 col-xs-12";
          card.innerHTML = renderAjaxBlogCard(el);
          blogsContainer.append(card);
        });
      })
      .catch((error) => console.log(error));
  });
}
