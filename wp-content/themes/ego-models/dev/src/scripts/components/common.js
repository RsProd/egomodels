import Swiper, { Navigation } from 'swiper';
import CardSlider from './thumbnails-slider';

Swiper.use([Navigation]);

const common = () => {
  new Swiper('.slider__inner', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.slider__btn-next',
      prevEl: '.slider__btn-prev'
    }
  });
  
  (function() {
    const cards = document.querySelectorAll('.card-profile');
    cards.forEach((card) => {
      const container = card.querySelector('.card-profile__photo');
      const slider = new CardSlider(container);
      slider.init();
    });
  })();
  
};

export default common;

