import gsap from 'gsap';

export default class Video {
  constructor() {
    this.section = document.querySelector('.section_video');
    this.badge = document.querySelector('.video-badge');
    this.video = document.querySelector('.video__video');
    this.videoPlaying = false;
    
    this.following = false;
    
    if (this.section) {
      this.tX = this.section.clientWidth / 2 - this.badge.clientWidth / 2;
      this.tY = this.section.clientHeight / 2 - this.badge.clientHeight / 2;
      
      this.pX = this.section.clientWidth / 2 - this.badge.clientWidth / 2;
      this.pY = this.section.clientHeight / 2 - this.badge.clientHeight / 2;
    }
    
    this.math = {
      lerp: (a, b, n) => (1 - n) * a + n * b
    };
  }
  
  track = (e) => {
    if (this.following === false) {
      this.tX = this.section.clientWidth / 2 - this.badge.clientWidth / 2;
      this.tY = this.section.clientHeight / 2 - this.badge.clientHeight / 2;
      return;
    }
    this.tX = e.clientX - this.badge.clientWidth / 2;
    this.tY = e.clientY - this.badge.clientHeight / 2;
  };
  
  render = () => {
    this.wY = this.section.getBoundingClientRect().top;
    this.pX = this.math.lerp(this.pX, this.tX, 0.05);
    
    if (this.following === true) {
      this.pY = this.math.lerp(this.pY, this.tY - this.wY, 0.05);
      this.badge.style.transform = `translate3d(${this.pX}px, ${this.pY}px, 0)`;
    } else {
      this.pY = this.math.lerp(this.pY, this.tY, 0.05);
      this.badge.style.transform = `translate3d(${this.pX}px, ${this.pY}px, 0)`;
    }
  };
  
  trackVideo = (e) => {
    this.badge.style.transform = `translate3d(${e.clientX - 60}px, ${e.clientY - 60}px, 0)`;
  };
  
  create = () => {
    gsap.ticker.add(this.render);
    
    this.section.addEventListener('mouseenter', () => {
      this.following = true;
    });
    
    this.section.addEventListener('mouseleave', () => {
      this.following = false;
    });
  };
  
  events = () => {
    window.addEventListener('mousemove', this.track);
    this.badge.addEventListener('click', (e) => {
      (e.currentTarget.closest('.is-video-playing')) ?
        this.stopVideo() :
        this.playVideo();
    });
  };
  
  destroy = () => {
    gsap.ticker.remove(this.render);
    window.removeEventListener('mousemove', this.track);
  };
  
  playVideo = () => {
    this.section.classList.add('is-video-playing');
    this.video.play();
  };
  
  stopVideo = () => {
    this.section.classList.remove('is-video-playing');
    this.video.pause();
  };
  
  init() {
    if (this.section !== null) {
      this.events();
      this.create();
    }
  }
}