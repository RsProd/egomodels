import { Loader } from '@googlemaps/js-api-loader';

const maps = () => {
  const mapEl = document.querySelector('.gmap-init');
  const init = GMaps.options !== null;
  
  if (mapEl && init) {
    const loader = new Loader({
      apiKey: GMaps.api_key,
      version: 'weekly'
    });
    
    loader.load().then(() => {
      
      const cords = {
        lat: GMaps.options.lat,
        lng: GMaps.options.lng
      };
      
      const map = new google.maps.Map(mapEl, {
        center: cords,
        zoom: GMaps.options.zoom,
        disableDefaultUI: true
      });
      
      const marker = new google.maps.Marker({
        position: cords,
        icon: path.theme + '/assets/icons/source/marker.svg'
      });
      
      marker.setMap(map);
    });
  }
};

export default maps;