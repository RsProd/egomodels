import phoneMask from 'intl-tel-input';
import axios from 'axios';

window.intlTelInputGlobals.loadUtils(path.theme + '/assets/scripts/lazy/utils-phone-mask.js');

const mask = () => {
  const fields = document.querySelectorAll('[name="number"]');
  
  fields.forEach((field) => {
    const mask = phoneMask(field, {
      initialCountry: 'auto',
      formatOnDisplay: false,
      autoPlaceholder: 'aggressive',
      preferredCountries: ['ua', 'ru'],
      separateDialCode: true,
      hiddenInput: 'full-number',
      
      geoIpLookup: (success) => {
        axios.get('https://api.sypexgeo.net/')
          .then((response) => {
            success(response.data.country.iso);
          })
          .catch(function(error) {
            success('UA');
          });
      }
    });
    
  });
  
};

export default mask;