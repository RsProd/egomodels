'use strict';

import preloader from './components/preloader';
import ToggleEl from './components/toggle-el';
import langToggle from './components/lang-toggle';
import colorsToggle from './components/colors-toggle';
import cookiePopup from './components/cookie-popup';
import sliders from './components/common';
import becomeModelForm from './components/become-model-form';
import photoTabs from './components/photo-tabs';
import search from './components/search';
import maps from './components/maps';
import modelVideo from './components/video-models';
import loadMoreBlog from './components/loadmore-blogs';
import inputSelect from './components/input-select';
import FBAuth from './components/auth';
import upBtn from './components/up-btn';
import preloadVideo from './components/preload-video';
import phoneMask from './components/phone-mask';

import FavoritesList from './components/favorites';
import Instagram from './components/instagram';
import Video from './components/video';
import AjaxFilter from './components/ajax-filter';
import LightBox from './components/lightbox';
import Calendar from './components/calendar';
import CommentsList from './components/comments';
import Forms from './components/form';
import GeneratePdf from './components/compcard-loader';

document.addEventListener('DOMContentLoaded', () => {
  preloader();
  langToggle();
  colorsToggle();
  cookiePopup(30);
  sliders();
  photoTabs();
  becomeModelForm();
  search();
  maps();
  ToggleEl();
  loadMoreBlog();
  inputSelect();
  preloadVideo();
  modelVideo();
  FBAuth();
  upBtn();
  phoneMask();
  
  const video = new Video();
  const instagram = new Instagram();
  const filter = new AjaxFilter();
  const favoritesList = new FavoritesList();
  const comments = new CommentsList();
  const forms = new Forms();
  const calendar = new Calendar();
  const lightBox = new LightBox();
  const compcardLoader = new GeneratePdf();
  
  video.init();
  instagram.init();
  filter.init();
  favoritesList.init();
  comments.init();
  forms.init();
  calendar.init();
  lightBox.init();
  compcardLoader.init();
});
