<?php
get_header();

$per_page = 9;
$q = get_queried_object();

$models = getModels([
	"catalog" => $q->slug,
	"posts_per_page" => $per_page
]);
?>

<main class="main main_catalog">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php get_sidebar() ?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 relative">
				<div class="catalog">
					<span class="stroke-placeholder stroke-placeholder_catalog">Models</span>
					<div class="page-head">
						<?php breadcrumbs() ?>
						<div class="filter-button filter-button_open"
						     data-toggle=".filter"
						     data-toggle-body="filter-open">
							<span class="filter-button__label"><?php _e('Фильтр', TEXT_DOMAIN) ?></span>
						</div>
						<div class="find-total"><?php _e('Найдено моделей', TEXT_DOMAIN) ?>:
							<span><?php echo $models['total'] ?></span>
						</div>
					</div>
					<div class="row grid-row ajax-container">
						<?php foreach ($models['posts'] as $model): ?>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 grid-item load">
								<?php get_template_part('templates/loops/loop', 'model', $model) ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	get_template_part('templates/catalog/block', 'preloader');
	get_template_part('templates/loops/ajax/loop', 'model');
	?>
</main>
<?php get_footer() ?>
