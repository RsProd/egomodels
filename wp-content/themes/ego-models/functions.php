<?php

define('PRODUCTION', true);
define('VER', 9.3);

define('WEBP_SUPPORT', false);
define('ACF_FIELDS', true);

define('LANG', pll_current_language());
define('LOCALE', pll_current_language('locale'));
define('TEXT_DOMAIN', 'ego-models');

define('TH_PATH', get_template_directory_uri());

require_once('includes/functions/common.php');
require_once('includes/components/common.php');