/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/scripts/app.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/app.js":
/*!****************************!*\
  !*** ./src/scripts/app.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_preloader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/preloader */ "./src/scripts/components/preloader.js");
/* harmony import */ var _components_toggle_el__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/toggle-el */ "./src/scripts/components/toggle-el.js");
/* harmony import */ var _components_lang_toggle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/lang-toggle */ "./src/scripts/components/lang-toggle.js");
/* harmony import */ var _components_colors_toggle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/colors-toggle */ "./src/scripts/components/colors-toggle.js");
/* harmony import */ var _components_cookie_popup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/cookie-popup */ "./src/scripts/components/cookie-popup.js");
/* harmony import */ var _components_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/common */ "./src/scripts/components/common.js");
/* harmony import */ var _components_become_model_form__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/become-model-form */ "./src/scripts/components/become-model-form.js");
/* harmony import */ var _components_photo_tabs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/photo-tabs */ "./src/scripts/components/photo-tabs.js");
/* harmony import */ var _components_search__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/search */ "./src/scripts/components/search.js");
/* harmony import */ var _components_maps__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/maps */ "./src/scripts/components/maps.js");
/* harmony import */ var _components_video_models__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/video-models */ "./src/scripts/components/video-models.js");
/* harmony import */ var _components_loadmore_blogs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/loadmore-blogs */ "./src/scripts/components/loadmore-blogs.js");
/* harmony import */ var _components_input_select__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/input-select */ "./src/scripts/components/input-select.js");
/* harmony import */ var _components_auth__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/auth */ "./src/scripts/components/auth.js");
/* harmony import */ var _components_up_btn__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/up-btn */ "./src/scripts/components/up-btn.js");
/* harmony import */ var _components_preload_video__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/preload-video */ "./src/scripts/components/preload-video.js");
/* harmony import */ var _components_phone_mask__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/phone-mask */ "./src/scripts/components/phone-mask.js");
/* harmony import */ var _components_favorites__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/favorites */ "./src/scripts/components/favorites.js");
/* harmony import */ var _components_instagram__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/instagram */ "./src/scripts/components/instagram.js");
/* harmony import */ var _components_video__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/video */ "./src/scripts/components/video.js");
/* harmony import */ var _components_ajax_filter__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/ajax-filter */ "./src/scripts/components/ajax-filter.js");
/* harmony import */ var _components_lightbox__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/lightbox */ "./src/scripts/components/lightbox.js");
/* harmony import */ var _components_calendar__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/calendar */ "./src/scripts/components/calendar.js");
/* harmony import */ var _components_comments__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/comments */ "./src/scripts/components/comments.js");
/* harmony import */ var _components_form__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/form */ "./src/scripts/components/form.js");
/* harmony import */ var _components_compcard_loader__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/compcard-loader */ "./src/scripts/components/compcard-loader.js");




























document.addEventListener('DOMContentLoaded', function () {
  Object(_components_preloader__WEBPACK_IMPORTED_MODULE_0__["default"])();
  Object(_components_lang_toggle__WEBPACK_IMPORTED_MODULE_2__["default"])();
  Object(_components_colors_toggle__WEBPACK_IMPORTED_MODULE_3__["default"])();
  Object(_components_cookie_popup__WEBPACK_IMPORTED_MODULE_4__["default"])(30);
  Object(_components_common__WEBPACK_IMPORTED_MODULE_5__["default"])();
  Object(_components_photo_tabs__WEBPACK_IMPORTED_MODULE_7__["default"])();
  Object(_components_become_model_form__WEBPACK_IMPORTED_MODULE_6__["default"])();
  Object(_components_search__WEBPACK_IMPORTED_MODULE_8__["default"])();
  Object(_components_maps__WEBPACK_IMPORTED_MODULE_9__["default"])();
  Object(_components_toggle_el__WEBPACK_IMPORTED_MODULE_1__["default"])();
  Object(_components_loadmore_blogs__WEBPACK_IMPORTED_MODULE_11__["default"])();
  Object(_components_input_select__WEBPACK_IMPORTED_MODULE_12__["default"])();
  Object(_components_preload_video__WEBPACK_IMPORTED_MODULE_15__["default"])();
  Object(_components_video_models__WEBPACK_IMPORTED_MODULE_10__["default"])();
  Object(_components_auth__WEBPACK_IMPORTED_MODULE_13__["default"])();
  Object(_components_up_btn__WEBPACK_IMPORTED_MODULE_14__["default"])();
  Object(_components_phone_mask__WEBPACK_IMPORTED_MODULE_16__["default"])();
  var video = new _components_video__WEBPACK_IMPORTED_MODULE_19__["default"]();
  var instagram = new _components_instagram__WEBPACK_IMPORTED_MODULE_18__["default"]();
  var filter = new _components_ajax_filter__WEBPACK_IMPORTED_MODULE_20__["default"]();
  var favoritesList = new _components_favorites__WEBPACK_IMPORTED_MODULE_17__["default"]();
  var comments = new _components_comments__WEBPACK_IMPORTED_MODULE_23__["default"]();
  var forms = new _components_form__WEBPACK_IMPORTED_MODULE_24__["default"]();
  var calendar = new _components_calendar__WEBPACK_IMPORTED_MODULE_22__["default"]();
  var lightBox = new _components_lightbox__WEBPACK_IMPORTED_MODULE_21__["default"]();
  var compcardLoader = new _components_compcard_loader__WEBPACK_IMPORTED_MODULE_25__["default"]();
  video.init();
  instagram.init();
  filter.init();
  favoritesList.init();
  comments.init();
  forms.init();
  calendar.init();
  lightBox.init();
  compcardLoader.init();
});

/***/ }),

/***/ "./src/scripts/components/ajax-filter.js":
/*!***********************************************!*\
  !*** ./src/scripts/components/ajax-filter.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AjaxFilter; });
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers */ "./src/scripts/components/helpers.js");
/* harmony import */ var _thumbnails_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./thumbnails-slider */ "./src/scripts/components/thumbnails-slider.js");
/* harmony import */ var nouislider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! nouislider */ "./node_modules/nouislider/distribute/nouislider.js");
/* harmony import */ var nouislider__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nouislider__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(qs__WEBPACK_IMPORTED_MODULE_4__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







var AjaxFilter = /*#__PURE__*/function () {
  function AjaxFilter() {
    var _this = this;

    _classCallCheck(this, AjaxFilter);

    _defineProperty(this, "setData", function (type, key, value) {
      if (type === 'add') {
        !_this.filterData[key] && (_this.filterData[key] = []);

        if (!_this.filterData[key].includes(value)) {
          if (Array.isArray(value)) {
            _this.filterData[key].splice(0, _this.filterData[key].length);

            _this.filterData[key].push(Math.floor(value[0]), Math.floor(value[1]));
          } else {
            _this.filterData[key].push(value);
          }
        }
      } else {
        _this.filterData[key] = _this.filterData[key].filter(function (item) {
          return item !== value;
        });
        if (_this.filterData[key].length === 0) delete _this.filterData[key];
      }

      _this.offset = 0;
    });

    _defineProperty(this, "slideBlock", function () {
      var _loop = function _loop(i) {
        var toggleInner = function toggleInner(el) {
          var inner = el.nextElementSibling;
          el.classList.contains('active') ? inner.style.maxHeight = inner.scrollHeight + 'px' : inner.style.maxHeight = null;
        };

        toggleInner(_this.slideButton[i]);

        _this.slideButton[i].addEventListener('click', function () {
          _this.slideButton[i].classList.toggle('active');

          toggleInner(_this.slideButton[i]);
        });
      };

      for (var i = 0; i < _this.slideButton.length; i++) {
        _loop(i);
      }
    });

    _defineProperty(this, "setCategory", function () {
      if (_this.category) {
        _this.setData('add', 'catalog', _this.category);
      }
    });

    _defineProperty(this, "locationSearch", function () {
      var self = _this;

      function handler() {
        var filter = this.value.toUpperCase();
        var li = self.locationList.getElementsByTagName('li');

        for (var i = 0; i < li.length; i++) {
          li[i].innerHTML.toUpperCase().indexOf(filter) > -1 ? li[i].style.display = '' : li[i].style.display = 'none';
        }
      }

      _this.locationInput.addEventListener('input', handler);
    });

    _defineProperty(this, "checkbox", function () {
      var debounceRequest = Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["debounce"])(function () {
        _this.request(true);

        _this.fetch = false;
      }, 1500);

      var _loop2 = function _loop2(i) {
        var input = _this.checkboxFields[i].querySelector('.checkbox__input');

        if (!input.closest('.checkbox_disabled')) {
          input.checked = false;
        }

        input.addEventListener('change', function () {
          var key = input.getAttribute('name');
          var value = +input.value;

          _this.loading();

          input.checked ? _this.setData('add', key, value) : _this.setData('remove', key, value);
          _this.fetch = true;
          debounceRequest();
        });
      };

      for (var i = 0; i < _this.checkboxFields.length; i++) {
        _loop2(i);
      }
    });

    _defineProperty(this, "rangeSlider", function () {
      var debounceRequest = Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["debounce"])(function () {
        _this.request(true);
      }, 500);

      var _loop3 = function _loop3(i) {
        var rangeLabels = _this.range[i].children[0],
            rangeInner = _this.range[i].children[1],
            dataMinAttr = +_this.range[i].getAttribute('data-min'),
            dataMaxAttr = +_this.range[i].getAttribute('data-max');
        nouislider__WEBPACK_IMPORTED_MODULE_2___default.a.create(rangeInner, {
          start: [dataMinAttr, dataMaxAttr],
          step: 1,
          connect: true,
          range: {
            'min': dataMinAttr,
            'max': dataMaxAttr
          }
        });
        rangeInner.noUiSlider.on('update', function (str, handle, value) {
          rangeLabels.children[handle].innerHTML = Math.floor(value[handle]);
        });
        rangeInner.noUiSlider.on('change.one', function (str, handle, value) {
          var key = _this.range[i].getAttribute('data-name');

          _this.loading();

          _this.setData('add', key, value);

          _this.rangeSliderChange = true;
          debounceRequest();
        });
      };

      for (var i = 0; _this.range.length > i; i++) {
        _loop3(i);
      }
    });

    _defineProperty(this, "reset", function () {
      _this.resetButton.addEventListener('click', function () {
        _this.filter.reset();

        _this.filterData = {};
        _this.offset = 0;

        _this.range.forEach(function (el) {
          return el.children[1].noUiSlider.reset();
        });

        _this.resetButton.classList.remove('active');

        _this.rangeSliderChange = false;

        _this.setCategory();

        _this.loading();

        _this.request(true);
      });
    });

    _defineProperty(this, "load", function () {
      var scrollArea = document.querySelector('html');
      var debounceRequest = Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["debounce"])(function () {
        _this.preloaderScroll();

        _this.request();
      }, 100);
      window.addEventListener('scroll', function () {
        var offset = document.querySelector('.footer').clientHeight * 2;
        var loading = scrollArea.scrollTop + scrollArea.clientHeight + offset >= scrollArea.scrollHeight && !_this.fetch;

        if (loading) {
          _this.offset = _this.ajaxContainer.children.length;
          debounceRequest();
        }
      });
    });

    _defineProperty(this, "loading", function () {
      var container = _this.preloader.content.cloneNode(true);

      _this.ajaxContainer.innerHTML = '';

      _this.ajaxContainer.append(container);
    });

    _defineProperty(this, "preloaderScroll", function () {
      var preload = document.createElement('div');
      var span = document.createElement('span');
      preload.className = 'catalog__preloader';

      for (var i = 0; i < 3; i++) {
        preload.append(span.cloneNode());
      }

      _this.ajaxContainer.parentElement.append(preload);

      setTimeout(function () {
        return preload.style.opacity = '1';
      }, 100);
    });

    _defineProperty(this, "print", function (data, redrawing) {
      if (redrawing) {
        _this.count.innerHTML = data.total;

        _this.ajaxContainer.querySelector('.loading').remove();
      } else {
        _this.ajaxContainer.parentElement.querySelector('.catalog__preloader').remove();
      }

      data.posts.forEach(function (el, i) {
        var gridEl = document.createElement('div');
        gridEl.className = 'col-lg-4 col-md-4 col-sm-4 col-xs-6 grid-item';
        gridEl.innerHTML = renderAjaxCardProfile(el);

        _this.ajaxContainer.append(gridEl);

        var card = gridEl.querySelector('.card-profile__photo');
        var slider = new _thumbnails_slider__WEBPACK_IMPORTED_MODULE_1__["default"](card);
        slider.init();
        setTimeout(function () {
          return gridEl.classList.add('load');
        }, 100 * (i + 1));
      });
      var visibleCount = _this.ajaxContainer.children.length;
      visibleCount === data.total ? _this.fetch = true : _this.fetch = false;
    });

    _defineProperty(this, "request", function (redrawing) {
      var data = {
        action: 'get_models',
        offset: _this.offset,
        filter: _objectSpread({}, _this.filterData)
      };
      axios__WEBPACK_IMPORTED_MODULE_3___default.a.post(path.ajax, qs__WEBPACK_IMPORTED_MODULE_4___default.a.stringify(data)).then(function (response) {
        return _this.print(response.data, redrawing);
      }).catch(function (error) {
        return console.log(error);
      });
      _this.fetch = true;
      var checkboxActive = Array.from(_this.filter.querySelectorAll('input[type="checkbox"]:checked'));
      var activeLength = checkboxActive.filter(function (e) {
        return +e.value !== +_this.category;
      }).length;
      activeLength !== 0 || _this.rangeSliderChange ? _this.resetButton.classList.add('active') : _this.resetButton.classList.remove('active');
    });

    this.filter = document.querySelector('.filter');
    this.catalogContainer = document.querySelector('.catalog');

    if (this.filter) {
      this.slideButton = this.filter.querySelectorAll('.filter__title');
      this.locationInput = this.filter.querySelector('.search-input');
      this.locationList = this.filter.querySelector('.search-list');
      this.checkboxFields = this.filter.querySelectorAll('.checkbox');
      this.range = this.filter.querySelectorAll('.rangeSlider');
      this.resetButton = document.querySelector('.reset-filter');
      this.preloader = document.querySelector('#loading');
      this.ajaxContainer = document.querySelector('.ajax-container');
      this.count = document.querySelector('.find-total span');
      this.fetch = false;
      this.rangeSliderChange = false;
      this.offset = 0;
      this.category = +loadMore.models;
      this.filterData = {};
    }
  }

  _createClass(AjaxFilter, [{
    key: "init",
    value: function init() {
      if (this.filter) {
        this.setCategory();
        this.slideBlock();
        this.locationSearch();
        this.checkbox();
        this.rangeSlider();
        this.reset();
        this.load();
      }
    }
  }]);

  return AjaxFilter;
}();



/***/ }),

/***/ "./src/scripts/components/auth.js":
/*!****************************************!*\
  !*** ./src/scripts/components/auth.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fb-sdk-wrapper */ "./node_modules/fb-sdk-wrapper/lib/index.js");
/* harmony import */ var fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0__);

var config = {
  appId: '1374665999549175'
};

var facebookOAuth = function facebookOAuth() {
  var button = document.querySelector('.facebook-login');
  var name = document.querySelector('input[name="abb-name"]');
  var email = document.querySelector('input[name="email"]');
  var photo = document.querySelector('input[name="photo"]');

  var auth = function auth(callback) {
    fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0__["load"]().then(function () {
      fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0__["init"]({
        appId: config.appId,
        cookie: true,
        xfbml: false,
        version: 'v9.0'
      });
      fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0__["login"]({
        scope: 'email',
        return_scopes: false
      }).then(function (response) {
        if (response.status === 'connected') {
          fb_sdk_wrapper__WEBPACK_IMPORTED_MODULE_0__["api"]('/me', 'get', {
            fields: 'id, email, first_name, last_name, picture.type(large)'
          }).then(function (response) {
            return callback(response);
          });
        }
      });
    });
  };

  if (button) {
    var foto = document.createElement('img');
    foto.className = 'facebook-login__photo';
    button.addEventListener('click', function () {
      auth(function (response) {
        name.value = "".concat(response.first_name, " ").concat(response.last_name);
        email.value = response.email;
        photo.value = response['picture']['data']['url'];
        foto.src = response['picture']['data']['url'];
        button.children[0].remove();
        button.append(foto);
      });
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (facebookOAuth);

/***/ }),

/***/ "./src/scripts/components/become-model-form.js":
/*!*****************************************************!*\
  !*** ./src/scripts/components/become-model-form.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var FormValidation = /*#__PURE__*/function () {
  function FormValidation() {
    _classCallCheck(this, FormValidation);

    this.DOM = {
      name: document.querySelector('[name="abb-name"]'),
      dateOfBirth: document.querySelector('[name="date-of-birth"]'),
      phone: document.querySelector('[name="number"]')
    };
    this.errors = {
      required: modelForm.formErrors['fieldRequired'],
      email: modelForm.formErrors['wrongEmail']
    };
  }

  _createClass(FormValidation, [{
    key: "validateStep",
    value: function validateStep(stepNumber) {
      var _this = this;

      var firstStepWrapper = document.querySelector("[data-form-step='".concat(stepNumber, "']"));

      var elements = _toConsumableArray(firstStepWrapper.querySelectorAll('[data-validation]'));

      var validationResult = elements.map(function (el) {
        var r = _this.validate(el);

        r = r.filter(function (res) {
          return res !== true;
        });
        return r.length;
      }).filter(function (res) {
        return res > 0;
      });
      return validationResult.length ? false : true;
    }
  }, {
    key: "validate",
    value: function validate(el) {
      var _this2 = this;

      var validationTypes = el.dataset.validation.split(' ');
      return validationTypes.map(function (type) {
        var res;

        if (type === 'required') {
          res = el.type === 'checkbox' ? _this2.validateRequiredCheckBox(el) : _this2.validateRequiredTextInput(el);
        }

        if (type === 'email') {
          res = _this2.validateEmail(el);
        }

        return res;
      });
    }
  }, {
    key: "validateRequiredTextInput",
    value: function validateRequiredTextInput(el) {
      var isValid = true;
      var parentField = el.closest('.form__field');

      if (el.value === '') {
        this.createError(parentField, 'required');
        isValid = false;
      } else {
        this.clearError(parentField);
      }

      return isValid;
    }
  }, {
    key: "validateRequiredCheckBox",
    value: function validateRequiredCheckBox(el) {
      var isValid = true;

      if (!el.checked) {
        el.closest('.checkbox').classList.add('is-error');
        isValid = false;
      } else {
        el.closest('.checkbox ').classList.remove('is-error');
      }

      return isValid;
    }
  }, {
    key: "validateEmail",
    value: function validateEmail(el) {
      var isValid = true;
      var val = el.value;
      if (!val) return;
      var emailRegEx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,10}$/g;
      var validEmail = val.match(emailRegEx);
      var parentField = el.closest('.form__field');

      if (!validEmail) {
        this.createError(parentField, 'email');
        isValid = false;
      } else {
        this.clearError(parentField, 'email');
      }

      return isValid;
    }
  }, {
    key: "createError",
    value: function createError(parentElement, errorType) {
      var errorElement = parentElement.querySelector('.error');

      if (errorElement) {
        if (errorElement.dataset.errorType === errorType) {
          return;
        } else {
          error.innerHTML = this.errors[errorType];
        }
      } else {
        var _error = document.createElement('span');

        _error.className = 'error';
        _error.innerHTML = this.errors[errorType];

        _error.setAttribute('data-error-type', errorType);

        parentElement.appendChild(_error);
      }
    }
  }, {
    key: "clearError",
    value: function clearError(parentElement) {
      var error = parentElement.querySelector('.error');

      if (error) {
        error.remove();
      }
    }
  }]);

  return FormValidation;
}();

var FileUploader = /*#__PURE__*/function () {
  function FileUploader() {
    _classCallCheck(this, FileUploader);

    this.images = [];
    this.modelForm = document.querySelector('.js-become-model-form');
    this.dropZone = document.querySelector('.drop-zone');
    this.fileInput = document.querySelector('.file-uploader__input');
    this.preview = document.querySelector('.files-preview');
    this.createUploader();
  }

  _createClass(FileUploader, [{
    key: "toggleModelForm",
    value: function toggleModelForm(action) {
      this.modelForm.classList[action]('is-dragover');
    }
  }, {
    key: "preventEvents",
    value: function preventEvents(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, {
    key: "isAdvancedUpload",
    value: function isAdvancedUpload() {
      var div = document.createElement('div');
      return ('draggable' in div || 'ondragstart' in div && 'ondrop' in div) && 'FormData' in window && 'FileReader' in window;
    }
  }, {
    key: "createUploader",
    value: function createUploader() {
      var _this3 = this;

      if (this.isAdvancedUpload()) {
        this.modelForm.classList.add('has-advanced-upload');
        this.dropZone.addEventListener('drag', this.preventEvents);
        this.dropZone.addEventListener('dragstart', this.preventEvents);
        this.dropZone.addEventListener('dragend', this.preventEvents);
        this.dropZone.addEventListener('dragover', this.preventEvents);
        this.dropZone.addEventListener('dragenter', this.preventEvents);
        this.dropZone.addEventListener('dragleave', this.preventEvents);
        this.dropZone.addEventListener('drop', this.preventEvents);
        this.dropZone.addEventListener('dragover', function () {
          return _this3.toggleModelForm('add');
        });
        this.dropZone.addEventListener('dragenter', function () {
          return _this3.toggleModelForm('add');
        });
        this.dropZone.addEventListener('dragleave', function () {
          return _this3.toggleModelForm('remove');
        });
        this.dropZone.addEventListener('dragend', function () {
          return _this3.toggleModelForm('remove');
        });
        this.dropZone.addEventListener('drop', function () {
          return _this3.toggleModelForm('remove');
        });
        this.dropZone.addEventListener('drop', function (e) {
          _this3.modelForm.classList.add('files-added');

          _this3.handleFiles(e.dataTransfer.files);
        });
        this.fileInput.addEventListener('change', function (e) {
          _this3.modelForm.classList.add('files-added');

          _this3.handleFiles(e.srcElement.files);
        });
      }
    }
  }, {
    key: "handleFiles",
    value: function handleFiles(files) {
      var _this4 = this;

      var _loop = function _loop() {
        file = files[i];

        if (!file.type.startsWith('image/')) {
          return "continue";
        }

        var uuid = "f".concat((+new Date()).toString(16));
        var itemWrapper = document.createElement('div');
        itemWrapper.classList.add('files-preview__item');
        var imageWrapper = document.createElement('div');
        imageWrapper.classList.add('files-preview__image-wrapper');
        var imageDeleteBtn = document.createElement('button');
        imageDeleteBtn.type = 'button';
        imageDeleteBtn.classList.add('files-preview__delete-btn');
        var imageDeleteSpan = document.createElement('span');
        imageDeleteSpan.classList.add('files-preview__delete-inner');
        var img = document.createElement('img');
        img.classList.add('files-preview__img');
        imageDeleteBtn.appendChild(imageDeleteSpan);
        imageWrapper.appendChild(img);
        itemWrapper.appendChild(imageDeleteBtn);
        itemWrapper.appendChild(imageWrapper);
        img.file = file;

        _this4.preview.appendChild(itemWrapper);

        reader = new FileReader();

        reader.onload = function (aImg) {
          return function (e) {
            aImg.src = e.target.result;
          };
        }(img);

        reader.readAsDataURL(file);

        _this4.images.push({
          filename: "file-".concat(uuid),
          file: file
        });

        imageDeleteBtn.addEventListener('click', function (e) {
          _this4.images = _this4.images.filter(function (el) {
            return el.filename !== "file-".concat(uuid);
          });
          itemWrapper.remove();
        });
      };

      for (var i = 0; i < files.length; i++) {
        var file;
        var reader;

        var _ret = _loop();

        if (_ret === "continue") continue;
      }
    }
  }, {
    key: "createImagesData",
    value: function createImagesData() {
      return this.images;
    }
  }]);

  return FileUploader;
}(); // Steps


var formStepsHandler = function formStepsHandler() {
  var form = new FormValidation();
  var fileUploader = new FileUploader();
  var btn = document.querySelector('[data-form-step-trigger]');
  var step1 = document.querySelector('[data-form-step=\'1\']');
  var step2 = document.querySelector('[data-form-step=\'2\']');
  var step3 = document.querySelector('[data-form-step=\'3\']');
  var mark1 = document.querySelector('[data-form-step-mark=\'1\']');
  var mark2 = document.querySelector('[data-form-step-mark=\'2\']');
  var mark3 = document.querySelector('[data-form-step-mark=\'3\']');
  var markBtn1 = document.querySelector('[data-form-step-mark-btn=\'1\']');
  var markBtn2 = document.querySelector('[data-form-step-mark-btn=\'2\']');
  var markBtn3 = document.querySelector('[data-form-step-mark-btn=\'3\']');

  var activeStep1 = function activeStep1() {
    step1.classList.add('active');
    mark1.classList.add('active');
    step2.classList.remove('active');
    step3.classList.remove('active');
    mark2.classList.remove('active');
    mark3.classList.remove('active');
  };

  var activeStep2 = function activeStep2() {
    step2.classList.add('active');
    mark2.classList.add('active');
    step1.classList.remove('active');
    step3.classList.remove('active');
    mark3.classList.remove('active');
  };

  var activeStep3 = function activeStep3() {
    step3.classList.add('active');
    mark3.classList.add('active');
    step2.classList.remove('active');
    step1.classList.remove('active');
  };

  var preloader = function () {
    var form = document.querySelector('.js-become-model-form');
    var load = document.createElement('div');
    var icon = document.createElement('div');
    load.className = 'form__preloader';
    icon.className = 'form__preloader-icon';
    load.append(icon);

    function add() {
      form.append(load);
      setTimeout(function () {
        return load.classList.add('show');
      }, 100);
    }

    function remove() {
      load.classList.remove('show');
      setTimeout(function () {
        return load.remove();
      }, 300);
    }

    return {
      add: add,
      remove: remove
    };
  }();

  var submitForm = function submitForm() {
    var form = document.querySelector('.js-become-model-form');
    var data = new FormData(form);
    var images = fileUploader.createImagesData();
    images.forEach(function (element) {
      data.append('photos[]', element.file);
    });
    preloader.add();
    request(data, function (response) {
      preloader.remove();
      response ? responseModelFormMessage(modelForm.modelFormSuccessText, '') : responseModelFormMessage(modelForm.modelFormErrorText, 'is-error');
    });
  };

  function responseModelFormMessage(msg, cls) {
    var successModal = document.createElement('div');
    successModal.className = "model-form-response-message ".concat(cls);
    successModal.innerHTML = "\n      <div class=\"model-form-response-message__text\">\n        ".concat(msg, "\n      <div>\n    ");
    var form = document.querySelector('.js-become-model-form');
    var btnText = btn.querySelector('.button__name');
    form.appendChild(successModal);
    activeStep1();
    form.reset();
    btnText.textContent = modelForm.modelFormNextStepBtnText;
    btn.setAttribute('data-form-step-trigger', '2');
    setTimeout(function () {
      successModal.classList.add('hide');
      setTimeout(function () {
        return successModal.remove();
      }, 300);
    }, 3000);
  }

  function stepsHandler(e) {
    e.preventDefault();
    var target = e.currentTarget;
    var targetType = target.classList.contains('form-navigation__num') ? 'nav' : 'btn';
    var nextStep = target.getAttribute(targetType === 'btn' ? 'data-form-step-trigger' : 'data-form-step-mark-btn');
    var btnText = btn.querySelector('.button__name');

    switch (nextStep) {
      case '1':
        activeStep1();
        btn.setAttribute('data-form-step-trigger', '2');
        btnText.textContent = modelForm.modelFormNextStepBtnText;
        break;

      case '2':
        var validateFirstStep = form.validateStep(1);
        if (!validateFirstStep) return false;
        activeStep2();
        btn.setAttribute('data-form-step-trigger', '3');
        btnText.textContent = modelForm.modelFormNextStepBtnText;
        break;

      case '3':
        var validateStep1 = form.validateStep(1);
        var validateStep2 = form.validateStep(2);
        if (!validateStep1 || !validateStep2) return false;
        activeStep3();
        btn.type = 'submit';
        btn.setAttribute('data-form-step-trigger', 'submit');
        btnText.textContent = modelForm.modelFormSubmitBtnText;
        break;

      case 'submit':
        submitForm();
        break;
    }
  }

  document.querySelector('.js-become-model-form input[name=\'number\']').addEventListener('input', function () {
    var parent = this.closest('.form__field');
    var code = parent.querySelector('.iti__selected-dial-code').innerHTML;
    parent.querySelector('input[name="full-number"]').value = code + this.value;
  });
  btn.addEventListener('click', stepsHandler);
  markBtn1.addEventListener('click', stepsHandler);
  markBtn2.addEventListener('click', stepsHandler);
  markBtn3.addEventListener('click', stepsHandler);
};

var formHelperMethods = function formHelperMethods() {
  var affiliatesRow = document.querySelector('.js-affiliates-row');
  var affiliatesCheckbox = document.querySelector('.js-affiliates-checkbox');
  affiliatesCheckbox.addEventListener('change', function (e) {
    affiliatesRow.classList[affiliatesCheckbox.checked ? 'add' : 'remove']('is-show');
  });
};

var becomeModelForm = function becomeModelForm() {
  if (document.querySelector('.js-become-model-form') !== null) {
    formStepsHandler();
    formHelperMethods();
  }
};

var request = function request(data, callback) {
  data.append('action', 'send_form');
  axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(path.ajax, data).then(function (response) {
    return callback(response.data);
  }).catch(function (error) {
    return console.log(error);
  });
};

/* harmony default export */ __webpack_exports__["default"] = (becomeModelForm);

/***/ }),

/***/ "./src/scripts/components/calendar.js":
/*!********************************************!*\
  !*** ./src/scripts/components/calendar.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Calendar; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var Calendar = /*#__PURE__*/function () {
  function Calendar() {
    var _this = this;

    _classCallCheck(this, Calendar);

    _defineProperty(this, "getEvents", function () {
      return new Promise(function (resolve, reject) {
        var dateStart = new Date(_this.date.year, _this.date.month, _this.date.day, +_this.options.general.schedule.start, 0);
        var dateEnd = new Date(_this.date.year, _this.date.month, _this.date.day + +_this.options.general['day-count'], +_this.options.general.schedule.end, 0);
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('https://www.googleapis.com/calendar/v3/calendars/' + _this.options.service.calendar_id + '/events', {
          params: {
            timeMin: dateStart,
            timeMax: dateEnd,
            orderBy: 'startTime',
            singleEvents: true,
            key: _this.options.service.api_key
          }
        }).then(function (response) {
          response.data.items.forEach(function (item) {
            var _this$events$fullDate;

            var start = new Date(item.start.dateTime);
            var end = new Date(item.end.dateTime);
            var fullDateStart = start.toLocaleString().slice(0, 10);
            var time = [];

            for (var i = start.getHours(); i < end.getHours(); i++) {
              time.push(i);
            }

            !_this.events[fullDateStart] ? _this.events[fullDateStart] = time : (_this$events$fullDate = _this.events[fullDateStart]).push.apply(_this$events$fullDate, time);
          });
          resolve();
        }).catch(function (error) {
          return reject(error);
        });
      });
    });

    _defineProperty(this, "set", function () {
      var week = Math.floor(+_this.options.general['day-count'] / 7);
      var count = 0;

      for (var i = 0; i < week; i++) {
        var dateStart = new Date(_this.date.year, _this.date.month, _this.date.day + 7 * i, 0, 0);
        var dateEnd = new Date(_this.date.year, _this.date.month, _this.date.day + 7 * i + 6, 0, 0);
        var current = "".concat(dateStart.getDate(), " ").concat(_this.options.labels.months[dateStart.getMonth()], " - ").concat(dateEnd.getDate(), " ").concat(_this.options.labels.months[dateEnd.getMonth()]);
        _this.data[i] = {
          top: {
            current: current,
            labels: []
          },
          days: []
        };

        for (var j = 0; j < 7; j++) {
          var date = new Date(_this.date.year, _this.date.month, _this.date.day + count++, 0, 0);
          var fullDate = date.toLocaleString().slice(0, 10);
          var day = date.getDay();
          var dayLabel = "".concat(_this.options.labels.days[day], ", ").concat(date.getDate());
          var datetime = {};

          for (var time = +_this.options.general.schedule.start; time <= +_this.options.general.schedule.end; time++) {
            var key = fullDate + 'T' + time;

            var ev = _this.events[fullDate] && _this.events[fullDate].includes(time);

            var disabled = ev ? ev : false;
            datetime[key] = {
              selected: false,
              disabled: disabled,
              price: {
                value: _this.options.prices[day].value,
                marker: _this.options.prices[day].marker
              }
            };
          }

          _this.data[i].top.labels.push(dayLabel);

          _this.data[i].days.push(datetime);
        }
      }
    });

    _defineProperty(this, "print", function () {
      var data = _this.data[0];
      var hoursLabel = document.createElement('span');
      var dayLabel = document.createElement('div');
      dayLabel.className = 'calendar__day-label';
      var dayEl = document.createElement('div');
      dayEl.className = 'calendar__day';
      var hourEl = document.createElement('div');
      hourEl.className = 'calendar__hour';
      _this.elements.current.innerHTML = data.top.current;

      for (var time = +_this.options.general.schedule.start; time <= +_this.options.general.schedule.end; time++) {
        var hoursLabelEl = hoursLabel.cloneNode();
        hoursLabelEl.innerHTML = "".concat(String(time).padStart(2, 0), ":00 - ").concat(String(time + 1).padStart(2, 0), ":00");

        _this.elements.labelsHours.append(hoursLabelEl);
      }

      data.top.labels.forEach(function (item, i) {
        var dayLabelEl = dayLabel.cloneNode();
        var inner = document.createElement('span');
        if (i === 0) dayLabelEl.classList.add('calendar__day-label_active');
        inner.innerHTML = item;
        dayLabelEl.append(inner);

        _this.elements.labelsDays.append(dayLabelEl);
      });
      data.days.forEach(function (item) {
        var onlyDayEl = dayEl.cloneNode();

        for (var day in item) {
          var hours = item[day];
          var onlyHourEl = hourEl.cloneNode();
          hours.disabled ? onlyHourEl.classList.add('disabled') : '';
          onlyHourEl.setAttribute('data-datetime', day);
          onlyHourEl.setAttribute('data-price', hours.price.value);
          onlyHourEl.classList.add(hours.price.marker);
          onlyDayEl.appendChild(onlyHourEl);
        }

        _this.elements.days.append(onlyDayEl);
      });
    });

    _defineProperty(this, "update", function (index) {
      var data = _this.data[index];
      _this.elements.current.innerHTML = data.top.current;
      index === 0 ? _this.elements.labelsDays.children[0].classList.add('calendar__day-label_active') : _this.elements.labelsDays.children[0].classList.remove('calendar__day-label_active');
      data.top.labels.forEach(function (label, i) {
        _this.elements.labelsDays.children[i].children[0].innerHTML = label;
      });
      data.days.forEach(function (item, i) {
        var a = 0;

        for (var dataTime in item) {
          var dayEl = _this.elements.days.children[i].children[a];
          dayEl.setAttribute('data-datetime', dataTime);
          dayEl.setAttribute('data-price', item[dataTime].price.value);
          item[dataTime].selected ? dayEl.classList.add('active') : dayEl.classList.remove('active');
          item[dataTime].disabled ? dayEl.classList.add('disabled') : dayEl.classList.remove('disabled');
          a++;
        }
      });
    });

    _defineProperty(this, "calculation", function (element) {
      var el = document.querySelector('.total-price__value');
      var price = +element.getAttribute('data-price');
      element.classList.contains('active') ? _this.formData['total_price'] = _this.formData['total_price'] + price : _this.formData['total_price'] = _this.formData['total_price'] - price;
      document.querySelector('input[name=\'price\']').value = _this.formData['total_price'];
      el.innerHTML = _this.formData['total_price'] + ' ₴';
    });

    _defineProperty(this, "navigation", function () {
      var prev = document.querySelector('.date-nav__prev');
      var next = document.querySelector('.date-nav__next');
      var weekCount = _this.data.length - 1;
      prev.addEventListener('click', function () {
        next.classList.remove('disabled');

        if (_this.week !== 0) {
          _this.update(--_this.week);

          if (_this.week === 0) {
            prev.classList.add('disabled');
          }
        }
      });
      next.addEventListener('click', function () {
        prev.classList.remove('disabled');

        if (_this.week !== weekCount) {
          _this.update(++_this.week);

          if (_this.week === weekCount) {
            next.classList.add('disabled');
          }
        }
      });
    });

    _defineProperty(this, "setFrom", function (t) {
      var dateTime = t.split('T');
    });

    _defineProperty(this, "select", function () {
      document.addEventListener('click', function (e) {
        var el = e.target;

        if (el.closest('.calendar__hour')) {
          var weekData = _this.data[_this.week].days;
          var dateTime = el.getAttribute('data-datetime');
          el.classList.toggle('active');

          for (var key in weekData) {
            if (weekData[key][dateTime] !== undefined) {
              weekData[key][dateTime]['selected'] = el.classList.contains('active');
            }
          } // this.setFrom();


          _this.calculation(el);
        }
      });
    });

    this.options = CalendarOptions;
    this.date = {
      dayWeek: new Date().getDay(),
      day: new Date().getDate(),
      month: new Date().getMonth(),
      year: new Date().getFullYear()
    };
    this.data = [];
    this.events = [];
    this.week = 0;
    this.priceMarker = ['marker1', 'marker2', 'marker3', 'marker4', 'marker5', 'marker6', 'marker7'];
    this.elements = {
      container: document.querySelector('.calendar'),
      labelsDays: document.querySelector('.calendar__days-label'),
      labelsHours: document.querySelector('.calendar__hours-label'),
      current: document.querySelector('.date-nav__current'),
      days: document.querySelector('.calendar__week-days')
    };
    this.formData = {
      total_price: 0,
      date: {}
    };
  }

  _createClass(Calendar, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      if (this.elements.container) {
        this.getEvents().then(function () {
          _this2.set();

          _this2.print();

          _this2.navigation();

          _this2.select();
        });
      }
    }
  }]);

  return Calendar;
}();



/***/ }),

/***/ "./src/scripts/components/colors-toggle.js":
/*!*************************************************!*\
  !*** ./src/scripts/components/colors-toggle.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);


var colorToggle = function colorToggle() {
  var rootContainer = document.querySelector('html');
  var buttonContainer = document.querySelectorAll('.scheme-buttons');

  if (buttonContainer !== null) {
    var _loop = function _loop(i) {
      var buttons = buttonContainer[i].children;

      for (var _i = 0; buttons.length > _i; _i++) {
        buttons[_i].addEventListener('click', function () {
          var scheme = this.getAttribute('data-scheme');

          for (var j = 0; buttons.length > j; j++) {
            var a = buttons[j].getAttribute('data-scheme');
            rootContainer.classList.remove(a);
          }

          js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set('scheme', scheme, {
            expires: 365
          });
          rootContainer.classList.add(scheme);
        });
      }
    };

    for (var i = 0; buttonContainer.length > i; i++) {
      _loop(i);
    }
  }
};

/* harmony default export */ __webpack_exports__["default"] = (colorToggle);

/***/ }),

/***/ "./src/scripts/components/comments.js":
/*!********************************************!*\
  !*** ./src/scripts/components/comments.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CommentsPost; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(qs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./validation */ "./src/scripts/components/validation.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var CommentsPost = /*#__PURE__*/function () {
  function CommentsPost() {
    var _this = this;

    _classCallCheck(this, CommentsPost);

    _defineProperty(this, "setFormData", function () {
      _this.fields.forEach(function (field) {
        _this.formData[field.name] = field.value;
      });
    });

    _defineProperty(this, "like", function () {
      document.addEventListener('click', function (e) {
        var clickEl = e.target;
        var element = clickEl.closest('.like');
        var commentID = clickEl.closest('.comment') ? clickEl.closest('.comment').getAttribute('data-comment-id') : false;

        if (element) {
          var counter = element.querySelector('.like__count');
          var data = {
            action: 'set_like',
            comment_id: commentID
          };

          _this.request(data, function (response) {
            counter.innerHTML = response;
            element.classList.toggle('active');
          });
        }
      });
    });

    _defineProperty(this, "reply", function () {
      document.addEventListener('click', function (e) {
        var target = e.target;
        var button = target.closest('.comment__reply');
        var formPosition = _this.commentsForm.getBoundingClientRect().top + window.pageYOffset - 300;

        var commentField = _this.commentsForm.querySelector('.comments-form__field textarea');

        var replyTo = document.querySelector('.reply-to');
        var createReplyTo = document.createElement('div');
        var createReplyClose = document.createElement('span');
        createReplyTo.className = 'reply-to';
        createReplyClose.className = 'reply-to__close';
        createReplyClose.innerText = 'Отменить';

        if (button) {
          var commentEl = button.closest('.comment');
          var id = commentEl.getAttribute('data-comment-id');
          var name = commentEl.querySelector('.comment__name').innerHTML;
          var replyName = '@' + name + ',';
          _this.formData['reply'] = id;
          commentField.value = replyName;
          createReplyTo.innerText = 'Ответ пользователю ' + name;
          createReplyTo.append(createReplyClose);
          if (replyTo !== null) replyTo.remove();
          commentField.after(createReplyTo);
          window.scrollTo({
            top: formPosition,
            behavior: 'smooth'
          });
        }

        if (target.closest('.reply-to__close')) {
          commentField.value = '';
          replyTo.remove();
          delete _this.formData['reply'];
        }
      });
    });

    _defineProperty(this, "setRating", function () {
      document.querySelectorAll('.rating').forEach(function (field) {
        var value = +field.getAttribute('data-value');
        var items = field.querySelectorAll('.rating__item');
        var input = field.querySelector('.rating__input');
        items.forEach(function (item, index) {
          if (index + 1 <= value) item.classList.add('active');

          if (input) {
            item.addEventListener('click', function () {
              input.value = index + 1;
              items.forEach(function (el, i) {
                return i <= index ? el.classList.add('active') : el.classList.remove('active');
              });
            });
          }
        });
      });
    });

    _defineProperty(this, "sendComment", function () {
      _this.commentsForm.addEventListener('submit', function (e) {
        e.preventDefault();

        _this.setFormData();

        var messageCreate = document.createElement('span');
        messageCreate.className = 'comments-form__message';

        var data = _objectSpread({
          action: 'send_comment'
        }, _this.formData);

        var valid = new _validation__WEBPACK_IMPORTED_MODULE_2__["default"](e.target);

        if (valid.complete()) {
          _this.commentsButton.disabled = true;

          _this.request(data, function (response) {
            messageCreate.innerHTML = response.data;

            _this.commentsForm.append(messageCreate);

            _this.commentsButton.disabled = false;

            _this.commentsForm.reset();

            _this.formData = {};

            if (document.querySelector('.reply-to')) {
              document.querySelector('.reply-to').remove();
            }

            setTimeout(function () {
              document.querySelector('.comments-form__message').remove();
            }, 3000);
          });
        }
      });
    });

    _defineProperty(this, "outputComments", function (comments, appendTo) {
      var itemClass = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'comments__item';
      comments.forEach(function (item, i) {
        var itemEl = document.createElement('div');
        itemEl.className = itemClass;
        itemEl.innerHTML = renderCommentTemplate(item);

        if (item.children !== undefined) {
          _this.outputComments(item.children, itemEl, 'comments__sub-item');
        }

        appendTo.append(itemEl);
        setTimeout(function () {
          return itemEl.classList.add('load');
        }, 100 * (i + 1));
      });
    });

    _defineProperty(this, "loadComments", function () {
      if (_this.loadButton !== null) {
        _this.loadButton.addEventListener('click', function () {
          var offset = _this.commentsList.children.length;
          var data = {
            action: 'get_comments',
            offset: offset
          };

          _this.loadButton.classList.add('disabled');

          _this.request(data, function (response) {
            _this.outputComments(response.comments, _this.commentsList);

            var offset = _this.commentsList.children.length;

            _this.setRating();

            +response.count === offset ? _this.loadButton.remove() : _this.loadButton.classList.remove('disabled');
          });
        });
      }
    });

    _defineProperty(this, "request", function (data, callback) {
      data.post_id = comments.post_id;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(path.ajax, qs__WEBPACK_IMPORTED_MODULE_1___default.a.stringify(data)).then(function (response) {
        return callback(response.data);
      }).catch(function (error) {
        return console.log(error);
      });
    });

    this.commentsForm = document.querySelector('.comments-form');

    if (this.commentsForm) {
      this.fields = this.commentsForm.querySelectorAll('input, textarea');
      this.commentsButton = this.commentsForm.querySelector('button');
      this.commentsList = document.querySelector('.comments-list');
      this.loadButton = document.querySelector('.loadmore_comments');
    }

    this.formData = {};
    this.offset = 0;
  }

  _createClass(CommentsPost, [{
    key: "init",
    value: function init() {
      if (this.commentsForm) {
        this.reply();
        this.sendComment();
        this.like();
        this.setRating();
        this.loadComments();
      }
    }
  }]);

  return CommentsPost;
}();



/***/ }),

/***/ "./src/scripts/components/common.js":
/*!******************************************!*\
  !*** ./src/scripts/components/common.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");
/* harmony import */ var _thumbnails_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./thumbnails-slider */ "./src/scripts/components/thumbnails-slider.js");


swiper__WEBPACK_IMPORTED_MODULE_0__["default"].use([swiper__WEBPACK_IMPORTED_MODULE_0__["Navigation"]]);

var common = function common() {
  new swiper__WEBPACK_IMPORTED_MODULE_0__["default"]('.slider__inner', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.slider__btn-next',
      prevEl: '.slider__btn-prev'
    }
  });

  (function () {
    var cards = document.querySelectorAll('.card-profile');
    cards.forEach(function (card) {
      var container = card.querySelector('.card-profile__photo');
      var slider = new _thumbnails_slider__WEBPACK_IMPORTED_MODULE_1__["default"](container);
      slider.init();
    });
  })();
};

/* harmony default export */ __webpack_exports__["default"] = (common);

/***/ }),

/***/ "./src/scripts/components/compcard-loader.js":
/*!***************************************************!*\
  !*** ./src/scripts/components/compcard-loader.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GeneratePdf; });
/* harmony import */ var html2pdf_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! html2pdf.js */ "./node_modules/html2pdf.js/dist/html2pdf.js");
/* harmony import */ var html2pdf_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(html2pdf_js__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var GeneratePdf = /*#__PURE__*/function () {
  function GeneratePdf() {
    var cardId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'compcard';
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, GeneratePdf);

    _defineProperty(this, "options", {
      margin: 0,
      image: {
        type: 'png',
        quality: 1
      },
      html2canvas: {
        dpi: 300,
        useCORS: true,
        letterRendering: false,
        removeContainer: false
      },
      jsPDF: {
        unit: 'mm',
        orientation: 'landscape'
      }
    });

    this.compcard = document.getElementById(cardId);
    this.options = Object.assign(this.options, options);
  }

  _createClass(GeneratePdf, [{
    key: "getModelName",
    value: function getModelName() {
      var name = this.compcard.querySelector('.card__name').innerHTML;
      return name.replace(' ', '-').toLowerCase();
    }
  }, {
    key: "createFileName",
    value: function createFileName() {
      return "egomodels-".concat(this.getModelName(), ".pdf");
    }
  }, {
    key: "createPDF",
    value: function createPDF() {
      html2pdf_js__WEBPACK_IMPORTED_MODULE_0___default()().set(this.options).from(this.compcard).save(this.createFileName());
    }
  }, {
    key: "addHandler",
    value: function addHandler() {
      var _this = this;

      if (this.compcard) {
        var btn = this.compcard.querySelector('.card__download');
        btn.addEventListener('click', function () {
          return _this.createPDF();
        });
      }
    }
  }, {
    key: "init",
    value: function init() {
      this.addHandler();
    }
  }]);

  return GeneratePdf;
}();



/***/ }),

/***/ "./src/scripts/components/cookie-popup.js":
/*!************************************************!*\
  !*** ./src/scripts/components/cookie-popup.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (function (days) {
  var popupContainer = document.querySelector('.cookies-popup');
  var closeBtn = document.querySelector('.cookies-popup__close');
  var acceptBtn = document.querySelector('.cookies-popup__button');

  if (popupContainer) {
    if (!js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get('accept-policy')) {
      setTimeout(function () {
        popupContainer.classList.add('active');
      }, 1000);
    }

    closeBtn.addEventListener('click', function () {
      popupContainer.classList.remove('active');
    });
    acceptBtn.addEventListener('click', function () {
      popupContainer.classList.remove('active');
      js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set('accept-policy', true, {
        expires: days
      });
    });
  }
});

/***/ }),

/***/ "./src/scripts/components/favorites.js":
/*!*********************************************!*\
  !*** ./src/scripts/components/favorites.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FavoritesList; });
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! imagesloaded */ "./node_modules/imagesloaded/imagesloaded.js");
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(imagesloaded__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./helpers */ "./src/scripts/components/helpers.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var FavoritesList = /*#__PURE__*/function () {
  function FavoritesList() {
    var _this = this;

    _classCallCheck(this, FavoritesList);

    _defineProperty(this, "setCookies", function () {
      js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set('favorites', _this.data, {
        expires: 7
      });
    });

    _defineProperty(this, "notify", function (name) {
      var titleText = name + ' ' + _this.notifyText['message'];
      var container = document.createElement('div');
      var title = document.createElement('div');
      var button = document.createElement('a');
      container.className = 'notify';
      title.className = 'notify__title';
      button.className = 'notify__button notify__button_ok';
      title.innerHTML = titleText;
      button.href = _this.notifyText.link.href;
      button.innerText = _this.notifyText.link.title;
      container.append(title, button);
      var notify = document.querySelector('.notify');

      if (notify) {
        var _title = notify.querySelector('.notify__title');

        _title.innerHTML = titleText;
      } else {
        _this.body.append(container);

        setTimeout(function () {
          return container.classList.add('active');
        }, 100);
      }
    });

    _defineProperty(this, "setData", function () {
      if (_this.buttonPopup) {
        _this.buttonPopup.addEventListener('click', function () {
          var selectCards = _this.containerProfiles.querySelectorAll('.card-profile');

          selectCards.forEach(function (item) {
            var name = item.getAttribute('data-id');

            if (!_this.formData.includes(name)) {
              _this.formData.push(name);
            }
          });
          _this.formFavorites.querySelector('.select-models').value = _this.formData.join(', ');
        });
      }
    });

    _defineProperty(this, "add", function () {
      var removeNotify = Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["debounce"])(function () {
        var notify = document.querySelector('.notify');
        notify.classList.remove('active');
        setTimeout(function () {
          return notify.remove();
        }, 300);
      }, 4000);
      document.addEventListener('click', function (e) {
        var target = e.target;
        var element = target.closest('.button-card_add');

        if (element) {
          var modelName = element.getAttribute('data-model-name');
          var id = +element.getAttribute('data-model-id');
          console.log(modelName, id);
          _this.data.includes(id) ? _this.data = _this.data.filter(function (item) {
            return item !== id;
          }) : _this.data.push(id);
          element.classList.toggle('active');

          _this.setCookies();

          if (element.classList.contains('active')) {
            _this.notify(modelName);

            removeNotify();
          }
        }
      });
    });

    _defineProperty(this, "remove", function () {
      if (_this.container) _this.placeholderContent.querySelector('.favorites').classList.remove('favorites_show');
      document.addEventListener('click', function (e) {
        var target = e.target;
        var element = target.closest('.button-card_remove');

        if (element) {
          var elCard = element.closest('.card-profile');
          var id = +elCard.getAttribute('data-id');
          var name = elCard.querySelector('.card-profile__name').innerHTML;
          var gridItem = element.closest('.grid-item');

          if (_this.formData.includes(name)) {
            _this.formData = _this.formData.filter(function (el) {
              return el !== name;
            });
          }

          _this.data = _this.data.filter(function (item) {
            return item !== id;
          });

          _this.setCookies();

          gridItem.classList.remove('load');
          setTimeout(function () {
            gridItem.remove();
            var elements = document.querySelectorAll('.card-profile');

            if (elements.length === 0) {
              _this.container.appendChild(_this.placeholderContent);

              _this.buttonPopup.remove();

              _this.clearButton.remove();

              _this.containerProfiles.remove();

              imagesloaded__WEBPACK_IMPORTED_MODULE_1___default()('.favorites', function (e) {
                return e.elements[0].classList.add('favorites_show');
              });
            }
          }, 500);
        }
      });
    });

    _defineProperty(this, "clear", function () {
      if (_this.clearButton) {
        _this.clearButton.addEventListener('click', function () {
          _this.containerProfiles.remove();

          _this.clearButton.remove();

          _this.buttonPopup.remove();

          _this.container.appendChild(_this.placeholderContent);

          imagesloaded__WEBPACK_IMPORTED_MODULE_1___default()('.favorites', function (e) {
            return e.elements[0].classList.add('favorites_show');
          });
          js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.remove('favorites');
        });
      }
    });

    this.body = document.querySelector('body');
    this.notifyText = notifyPopup;
    this.data = js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get('favorites') !== undefined ? JSON.parse(js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get('favorites')) : [];
    this.container = document.querySelector('.container-favorites');
    this.containerProfiles = document.querySelector('.ajax-container');
    this.buttonPopup = document.querySelector('.button_favorites');
    this.formFavorites = document.querySelector('.form_favorites');
    this.clearButton = document.querySelector('.clear-button');
    this.formData = [];

    if (this.container) {
      this.placeholder = document.querySelector('#model-not-found');
      this.placeholderContent = this.placeholder.content.cloneNode(true);
    }
  }

  _createClass(FavoritesList, [{
    key: "init",
    value: function init() {
      this.add();
      this.remove();
      this.clear();
      this.setData();
    }
  }]);

  return FavoritesList;
}();



/***/ }),

/***/ "./src/scripts/components/form.js":
/*!****************************************!*\
  !*** ./src/scripts/components/form.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Forms; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(qs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./validation */ "./src/scripts/components/validation.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var Forms = /*#__PURE__*/function () {
  function Forms() {
    var _this = this;

    _classCallCheck(this, Forms);

    _defineProperty(this, "setData", function (form) {
      var inputs = form.querySelectorAll('input, textarea');
      _this.data = {};
      inputs.forEach(function (input) {
        var ckeckbox = input.type === 'checkbox';
        input.value.length && !ckeckbox || ckeckbox && input.checked ? _this.data[input.name] = input.value : delete _this.data[input.name];
      });
    });

    _defineProperty(this, "request", function (data, callback) {
      data['action'] = 'send_form';
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(path.ajax, qs__WEBPACK_IMPORTED_MODULE_1___default.a.stringify(data)).then(function (response) {
        return callback(response.data);
      }).catch(function (error) {
        return console.log(error);
      });
    });

    _defineProperty(this, "send", function () {
      var message = document.createElement('div');
      message.className = 'form__message';

      _this.forms.forEach(function (form) {
        var messageEl = message.cloneNode();
        form.addEventListener('submit', function (e) {
          e.preventDefault();

          _this.setData(e.target);

          var valid = new _validation__WEBPACK_IMPORTED_MODULE_2__["default"](e.target);

          if (valid.complete()) {
            var button = form.querySelector('button');
            button.disabled = true;

            _this.request(_this.data, function (response) {
              form.reset();
              button.disabled = false;
              messageEl.innerHTML = response;
              form.append(messageEl);
              setTimeout(function () {
                document.querySelectorAll('.form__message').forEach(function (el) {
                  return el.remove();
                });
              }, 2000);
            });
          }
        });
      });
    });

    this.forms = document.querySelectorAll('.form_contact, .form_contact-page, .form_calendar, .form_favorites');
    this.data = {};
  }

  _createClass(Forms, [{
    key: "init",
    value: function init() {
      this.send();
    }
  }]);

  return Forms;
}();



/***/ }),

/***/ "./src/scripts/components/helpers.js":
/*!*******************************************!*\
  !*** ./src/scripts/components/helpers.js ***!
  \*******************************************/
/*! exports provided: debounce, sniffer, createEl, declOfNum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sniffer", function() { return sniffer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createEl", function() { return createEl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "declOfNum", function() { return declOfNum; });
var sniffer = function sniffer() {
  var ua = navigator.userAgent.toLowerCase();
  var isWindowsPhone = /windows phone|iemobile|wpdesktop/.test(ua);
  var isDroidPhone = !isWindowsPhone && /android.*mobile/.test(ua);
  var isDroidTablet = !isWindowsPhone && !isDroidPhone && /android/i.test(ua);
  var isIos = !isWindowsPhone && /ip(hone|od|ad)/i.test(ua) && !window.MSStream;
  var isIpad = !isWindowsPhone && /ipad/i.test(ua) && isIos;
  var isTablet = isDroidTablet || isIpad;
  var isPhone = isDroidPhone || isIos && !isIpad || isWindowsPhone;
  var isDevice = isPhone || isTablet;
  var infos = {
    isDevice: isDevice,
    isPhone: isPhone,
    isTablet: isTablet,
    isDesktop: !isPhone && !isTablet
  };
  var output = {};
  Object.keys(infos).forEach(function (info) {
    return Object.defineProperty(output, info, {
      get: function get() {
        return infos[info];
      }
    });
  });
  return Object.freeze(output);
};

var debounce = function debounce(func, wait, immediate) {
  var timeout;
  return function executedFunction() {
    var context = this;
    var args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

var createEl = function createEl(arr, appendTo) {
  var el = '';

  for (var key in arr) {
    var _el = document.createElement(arr[key].tag);

    arr[key].class ? _el.className = arr[key].class : '';
    arr[key].text ? _el.innerHTML = arr[key].text : '';

    if (arr[key].attr !== undefined) {
      for (var attr in arr[key].attr) {
        _el.setAttribute(attr, arr[key].attr[attr]);
      }
    }

    if (arr[key].style !== undefined) {
      Object.assign(_el.style, arr[key].style);
    }

    if (arr[key].children !== undefined) {
      createEl(arr[key].children, _el);
    }

    appendTo.append(_el);
  }
};

var declOfNum = function declOfNum(n, text_forms) {
  n = Math.abs(n) % 100;
  var n1 = n % 10;

  if (n > 10 && n < 20) {
    return text_forms[2];
  }

  if (n1 > 1 && n1 < 5) {
    return text_forms[1];
  }

  if (n1 === 1) {
    return text_forms[0];
  }

  return n + ' ' + text_forms[2];
};



/***/ }),

/***/ "./src/scripts/components/input-select.js":
/*!************************************************!*\
  !*** ./src/scripts/components/input-select.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var select = function select() {
  var el = document.querySelectorAll('.select');
  el.forEach(function (select) {
    var title = select.querySelector('.select__title');
    var input = select.querySelector('.select__input');
    var inner = select.querySelector('.select__content');
    var items = inner.querySelectorAll('.select__item');
    title.addEventListener('click', function () {
      select.classList.toggle('active');
    });
    items.forEach(function (item) {
      var value = item.getAttribute('data-value');
      var label = item.innerHTML;
      item.addEventListener('click', function () {
        select.classList.toggle('active');
        title.innerHTML = label;
        input.value = value;
      });
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (select);

/***/ }),

/***/ "./src/scripts/components/instagram.js":
/*!*********************************************!*\
  !*** ./src/scripts/components/instagram.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Instagram; });
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./helpers */ "./src/scripts/components/helpers.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var Instagram = /*#__PURE__*/function () {
  function Instagram() {
    var _this = this;

    _classCallCheck(this, Instagram);

    _defineProperty(this, "media", function () {
      var fields = ['media_url', 'media_type', 'caption'];
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('https://graph.instagram.com/me/media', {
        params: {
          fields: JSON.stringify(fields),
          limit: 9,
          access_token: settings.api_instagram
        }
      }).then(function (response) {
        return _this.outputWrapper(response.data.data);
      }).catch(function (error) {
        console.log(error);
      });
    });

    _defineProperty(this, "cache", function () {
      _this.gridBounding = _this.grid.getBoundingClientRect();
      _this.blockBounding = _this.block[0].getBoundingClientRect();
      _this.scrollerBounding = _this.scroller.getBoundingClientRect();
      _this.scrollTop = Math.abs(_this.page.getBoundingClientRect().top);
      _this.distance = _this.scrollTop - (_this.scrollerBounding.top + _this.scrollTop);
      _this.percentage = _this.distance / ((_this.scrollerBounding.height - window.innerHeight) / 100);
      _this.blockCenter = window.innerHeight / 2 - _this.blockBounding.height / 2;
    });

    _defineProperty(this, "positions", function () {
      _this.blockScale = _this.ww / _this.blockBounding.width;

      _toConsumableArray(_this.layer).forEach(function (i) {
        return requestAnimationFrame(function () {
          i.style.transform = "scale(".concat(_this.blockScale);
          i.style.transformOrigin = "44.9% 50%";
        });
      });
    });

    _defineProperty(this, "reset", function () {
      _this.layer[1].style.visibility = 'hidden';
      _this.layer[3].style.visibility = 'hidden';
      _this.section.style.pointerEvents = '';

      _toConsumableArray(_this.layer).forEach(function (i) {
        i.style.transform = "scale(".concat(_this.blockScale, ")");
        _this.grid.style.top = 0;
        _this.grid.style.transform = '';
        _this.grid.style.bottom = 'auto';
      });
    });

    _defineProperty(this, "transform", function () {
      // set
      _this.grid.style.top = 0;
      _this.grid.style.bottom = 'auto';
      _this.section.style.pointerEvents = 'all';
      _this.transformTop = Math.abs(_this.scrollerBounding.top); // easeOutQuad

      var t1 = _this.percentage / 100;
      var f1 = t1 * (2 - t1);
      _this.easeOutQuad = f1 * 100;
      _this.ease1 = (_this.blockScale - 1) / 100 * _this.easeOutQuad;
      _this.ease1Scale = _this.blockScale - _this.ease1; // easeOutCubic

      var t2 = _this.percentage / 100;
      var f2 = --t2 * t2 * t2 + 1;
      _this.easeOutCubic = f2 * 100;
      _this.ease2 = (_this.blockScale - 1) / 100 * _this.easeOutCubic;
      _this.ease2Scale = _this.blockScale - _this.ease2; // easeOutQuart

      var t3 = _this.percentage / 100;
      var f3 = 1 - --t3 * t3 * t3 * t3;
      _this.easeOutQuart = f3 * 100;
      _this.ease3 = (_this.blockScale - 1) / 100 * _this.easeOutQuart;
      _this.ease3Scale = _this.blockScale - _this.ease3; // linear

      _this.linear = (_this.blockScale - 1) / 100 * _this.percentage;
      _this.linearScale = _this.blockScale - _this.linear; // scale

      if (_this.scale <= 1) {
        _toConsumableArray(_this.layer).forEach(function (i) {
          i.style.transform = "scale(1)";
        });

        return;
      } // visibility


      _this.layer[3].style.visibility = '';
      _this.layer[1].style.visibility = ''; // sticky

      _this.grid.style.transform = "translate3d(0, ".concat(_this.transformTop, "px, 0)"); // scaling

      _this.layer[0].style.transform = "scale(".concat(_this.ease3Scale, ")");
      _this.layer[1].style.transform = "scale(".concat(_this.linearScale, ")");
      _this.layer[2].style.transform = "scale(".concat(_this.linearScale, ")");
      _this.layer[3].style.transform = "scale(".concat(_this.ease2Scale, ")");
      _this.layer[4].style.transform = "scale(".concat(_this.linearScale, ")");
      _this.layer[5].style.transform = "scale(".concat(_this.ease1Scale, ")");
      _this.layer[6].style.transform = "scale(".concat(_this.ease2Scale, ")");
      _this.layer[7].style.transform = "scale(".concat(_this.linearScale, ")");
      _this.layer[8].style.transform = "scale(".concat(_this.linearScale, ")");
      _this.layer[9].style.transform = "scale(".concat(_this.linearScale, ")");
    });

    _defineProperty(this, "render", function () {
      _this.cache();

      if (window.innerHeight > _this.gridBounding.height) {
        _this.margin = window.innerHeight - _this.gridBounding.height;
        _this.scroller.style.top = "-".concat(_this.margin / 2, "px");
        _this.scroller.style.bottom = "-".concat(_this.margin / 2, "px");
      } else {
        _this.margin = _this.gridBounding.height - window.innerHeight;
        _this.scroller.style.top = "".concat(_this.margin / 2, "px");
        _this.scroller.style.bottom = "".concat(_this.margin / 2, "px");
      }

      if (_this.percentage > 0 && _this.percentage < 100) {
        _this.transform();
      }

      if (_this.percentage > 100) {
        _toConsumableArray(_this.layer).forEach(function (i) {
          i.style.transform = "scale(1)";
        });

        _this.grid.style.bottom = 0;
        _this.grid.style.top = 'auto';
        _this.grid.style.transform = '';
      }

      if (_this.percentage < 0) {
        _this.reset();
      }
    });

    _defineProperty(this, "create", function () {
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].ticker.add(_this.render);
    });

    _defineProperty(this, "destroy", function () {
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].ticker.remove(_this.render);
    });

    this.section = document.querySelector('.section_instagram');
    this.grid = document.querySelector('.inst-grid');
    this.gridContainer = document.querySelector('.grid-container');
    this.layer = document.querySelectorAll('.inst-grid__layer');
    this.block = document.querySelectorAll('.inst-grid__item');
    this.scroller = document.querySelector('.s-instagram-scroller');
    this.page = document.querySelector('main');
    this.ww = window.innerWidth;
    this.wh = window.innerHeight;
    this.breakpoint = !window.matchMedia('(min-width: 992px)').matches;
  }

  _createClass(Instagram, [{
    key: "outputWrapper",
    value: function outputWrapper(data) {
      var _this2 = this;

      data.forEach(function (item, i) {
        if (!_this2.block[i].childElementCount) {
          if (item.media_type === 'VIDEO') {
            var video = document.createElement('video');
            var source = document.createElement('source');
            video.className = 'inst-grid__video';
            video.setAttribute('autoplay', true);
            video.setAttribute('loop', true);
            video.setAttribute('preload', 'auto');
            source.setAttribute('type', 'video/mp4');
            source.setAttribute('src', item.media_url);
            video.append(source);
            video.muted = 'muted';

            _this2.block[i].append(video);
          } else {
            _this2.block[i].style.background = 'url(' + item.media_url + ') top center / cover';
          }
        }
      });
    }
  }, {
    key: "init",
    value: function init() {
      if (this.section) {
        this.media();

        if (Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["sniffer"])().isDevice || this.breakpoint) {
          this.gridContainer.classList.add('is-mobile');
          return;
        }

        this.cache();
        this.create();
        this.positions();
      }
    }
  }]);

  return Instagram;
}();



/***/ }),

/***/ "./src/scripts/components/lang-toggle.js":
/*!***********************************************!*\
  !*** ./src/scripts/components/lang-toggle.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function () {
  var langBtn = document.querySelector('.lang__current');

  if (langBtn != null) {
    langBtn.onclick = function (e) {
      e.stopPropagation();
      this.parentNode.classList.toggle('active');
    };

    document.onclick = function () {
      document.querySelector('.lang_desktop').classList.remove('active');
    };
  }
});

/***/ }),

/***/ "./src/scripts/components/lightbox.js":
/*!********************************************!*\
  !*** ./src/scripts/components/lightbox.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LightBox; });
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! imagesloaded */ "./node_modules/imagesloaded/imagesloaded.js");
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(imagesloaded__WEBPACK_IMPORTED_MODULE_1__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



swiper__WEBPACK_IMPORTED_MODULE_0__["default"].use([swiper__WEBPACK_IMPORTED_MODULE_0__["Navigation"], swiper__WEBPACK_IMPORTED_MODULE_0__["Thumbs"]]);

var LightBox = /*#__PURE__*/function () {
  function LightBox() {
    var _this = this;

    _classCallCheck(this, LightBox);

    _defineProperty(this, "create", function (arr) {
      var container = document.createElement('div');
      container.className = 'lightbox loading';
      var close = document.createElement('div');
      close.className = 'lightbox__close';
      var inner = document.createElement('div');
      inner.className = 'lightbox__inner container';
      var imageBig = document.createElement('div');
      imageBig.className = 'lightbox__gallery swiper-container';
      var thumbs = document.createElement('div');
      thumbs.className = 'lightbox__thumbs swiper-container';
      var wrap = document.createElement('div');
      wrap.className = 'swiper-wrapper';
      var slide = document.createElement('div');
      slide.className = 'lightbox__slide swiper-slide';
      var image = document.createElement('img');
      image.className = 'lightbox__image';
      var prevArrow = document.createElement('div');
      prevArrow.className = 'swiper-button-prev';
      var nextArrow = document.createElement('div');
      nextArrow.className = 'swiper-button-next';

      for (var i = 0; arr.length > i; i++) {
        var src = arr[i].getAttribute('href');
        var slideEl = slide.cloneNode();
        var imageEl = image.cloneNode();
        imageEl.setAttribute('src', src);
        slideEl.append(imageEl);
        wrap.append(slideEl);
      }

      var wrapThumb = wrap.cloneNode(true);
      wrapThumb.classList.add('lightbox__thumb-wrap');
      imageBig.append(wrap);
      thumbs.append(wrapThumb);
      inner.append(imageBig, thumbs, prevArrow, nextArrow);
      container.append(close, inner);

      _this.body.append(container);
    });

    _defineProperty(this, "open", function () {
      _this.links.forEach(function (item, index, arr) {
        item.addEventListener('click', function (e) {
          e.preventDefault();

          _this.create(arr);

          _this.slider(index);

          item.parentElement.classList.add('disabled');
          var lightBox = document.querySelector('.lightbox');
          lightBox.classList.add('active');

          _this.body.classList.add('menu-open');

          item.parentElement.classList.remove('disabled');
          imagesloaded__WEBPACK_IMPORTED_MODULE_1___default()('.lightbox', function () {
            return lightBox.classList.remove('loading');
          });
        });
      });
    });

    _defineProperty(this, "close", function () {
      document.addEventListener('click', function (e) {
        var lightbox = document.querySelector('.lightbox');

        if (e.target.classList[0] === 'lightbox__close') {
          lightbox.classList.remove('active');

          _this.body.classList.remove('menu-open');

          setTimeout(function () {
            lightbox.remove();
          }, 500);
        }
      });
    });

    _defineProperty(this, "slider", function (slideTo) {
      var gallery = '.lightbox__gallery';
      var thumbs = '.lightbox__thumbs';
      var galleryThumbs = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](thumbs, {
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        touchRatio: 0.2,
        breakpoints: {
          320: {
            slidesPerView: 4,
            spaceBetween: 4
          },
          640: {
            spaceBetween: 4,
            slidesPerView: 8
          }
        }
      });
      var galleryTop = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](gallery, {
        spaceBetween: 10,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        thumbs: {
          swiper: galleryThumbs
        }
      });
      galleryTop.slideTo(slideTo, 0);
    });

    this.body = document.querySelector('body');
    this.links = document.querySelectorAll('[data-lightbox]');
  }

  _createClass(LightBox, [{
    key: "init",
    value: function init() {
      this.open();
      this.close();
    }
  }]);

  return LightBox;
}();



/***/ }),

/***/ "./src/scripts/components/loadmore-blogs.js":
/*!**************************************************!*\
  !*** ./src/scripts/components/loadmore-blogs.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(qs__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["default"] = (function () {
  var loadmoreBtn = document.querySelector(".js-loadmore-blogs");
  var blogsContainer = document.querySelector(".js-blogs-container");
  if (!loadmoreBtn) return;
  var catID = loadMore.blogs;
  loadmoreBtn.addEventListener("click", function (e) {
    var currentBlogsNumber = blogsContainer.children.length;
    loadmoreBtn.classList.add("disabled");
    var data = {
      action: "load_more_blogs",
      offset: currentBlogsNumber
    };

    if (catID) {
      data['cat'] = catID;
    }

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(path.ajax, qs__WEBPACK_IMPORTED_MODULE_1___default.a.stringify(data)).then(function (response) {
      var restPosts = parseInt(response.data.total) - currentBlogsNumber;
      loadmoreBtn.classList.remove("disabled");

      if (restPosts < response.data.posts_per_page) {
        loadmoreBtn.remove();
      }

      response.data.posts.forEach(function (el) {
        var card = document.createElement("div");
        card.className = "col-lg-4 col-md-4 col-sm-6 col-xs-12";
        card.innerHTML = renderAjaxBlogCard(el);
        blogsContainer.append(card);
      });
    }).catch(function (error) {
      return console.log(error);
    });
  });
});

/***/ }),

/***/ "./src/scripts/components/maps.js":
/*!****************************************!*\
  !*** ./src/scripts/components/maps.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _googlemaps_js_api_loader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @googlemaps/js-api-loader */ "./node_modules/@googlemaps/js-api-loader/dist/index.esm.js");


var maps = function maps() {
  var mapEl = document.querySelector('.gmap-init');
  var init = GMaps.options !== null;

  if (mapEl && init) {
    var loader = new _googlemaps_js_api_loader__WEBPACK_IMPORTED_MODULE_0__["Loader"]({
      apiKey: GMaps.api_key,
      version: 'weekly'
    });
    loader.load().then(function () {
      var cords = {
        lat: GMaps.options.lat,
        lng: GMaps.options.lng
      };
      var map = new google.maps.Map(mapEl, {
        center: cords,
        zoom: GMaps.options.zoom,
        disableDefaultUI: true
      });
      var marker = new google.maps.Marker({
        position: cords,
        icon: path.theme + '/assets/icons/source/marker.svg'
      });
      marker.setMap(map);
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (maps);

/***/ }),

/***/ "./src/scripts/components/phone-mask.js":
/*!**********************************************!*\
  !*** ./src/scripts/components/phone-mask.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var intl_tel_input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! intl-tel-input */ "./node_modules/intl-tel-input/index.js");
/* harmony import */ var intl_tel_input__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(intl_tel_input__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);


window.intlTelInputGlobals.loadUtils(path.theme + '/assets/scripts/lazy/utils-phone-mask.js');

var mask = function mask() {
  var fields = document.querySelectorAll('[name="number"]');
  fields.forEach(function (field) {
    var mask = intl_tel_input__WEBPACK_IMPORTED_MODULE_0___default()(field, {
      initialCountry: 'auto',
      formatOnDisplay: false,
      autoPlaceholder: 'aggressive',
      preferredCountries: ['ua', 'ru'],
      separateDialCode: true,
      hiddenInput: 'full-number',
      geoIpLookup: function geoIpLookup(success) {
        axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('https://api.sypexgeo.net/').then(function (response) {
          success(response.data.country.iso);
        }).catch(function (error) {
          success('UA');
        });
      }
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (mask);

/***/ }),

/***/ "./src/scripts/components/photo-tabs.js":
/*!**********************************************!*\
  !*** ./src/scripts/components/photo-tabs.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var masonry_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! masonry-layout */ "./node_modules/masonry-layout/masonry.js");
/* harmony import */ var masonry_layout__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(masonry_layout__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! imagesloaded */ "./node_modules/imagesloaded/imagesloaded.js");
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(imagesloaded__WEBPACK_IMPORTED_MODULE_1__);



var tabs = function tabs() {
  var tabs = document.querySelector('.tabs');

  if (tabs) {
    var nav = tabs.querySelectorAll('.tabs__nav li');
    var content = tabs.querySelectorAll('.tabs__item');
    var masonry = [];
    var activeTab = 0;
    content.forEach(function (tab, i) {
      var el = tab.querySelector('.js-masonry');
      imagesloaded__WEBPACK_IMPORTED_MODULE_1___default()('.tabs', function () {
        masonry[i] = new masonry_layout__WEBPACK_IMPORTED_MODULE_0___default.a(el, {
          columnWidth: '.model-gallery__item',
          gutter: 0,
          itemSelector: '.js-masonry-item',
          transitionDuration: 0,
          horizontalOrder: true
        });
      });
    });
    nav[activeTab].classList.add('active');
    content[activeTab].classList.add('active');
    nav.forEach(function (item, number) {
      item.addEventListener('click', function () {
        if (number !== activeTab && number >= 0) {
          nav[activeTab].classList.remove('active');
          nav[number].classList.add('active');
          content[activeTab].classList.remove('active');
          content[number].classList.add('active');
          masonry[number].layout();
          activeTab = number;
        }
      });
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (tabs);

/***/ }),

/***/ "./src/scripts/components/preload-video.js":
/*!*************************************************!*\
  !*** ./src/scripts/components/preload-video.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var preloadVideo = function preloadVideo() {
  window.addEventListener('load', function () {
    setTimeout(function () {
      var video = document.querySelectorAll('.video__poster, .model-video__player');
      video.forEach(function (item) {
        var source = item.querySelector('source');
        source.src = source.getAttribute('data-src');
        item.load();
        item.play();
      });
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (preloadVideo);

/***/ }),

/***/ "./src/scripts/components/preloader.js":
/*!*********************************************!*\
  !*** ./src/scripts/components/preloader.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var preloader = function preloader() {
  window.addEventListener('load', function () {
    var body = document.querySelector('body');
    var preloader = document.querySelector('.preloader');
    var timeout = 1000;
    body.classList.remove('preload');
    setTimeout(function () {
      preloader.classList.remove('preloader_load');
    }, timeout);
  });
};

/* harmony default export */ __webpack_exports__["default"] = (preloader);

/***/ }),

/***/ "./src/scripts/components/search.js":
/*!******************************************!*\
  !*** ./src/scripts/components/search.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(qs__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["default"] = (function () {
  // Modal
  var searchBtn = document.querySelector('.buttons__item_search');
  var searchContainer = document.querySelector('.search-container');

  if (searchContainer) {
    searchContainer.addEventListener('click', function (e) {
      if (e.target.closest('.search-form')) return;
      searchContainer.classList.remove('active');
      searchBtn.classList.remove('active');
      document.querySelector('body').classList.remove('popup-open');
    });
  } // LiveSearch


  var body = document.querySelector('body');
  var formSearch = document.querySelector('.search-form');

  if (formSearch) {
    var startSearch = function startSearch() {
      var data = {
        s: inputSearch.value,
        post_type: ['post', 'models', 'page']
      };
      inputSearch.value.length > 2 ? request(data, function (response) {
        response.total > 0 ? visibleResult(response.posts) : hiddenResult('По вашему запросу ничего не найдено');
      }) : hiddenResult('Запрос должен быть длиной минимум 3 символа');
    };

    var visibleResult = function visibleResult(data) {
      resultContainer.classList.add('is-active');

      while (resultContainer.firstChild) {
        resultContainer.removeChild(resultContainer.firstChild);
      }

      for (var item in data) {
        var article = data[item];
        var itemEl = document.createElement('div');
        itemEl.className = 'search-form__item';
        itemEl.innerHTML = "\n          <a class=\"search-result\" href=\"".concat(article.link, "\">\n            <div class=\"search-result__title\">").concat(article.title, "</div>\n            <div class=\"search-result__descr\">").concat(article.description, "</div>\n          </a>\n        ");
        resultContainer.append(itemEl);
      }

      formSearch.classList.add('active');
      body.classList.add('active');
    };

    var hiddenResult = function hiddenResult(message) {
      resultContainer.innerHTML = message;
      resultContainer.classList.remove('is-active');
    };

    var request = function request(data, callback) {
      data.action = 'get_posts';
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(path.ajax, qs__WEBPACK_IMPORTED_MODULE_1___default.a.stringify(data)).then(function (response) {
        return callback(response.data);
      }).catch(function (error) {
        return console.log(error);
      });
    };

    var inputSearch = formSearch.querySelector('.search-form__input');
    var resultContainer = formSearch.querySelector('.search-form__result');
    inputSearch.addEventListener('input', startSearch);
  }
});

/***/ }),

/***/ "./src/scripts/components/thumbnails-slider.js":
/*!*****************************************************!*\
  !*** ./src/scripts/components/thumbnails-slider.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CardSlider; });
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers */ "./src/scripts/components/helpers.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var CardSlider = /*#__PURE__*/function () {
  function CardSlider(card) {
    var _this = this;

    _classCallCheck(this, CardSlider);

    _defineProperty(this, "clear", function () {
      _this.timeline.clear();

      _this.reset(_this.current);

      _this.immediate && _this.play();
    });

    _defineProperty(this, "reset", function (current) {
      _this.slides.filter(function (slide, i) {
        return i !== current;
      }).forEach(function (slide) {
        return gsap__WEBPACK_IMPORTED_MODULE_0__["default"].set(slide, {
          opacity: '0'
        });
      });
    });

    _defineProperty(this, "to", function (current, next) {
      _this.timeline.play();

      _this.timeline.to(current, {
        opacity: '0'
      }, 0).fromTo(next, {
        opacity: '0'
      }, {
        opacity: '1'
      }, 0);
    });

    _defineProperty(this, "play", function () {
      if (!_this.timeline.isActive()) {
        var next = _this.current === _this.length ? 0 : _this.current + 1;

        _this.to(_this.slides[_this.current], _this.slides[next]);

        _this.current < _this.length ? _this.current++ : _this.current = 0;
      }
    });

    _defineProperty(this, "addListeners", function () {
      _this.card.addEventListener('mouseover', function (e) {
        _this.immediate = true;

        _this.play();
      });

      _this.card.addEventListener('mouseout', function (e) {
        _this.immediate = false;

        if (_this.current !== 0) {
          _this.timeline.then(function () {
            _this.to(_this.slides[_this.current], _this.slides[0]);

            _this.current = 0;
          });
        }
      });
    });

    this.time = 0.8;
    this.ease = 'none';
    this.current = 0;
    this.card = card;
    this.slides = _toConsumableArray(card.querySelectorAll('.card-profile__image'));
    this.immediate = false;
    this.length = this.slides.length - 1;
    this.breakpoint = window.matchMedia('(min-width: 992px)').matches;
    this.timeline = new gsap__WEBPACK_IMPORTED_MODULE_0__["default"].timeline({
      paused: true,
      onComplete: function onComplete() {
        return _this.clear();
      }
    });
  }

  _createClass(CardSlider, [{
    key: "init",
    value: function init() {
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].defaults({
        ease: this.ease,
        duration: this.time,
        immediateRender: true,
        repeatRefresh: true,
        delay: 1
      });

      if (!Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["sniffer"])().isDevice && this.breakpoint) {
        if (this.length > 1) {
          this.addListeners();
        }
      }
    }
  }]);

  return CardSlider;
}();



/***/ }),

/***/ "./src/scripts/components/toggle-el.js":
/*!*********************************************!*\
  !*** ./src/scripts/components/toggle-el.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var toggleEl = function toggleEl() {
  document.addEventListener('click', function (e) {
    var element = e.target.closest('[data-toggle]');
    var allElements = document.querySelectorAll('[data-toggle]');

    if (element) {
      var attr = {
        toggle: element.getAttribute('data-toggle'),
        body: element.getAttribute('data-toggle-body'),
        context: element.getAttribute('data-toggle-context')
      };
      var el = {
        toggle: document.querySelectorAll(attr.toggle),
        body: document.querySelector('body'),
        context: attr.context && attr.context.length ? element.closest(attr.context) : false
      }; // allElements.forEach((item) => {
      //   if (element !== item) {
      //     const toggleClass = item.getAttribute('data-toggle');
      //     const toggleEl = document.querySelectorAll(toggleClass);
      //     const bodyClassAttr = item.getAttribute('data-toggle-body');
      //
      //     item.classList.remove('active');
      //     el.body.classList.remove(bodyClassAttr);
      //     toggleEl.forEach(el => el.classList.remove('active'));
      //
      //   }
      // });

      element.classList.toggle('active');
      attr.context && el.context ? el.context.querySelectorAll(attr.toggle).forEach(function (el) {
        return el.classList.toggle('active');
      }) : el.toggle.forEach(function (el) {
        return el.classList.toggle('active');
      });

      if (attr.body !== null && attr.body.length) {
        el.body.classList.toggle(attr.body);
      }
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (toggleEl);

/***/ }),

/***/ "./src/scripts/components/up-btn.js":
/*!******************************************!*\
  !*** ./src/scripts/components/up-btn.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var scroll_behavior_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! scroll-behavior-polyfill */ "./node_modules/scroll-behavior-polyfill/dist/index.js");
/* harmony import */ var scroll_behavior_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(scroll_behavior_polyfill__WEBPACK_IMPORTED_MODULE_0__);


var buttonUP = function buttonUP() {
  var btn = document.querySelector('.up-btn');

  if (btn) {
    btn.addEventListener('click', function () {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    });
    window.addEventListener('scroll', function () {
      window.scrollY >= 100 ? btn.classList.add('active') : btn.classList.remove('active');
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (buttonUP);

/***/ }),

/***/ "./src/scripts/components/validation.js":
/*!**********************************************!*\
  !*** ./src/scripts/components/validation.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Validation; });
/* harmony import */ var validate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! validate.js */ "./node_modules/validate.js/validate.js");
/* harmony import */ var validate_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(validate_js__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var Validation = /*#__PURE__*/function () {
  function Validation(form, data) {
    var _this = this;

    _classCallCheck(this, Validation);

    _defineProperty(this, "valid", function () {
      var fields = _this.form.querySelectorAll('input, textarea');

      var thisRules = {};
      validate_js__WEBPACK_IMPORTED_MODULE_0___default.a.validators.presence.message = '^' + _this.translation.fieldRequired;
      var rules = {
        'abb-name': {
          presence: true,
          length: {
            minimum: 3,
            message: '^' + _this.translation.minLength
          }
        },
        'number': {
          presence: true,
          format: {
            pattern: '[0-9 -]+',
            flags: 'i',
            message: '^' + _this.translation.invalidNumber
          }
        },
        'email': {
          presence: true,
          email: {
            message: '^' + _this.translation.wrongEmail
          }
        },
        'message': {
          presence: true
        },
        'personal': {
          presence: true
        },
        'comment': {
          presence: true
        }
      };
      fields.forEach(function (e) {
        if (rules[e.name] !== undefined) {
          thisRules[e.name] = rules[e.name];
        }
      });

      _this.form.querySelectorAll('.form__error').forEach(function (el) {
        return el.remove();
      });

      _this.form.querySelectorAll('input[type="checkbox"]').forEach(function (el) {
        return el.parentElement.classList.remove('is-error');
      });

      _this.errors = validate_js__WEBPACK_IMPORTED_MODULE_0___default()(_this.data, thisRules);

      _this.printErrors();
    });

    _defineProperty(this, "printErrors", function () {
      var errorEl = document.createElement('span');
      errorEl.className = 'form__error';

      for (var key in _this.errors) {
        var field = _this.form.querySelector('[name=\'' + key + '\']');

        var el = errorEl.cloneNode();
        var type = field.getAttribute('type');

        if (type !== 'checkbox') {
          el.innerHTML = _this.errors[key];
          field.closest('.form__field').append(el);
        } else {
          field.parentElement.classList.add('is-error');
        }
      }
    });

    _defineProperty(this, "complete", function () {
      return _this.errors === undefined;
    });

    this.form = form;
    this.data = validate_js__WEBPACK_IMPORTED_MODULE_0___default.a.collectFormValues(form);
    this.translation = modelForm.formErrors;
    this.errors = {};
    this.init();
  }

  _createClass(Validation, [{
    key: "init",
    value: function init() {
      this.valid();
    }
  }]);

  return Validation;
}();



/***/ }),

/***/ "./src/scripts/components/video-models.js":
/*!************************************************!*\
  !*** ./src/scripts/components/video-models.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var videoModels = function videoModels() {
  var video = document.querySelector('.model-video');

  if (video !== null) {
    var player = video.querySelector('.model-video__player');
    var mute = document.querySelector('.model-video__mute');
    var play = document.querySelector('.model-video__control');
    mute.addEventListener('click', function () {
      player.muted = mute.classList.contains('active');
      mute.classList.toggle('active');
    });
    setTimeout(function () {
      if (!player.paused) {
        play.classList.add('active');
      }
    });
    play.addEventListener('click', function () {
      play.classList.toggle('active');
      player.paused ? player.play() : player.pause();
    });
    player.addEventListener('loadeddata', function (event) {
      video.classList.add('loaded');
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (videoModels);

/***/ }),

/***/ "./src/scripts/components/video.js":
/*!*****************************************!*\
  !*** ./src/scripts/components/video.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Video; });
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var Video = /*#__PURE__*/function () {
  function Video() {
    var _this = this;

    _classCallCheck(this, Video);

    _defineProperty(this, "track", function (e) {
      if (_this.following === false) {
        _this.tX = _this.section.clientWidth / 2 - _this.badge.clientWidth / 2;
        _this.tY = _this.section.clientHeight / 2 - _this.badge.clientHeight / 2;
        return;
      }

      _this.tX = e.clientX - _this.badge.clientWidth / 2;
      _this.tY = e.clientY - _this.badge.clientHeight / 2;
    });

    _defineProperty(this, "render", function () {
      _this.wY = _this.section.getBoundingClientRect().top;
      _this.pX = _this.math.lerp(_this.pX, _this.tX, 0.05);

      if (_this.following === true) {
        _this.pY = _this.math.lerp(_this.pY, _this.tY - _this.wY, 0.05);
        _this.badge.style.transform = "translate3d(".concat(_this.pX, "px, ").concat(_this.pY, "px, 0)");
      } else {
        _this.pY = _this.math.lerp(_this.pY, _this.tY, 0.05);
        _this.badge.style.transform = "translate3d(".concat(_this.pX, "px, ").concat(_this.pY, "px, 0)");
      }
    });

    _defineProperty(this, "trackVideo", function (e) {
      _this.badge.style.transform = "translate3d(".concat(e.clientX - 60, "px, ").concat(e.clientY - 60, "px, 0)");
    });

    _defineProperty(this, "create", function () {
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].ticker.add(_this.render);

      _this.section.addEventListener('mouseenter', function () {
        _this.following = true;
      });

      _this.section.addEventListener('mouseleave', function () {
        _this.following = false;
      });
    });

    _defineProperty(this, "events", function () {
      window.addEventListener('mousemove', _this.track);

      _this.badge.addEventListener('click', function (e) {
        e.currentTarget.closest('.is-video-playing') ? _this.stopVideo() : _this.playVideo();
      });
    });

    _defineProperty(this, "destroy", function () {
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].ticker.remove(_this.render);
      window.removeEventListener('mousemove', _this.track);
    });

    _defineProperty(this, "playVideo", function () {
      _this.section.classList.add('is-video-playing');

      _this.video.play();
    });

    _defineProperty(this, "stopVideo", function () {
      _this.section.classList.remove('is-video-playing');

      _this.video.pause();
    });

    this.section = document.querySelector('.section_video');
    this.badge = document.querySelector('.video-badge');
    this.video = document.querySelector('.video__video');
    this.videoPlaying = false;
    this.following = false;

    if (this.section) {
      this.tX = this.section.clientWidth / 2 - this.badge.clientWidth / 2;
      this.tY = this.section.clientHeight / 2 - this.badge.clientHeight / 2;
      this.pX = this.section.clientWidth / 2 - this.badge.clientWidth / 2;
      this.pY = this.section.clientHeight / 2 - this.badge.clientHeight / 2;
    }

    this.math = {
      lerp: function lerp(a, b, n) {
        return (1 - n) * a + n * b;
      }
    };
  }

  _createClass(Video, [{
    key: "init",
    value: function init() {
      if (this.section !== null) {
        this.events();
        this.create();
      }
    }
  }]);

  return Video;
}();



/***/ })

/******/ });
//# sourceMappingURL=main.js.map