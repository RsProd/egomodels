<?php get_header();
if (have_posts()):
	while (have_posts()): the_post(); ?>
		<main class="main" role="main">
			<section class="section">
				<div class="container relative">
					<div class="page-head">
						<?php breadcrumbs(); ?>
					</div>
					<div class="content">
						<?php the_content(); ?>
					</div>
				</div>
			</section>
		</main>
	<?php
	endwhile;
endif;
get_footer(); ?>