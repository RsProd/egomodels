<?php
$name = get_field('name');
$params = get_field('parameters');
$add_params = get_field('add_parameters');
$instagram = get_field('instagram');
$model = get_field('models_link');
$description = get_field('description');
$video = get_field('video');
$favorites = in_array($post->ID, getFavorites());
$compcard = sprintf('%s?model=%s', get_field('compcard', 'options'), get_the_ID());
?>

<?php get_header(); ?>
<main class="main main_model">
	<section class="section section_model">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 relative last-xs">
					<div class="model__info">
						<div class="page-head page-head_model">
							<?php breadcrumbs('breadcrumbs_model') ?>
						</div>
						<?php if ($departure = get_field('departure')): ?>
							<div class="model-localtion">
								<?php echo svg('plane', 'model-localtion__icon') ?>
								<div class="model-localtion__name"><?php echo $departure ?></div>
							</div>
						<?php endif; ?>
						<div class="model__sub-panel">
							<div class="add-panel">
								<a class="button" href="<?php echo $compcard ?>" target="_blank">
									<?php echo svg('plus', 'button__icon') ?>
									<div class="button__name">Compcard</div>
								</a>
								<div
									class="button-card button-card_static button-card_add <?php echo $favorites ? 'active' : '' ?>"
									data-model-name="<?php echo $name ?>"
									data-model-id="<?php echo get_the_ID() ?>">
									<?php echo svg('cheart', 'button-card__icon') ?>
								</div>
							</div>
							<?php if (!empty($name)): ?>
								<h1 class="model__subtitle"><?php echo $name ?></h1>
							<?php endif; ?>
						</div>
						<div class="model-social">
							<?php if (!empty($instagram)): ?>
								<a class="model-social__item" href="<?php echo $instagram ?>" target="_blank">
									<?php echo svg('instagram-icon', 'model-social__icon') ?>
								</a>
							<?php endif;
							if (!empty($model)): ?>
								<a class="model-social__item" href="<?php echo $model ?>" target="_blank">
									<?php echo svg('m', 'model-social__icon') ?>
								</a>
							<?php endif; ?>
						</div>
						<?php get_template_part('templates/common/block', 'social', ["container" => "social_model", "is_model" => "true"]) ?>
						<div class="model__params">
							<?php if (!empty($params)): ?>
								<?php foreach ($params as $key => $item): ?>
									<div class="model__params-item">
										<span class="model__params-label"><?php echo $key ?>:</span>
										<span class="model__params-value"><?php echo $item ?></span>
									</div>
								<?php endforeach; endif; ?>
							<?php if (!empty($add_params)): foreach ($add_params as $key => $item): ?>
								<div class="model__params-item">
									<span class="model__params-label"><?php echo $key ?>:</span>
									<span class="model__params-value"><?php echo $item ?></span>
								</div>
							<?php endforeach;
							endif; ?>
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
					<div class="model__hero">
						<?php if (!empty($video)): ?>
							<div class="model-video">
								<video class="model-video__player" autoplay muted loop playsinline>
									<source data-src="<?php echo $video ?>" src="">
								</video>
								<span class="model-video__mute"></span>
								<span class="model-video__control"></span>
							</div>
						<?php elseif (has_post_thumbnail()): ?>
							<img class="model__hero-img" src="<?php echo get_the_post_thumbnail_url() ?>"
							     alt="avatar">
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section_description">
		<div class="container">
			<div class="row">
				<?php if (!empty($description)): ?>
					<div class="model__description">
						<?php echo($description); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="section section_photos">
		<div class="container">
			<div class="row">
				<?php get_template_part('templates/model/block', 'tabs') ?>
			</div>
		</div>
	</section>
</main>
<?php get_footer() ?>
