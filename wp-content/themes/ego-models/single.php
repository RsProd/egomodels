<?php get_header(); ?>
	<main class="main" role="main">
		<section class="section section_single">
			<?php if (have_posts()): while (have_posts()): the_post(); ?>
				<div class="page-head page-head_black">
					<div class="container relative">
						<?php breadcrumbs('breadcrumbs_inversion') ?>
						<div class="row flex-end">
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
								<?php get_template_part('templates/single/block', 'post-info', get_the_ID()) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
							<?php if (has_post_thumbnail()): ?>
								<img class="page-thumbnail" src="<?php echo get_the_post_thumbnail_url() ?>"
								     alt="<?php echo get_the_title() ?>" />
							<?php else: ?>
								<span class="image-holder image-holder_blog">
									<?php echo svg('photo', 'image-holder__icon') ?>
									<span
										class="image-holder__label"><?php _e('Миниатюра не загружена', TEXT_DOMAIN) ?></span>
								</span>
							<?php endif; ?>
							<div class="content">
								<?php the_content() ?>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</section>
		<?php
		get_template_part('templates/single/section', 'comments');
		get_template_part('templates/single/section', 'related-posts');
		?>
	</main>
<?php get_footer();
