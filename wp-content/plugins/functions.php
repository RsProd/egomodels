<?php
define('PRODUCTION', false);

define('WEBP_SUPPORT', true); //Need php module imagick

define("TH_PATH", get_template_directory_uri());
define('TEXT_DOMAIN', '4devTheme');

require_once('includes/functions/common.php');
require_once('includes/components/common.php');
