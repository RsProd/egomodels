<?php get_header();

echo 'This is SINGLE.PHP';

if (have_posts()):
	while (have_posts()): the_post();
		echo get_the_title();
		echo get_the_content();
	endwhile;
endif;

get_footer();
